## Synopsis ##

Project ini merupakan front-end dari project eOffice PKBLU untuk menampilkan dokumen elektronik pada browser desktop dan mobile
Installation

## FrontEnd Installation ##

* Semua database (file .nsf) harus dalam satu folder, kecuali eoffice.nsf yang bisa terpisah
* Edit file env.js (jika tidak ada, copy-paste file env.js.bak) dan edit apiUrlPath dan eofficeUrl dengan url untuk api dan home eOffice
* pastikan path "local/notesdata/domino/html/temp" ada karena digunakan sebagai direktori sementara untuk unggah (upload) photo

## BackEnd Installation ##
* internet site
* register
* sync profile
* sync klasifikasi
* list nota tindakan
* layout takah pdf & html
* api jenis surat masuk manual
* sps configuration by key & unit code
* Pastikan permission untuk pdfagent dan servlet