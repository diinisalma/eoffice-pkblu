function render( inputText ) {
	var elt = {sym:"qrcode",desc:"QR Code",text:"http://www.terryburton.co.uk/barcodewriter/",opts:"eclevel=M"};
	var t_opts = 'eclevel=M';
	var t_text = inputText;
	var t_altx = '';
	var text = t_text.replace(/^\s+/,'').replace(/\s+$/,'');
	var altx = t_altx.replace(/^\s+/,'').replace(/\s+$/,'');
	var opts = t_opts.replace(/^\s+/,'').replace(/\s+$/,'');
	
	/*
	var elt = symdesc[$('#symbol')[0].selectedIndex];
	var text = $('#symtext').val().replace(/^\s+/,'').replace(/\s+$/,'');
	var altx = $('#symaltx').val().replace(/^\s+/,'').replace(/\s+$/,'');
	var opts = $('#symopts').val().replace(/^\s+/,'').replace(/\s+$/,'');
	*/
	var bw = new BWIPJS;

	// Convert the options to a dictionary object, so we can pass alttext with
	// spaces.
	var tmp = opts.split(' '); 
	opts = {};
	for (var i = 0; i < tmp.length; i++) {
		if (!tmp[i])
			continue;
		var eq = tmp[i].indexOf('=');
		if (eq == -1)
			opts[tmp[i]] = bw.value(true);
		else
			opts[tmp[i].substr(0, eq)] = bw.value(tmp[i].substr(eq+1));
	}

	// Add the alternate text
	if (altx)
		opts.alttext = bw.value(altx);

	// Add any hard-coded options required to fix problems in the javascript
	// emulation. 
	opts.inkspread = bw.value(0);
	if (needyoffset[elt.sym] && !opts.textxalign && !opts.textyalign &&
			!opts.alttext && opts.textyoffset === undefined)
		opts.textyoffset = bw.value(-10);

	var rot  = 'N';
	/*
	var rots = [ 'rotL', 'rotR', 'rotI' ];
	for (var i = 0; i < rots.length; i++) {
		if (document.getElementById(rots[i]).checked) {
			rot = rots[i].charAt(3);
			break;
		}
	}
	*/
	bw.bitmap(new Bitmap);
	
	//var scl = parseInt(document.getElementById('scale').value, 10) || 2;
	var scl = 1;
	bw.scale(scl,scl);

	var div = document.getElementById('output');
	if (div)
		div.innerHTML = '';

	bw.push(text);
	bw.push(opts);

	try {
		bw.call(elt.sym);
		bw.bitmap().show('canvas', rot);
	} catch(e) {
		var s = '';
		if (e.fileName)
			s += e.fileName + ' ';
		if (e.lineNumber)
			s += '[line ' + e.lineNumber + '] ';
		alert(s + (s ? ': ' : '') + e.message);
	}
}