 var app = angular.module('klasifikasi', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ui.sortable']);
 var namaUrl = window.location.hostname;
 var pilihan = [];
 
 //klasifikasi service factory
 app.factory('KlasifikasiFactory', ['$http', function($http) {
	 
	return {
		/* dapetin maincatnya aja */
		getMaincat : function(cache) {
		var url = '//' + namaUrl + '/web/scx/klasifikasi/maincat';
		console.log("getmaincat service cache" + cache);
			   return $http.get(url,cache).then(function(response){
	  				return response.data.data;
	  			});
	   },
	   /* service expand category */
	   getCat : function(cats,cache) {
		   
		   var url = "//" + namaUrl + "/web/scx/klasifikasi/cat";
		   for(var i=0;i<cats.length;i++){
			   url += "/"+cats[i];
		   }
		   return $http.get(url,cache).then(function(response){
		   					 return response.data.data;
		   				 });
	   }
	}
	 }]);
 
 
 app.controller('ModalKlasifikasiCtrl', function ($uibModal, $log, $document) {
  var $ctrl = this;

  $ctrl.open = function (mode, target) {
      var modalInstance = $uibModal.open({
      animation: $ctrl.animationsDisabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'klasifikasi',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      backdrop: 'static',
      /* pake cara langsung hit http
      resolve: {
          maincatKlasifikasi: ['$http',function ($http) {
      	  
      	  return $http.get('http://' + namaUrl + '/web/scx/klasifikasi/maincat').then(function(response){
  				return response.data.data;
  				
  			});
          }]
        },*/
      
      /* pake service/factory */
      resolve: {
          maincatKlasifikasi: ['KlasifikasiFactory',function (KlasifikasiFactory) {
      	  
      	  return KlasifikasiFactory.getMaincat({cache:true});
          }]
        }
      
    });
      
      // fungi yg bakal dijalanin setelah klik fungsi ok
      modalInstance.result.then(function (selectedItem) {
    	  
		  console.log(target); 
		  console.log("result : "+selectedItem);
		  var element = document.getElementById(target);
		  if(mode == "multiple"){
			  // <textarea>
			  element.value = selectedItem.join("\n");	
		  }else if(mode == "single"){
			// <input text>
			  element.setAttribute("value", selectedItem);		
		  }    
	    		   
	  }, function () {
		  console.log('Modal dismissed at: ' + new Date());
	}); 
      
  };
});
 app.controller('ModalInstanceCtrl', function ($scope,$uibModalInstance, $http, maincatKlasifikasi, KlasifikasiFactory) {
	 var $ctrl = this;
	 $scope.collapsed = true;
		$scope.maincat = maincatKlasifikasi;
		$scope.searchKlasifikasi = "";
		
			$scope.tambah_pilihan = function(klasifikasi, $filter){
				console.log(klasifikasi.kode);
				$ctrl.pilihan = klasifikasi.kode + " - "+klasifikasi.topik;
				
				//setelah klasifikasi dipilih langsung jalanin fungsi tombol ok
				$ctrl.ok();
			};

			$scope.toggle = function(obj, parent1, parent2, parent3, parent4){
				console.log(obj);
				console.log("parent : "+parent1);
				if(obj.expanded){
					console.log("is collapsed");
					console.log(obj);
					$scope.collapse(obj);
					obj.expanded = false;
					
				}else{
					console.log("is not collapsed");
					console.log(obj);
					$scope.expand(obj, parent1, parent2, parent3, parent4);
					obj.expanded = true;
				}
			}
			
			$scope.expand = function(catobj, parent1, parent2, parent3, parent4){
				console.log("expanded : "+catobj.expanded);
				//console.log(catobj.category+"/"+parent1+ "/"+parent2+ "/"+parent3+ "/"+parent4);
				var cats = [];
				if(catobj != null){
					if(parent1 == null){
						cats = [catobj.category];
						//console.log("Cats1 : " + cats);
					}else if(parent2 == null){
						cats = [parent1,catobj.kode];
						//console.log("Cats2 : " + cats);
					}else if(parent3 == null){
						cats = [parent1,parent2,catobj.kode];
						//console.log("Cats3 : " + cats);
					}else if(parent4==null){
						cats = [parent1, parent2, parent3,catobj.kode];
						//console.log("Cats4 : " + cats);
					}else{
						cats = [parent1, parent2, parent3, parent4, catobj.kode];
						//console.log("Cats else : " + cats);
					}
				}
				
				catobj.nodes = KlasifikasiFactory.getCat(cats,{cache:true}).then(function(response){
					catobj.nodes = response;
	  		  	});
				
				/*
				if(catobj != null && parent1 != null && parent2 != null && parent4 != null){
					
				}else{
					if(catobj != null && parent1 != null && parent2 != null && parent3 != null){
						
					}else
						{
						if(catobj != null &&parent1 != null && parent2 != null){
							$http.get('//' + namaUrl + '/web/scx/klasifikasi/cat/'+parent1+'/'+parent2+'/'+catobj.kode).then(function(response){
								$scope.catlist = response.data.data;
								catobj.nodes = $scope.catlist;
					  		  });
						}else{
							if(catobj != null && parent1 != null){
								$http.get('//' + namaUrl + '/web/scx/klasifikasi/cat/'+parent1+'/'+catobj.kode).then(function(response){
									$scope.catlist = response.data.data;
									catobj.nodes = $scope.catlist;
						  		  });
							}else{
								if(catobj != null){
									cats.push(catobj.category);
									catobj.nodes = KlasifikasiFactory.getCat(cats,{cache:true}).then(function(response){
										catobj.nodes = response;
						  		  	});
									/*
									$http.get('//' + namaUrl + '/web/scx/klasifikasi/cat/'+catobj.category).then(function(response){
										$scope.catlist = response.data.data;
										catobj.nodes = $scope.catlist;
						  		  	});
								}
							}
						}
						}
				}
				*/
			}
			
			$scope.collapse = function(catobj){
				console.log(catobj);
				catobj.nodes = [];
				
			};

	  $ctrl.ok = function () {
	    //alert("ok");
		  $uibModalInstance.close($ctrl.pilihan);
	  };

	  $ctrl.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	  };
	  
	  /* !cuma eksperimen : panggil service pake button refresh */
	  
	  $ctrl.refresh = function () {
		  		
		  		KlasifikasiFactory.getMaincat().then(function(successResponse){
		  			$scope.maincat = successResponse; 
		  			console.log($scope.maincat); 
		  			/*$scope.maincat.push({level:"999",category:"tes tes tes push"});*/ 
		  			console.log("maincat telah direfresh");});
		  		};
	  
	});
 
