 var addr_book = angular.module('ui.bootstrap.demo', ['ngAnimate', 'ngSanitize', 'ui.bootstrap', 'ui.tree']);
 var namaUrl = window.location.hostname;
 addr_book.controller('ModalDemoCtrl', function ($uibModal, $log, $document) {
  var $ctrl = this;
  $ctrl.items = ['item1', 'item2', 'item3'];

  $ctrl.animationsEnabled = true;

  $ctrl.open = function (size, parentSelector) {
    var parentElem = parentSelector ? 
      angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
      var modalInstance = $uibModal.open({
      animation: $ctrl.animationsDisabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'address-book',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      size: size,
      appendTo: parentElem,
      resolve: {
        items: function () {
          return $ctrl.items;
        }
      }
    });
  };

});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

 addr_book.controller('ModalInstanceCtrl', function ($uibModalInstance, items) {
  var $ctrl = this;
  $ctrl.items = items;
  $ctrl.selected = {
    item: $ctrl.items[0]
  };

  $ctrl.ok = function () {
    $uibModalInstance.close($ctrl.selected.item);
  };

  $ctrl.cancel = function () {
    $uibModalInstance.dismiss('cancel');
  };
});

// Please note that the close and dismiss bindings are from $uibModalInstance.

addr_book.component('modalComponent', {
  templateUrl: 'myModalContent.html',
  bindings: {
    resolve: '<',
    close: '&',
    dismiss: '&'
  },
  controller: function () {
    var $ctrl = this;

    $ctrl.$onInit = function () {
      $ctrl.items = $ctrl.resolve.items;
      $ctrl.selected = {
        item: $ctrl.items[0]
      };
    };

    $ctrl.ok = function () {
      $ctrl.close({$value: $ctrl.selected.item});
    };

    $ctrl.cancel = function () {
      $ctrl.dismiss({$value: 'cancel'});
    };
  }
});

addr_book.controller('AddressBookCtrl', ['$scope','$http', function ($scope,$http) {

	$scope.collapsed = true;
  
	/* Load main category */
	$scope.maincat = [];
	//$scope.init = function () {
		$scope.$emit('LOAD');
		
		$http.get('http://' + namaUrl + '/web/scx/addrbook/maincat').then(function(response){
			$scope.maincat = response.data.data;
			//console.log("Data = " + response.data.data.jabatan);
		});
		$http.get('http://' + namaUrl + '/web/scx/addrbook/all/1/999').then(function(response){
			$scope.persons = response.data.data;
			//$scope.user_name = response.data.data[0].fullname;
			//console.log("address book all : "+response.data.data[0].fullname);
			//console.log("Data = " + response.data.data.jabatan);
  		});
		$scope.$emit('UNLOAD')
	//};
	

		
		$scope.pilihan = [];
		
		console.log("data :"+ $scope.pilihan);
		
		$scope.tambah_pilihan = function(kontak, $filter){
			
			if ($scope.pilihan.indexOf(kontak) == -1) {
				$scope.pilihan.push(kontak);
				//
				console.log($scope.pilihan);
				var valueArr = $scope.pilihan.map(function(item){ return item.jabatan });
				$scope.isDuplicate = valueArr.some(function(item, idx){ 
				    return valueArr.indexOf(item) != idx 
				});
				if($scope.isDuplicate){
					$scope.pilihan.pop();
					alert(kontak.jabatan+" sudah ada di daftar pilihan");
				}
				
				console.log("is Duplicate ? "+$scope.isDuplicate);
				console.log("array pilihan "+valueArr);
		     }else{
		    	 alert(kontak.jabatan+" sudah ada di daftar pilihan");
		     }
			
			//console.log(name.node.fullname);
			
		};
		
		$scope.hapus_pilihan = function(kontak){
			var index = $scope.pilihan.indexOf(kontak.node);
			console.log("index :"+index);
			$scope.pilihan.splice(index,1);
			//$scope.pilihan.pop(kontak.node);			
			//console.log(name.node.fullname);
			
		};
		
//		$scope.toggle = function(obj){
//			console.log(obj);
//			if(obj.collapsed){
//				console.log("is collapsed");
//				console.log(obj);
//				$scope.expand(obj);
//				obj.collapsed = false;
//			}else{
//				console.log("is not collapsed");
//				console.log(obj);
//				$scope.collapse(obj);
//				obj.collapsed = true;
//				
//				
//			}
//		}
		
		$scope.toggle = function(obj, parent1, parent2, parent3, parent4){
			console.log(obj);
			console.log("parent : "+parent1);
			if(obj.expanded){
				console.log("is collapsed");
				console.log(obj);
				$scope.collapse(obj, parent1, parent2, parent3, parent4);
				obj.expanded = false;
				
			}else{
				console.log("is not collapsed");
				console.log(obj);
				$scope.expand(obj, parent1, parent2, parent3, parent4);
				obj.expanded = true;
			}
		}
		
		$scope.expand = function(catobj, parent1, parent2, parent3, parent4){
			console.log("expanded : "+catobj.expanded);
			if(catobj != null && parent1 != null && parent2 != null && parent4 != null){
				
			}else{
				if(catobj != null && parent1 != null && parent2 != null && parent3 != null){
					
				}else
					{
					if(catobj != null &&parent1 != null && parent2 != null){
						$http.get('//' + namaUrl + '/web/scx/addrbook/cat/'+parent1+'/'+parent2+'/'+catobj.jabatan).then(function(response){
							$scope.catlist = response.data.data;
							catobj.nodes = $scope.catlist;
				  		  });
					}else{
						if(catobj != null && parent1 != null){
							$http.get('//' + namaUrl + '/web/scx/addrbook/cat/'+parent1+'/'+catobj.jabatan).then(function(response){
								$scope.catlist = response.data.data;
								catobj.nodes = $scope.catlist;
					  		  });
						}else{
							if(catobj != null){
								$http.get('//' + namaUrl + '/web/scx/addrbook/cat/'+catobj.category).then(function(response){
									$scope.catlist = response.data.data;
									catobj.nodes = $scope.catlist;
					  		  	});
							}
						}
					}
					}
			}
		}
		
		$scope.collapse = function(catobj, parent1, parent2, parent3, parent4){
			console.log(catobj);
			catobj.nodes = [];
			
		};
		
		
	$scope.parent_node = function () {
		$http.get('http://' + namaUrl + '/web/scx/addrbook/cat/'+jabatan).then(function(response){
			$scope.sekretaris = response.data.data;
			console.log("Data = " + response.data.data.jabatan);
  		});
	    scope.toggle();
	};
	
	
	$scope.all = function () {
		alert("dendi");
		$http.get('http://' + namaUrl + '/web/scx/addrbook/all/1/999').then(function(response){
			$scope.persons = response.data.data;
			//console.log("Data = " + response.data.data.jabatan);
  		});
		
	};

	$scope.moveLastToTheBeginning = function () {
		var a = $scope.data.pop();
		$scope.data.splice(0, 0, a);
	};

  $scope.collapseAll = function () {
    $scope.$broadcast('angular-ui-tree:collapse-all');
  };

  $scope.expandAll = function () {
    $scope.$broadcast('angular-ui-tree:expand-all');
  };

}]);

addr_book.controller('ContentCtrl',['$scope',function($scope){
	$scope.$on('LOAD',function(){$scope.loading=true});
	$scope.$on('UNLOAD',function(){$scope.loading=false});
}]);