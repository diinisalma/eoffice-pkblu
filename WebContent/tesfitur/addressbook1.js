 var addr_book = angular.module('ui.bootstrap.demo', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','ui.sortable']);
 var namaUrl = window.location.hostname;
 var pilihan = [];
 
 addr_book.controller('ModalDemoCtrl', function ($uibModal, $log, $document) {
  var $ctrl = this;
  
$ctrl.items = [];
  $ctrl.animationsEnabled = true;

  $ctrl.open = function (mode, target) {
	  //var $scope.pilihan = target;
      var modalInstance = $uibModal.open({
      animation: $ctrl.animationsDisabled,
      ariaLabelledBy: 'modal-title',
      ariaDescribedBy: 'modal-body',
      templateUrl: 'address-book',
      controller: 'ModalInstanceCtrl',
      controllerAs: '$ctrl',
      backdrop: 'static',
      resolve: {
        pilihan: function () {
          return target;
        }
      }
    });
  };
});
 /*===DATEFORMATER DIRECTIVE===*/
 addr_book.directive('isRead', function() { 	
   return {   
	'scope':false,
 	'link': function (scope, element, attrs){
 			scope.$watch(attrs.isRead,function(style){
 				if(!style == 0){
 					return ;
 				}
 				attrs.$set('style','font-weight:bold');
 			});
 	}
   };
 });
 addr_book.controller('ModalInstanceCtrl', function ($uibModalInstance, pilihan) {
	  var $ctrl = this;
	  $ctrl.pilihan = pilihan;

	  $ctrl.ok = function () {
	    alert("ok");
		  $uibModalInstance.close($ctrl.pilihan);
		  pilihan = [];
	  };

	  $ctrl.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	  };
	  
	});

// Please note that the close and dismiss bindings are from $uibModalInstance.

addr_book.controller('AddressBookCtrl', ['$scope','$http', function ($scope,$http) {

	$scope.collapsed = true;
	$scope.pilihan = pilihan;
	$scope.maincat = [];
	
		$scope.$emit('LOAD');
		
		$http.get('http://' + namaUrl + '/web/scx/addrbook/maincat').then(function(response){
			$scope.maincat = response.data.data;
			//console.log("Data = " + response.data.data.jabatan);
		});
		$http.get('http://' + namaUrl + '/web/scx/addrbook/all/1/999').then(function(response){
			$scope.persons = response.data.data;
  		});
		$scope.$emit('UNLOAD')
	//};
		$scope.tambah_pilihan = function(kontak, $filter){
			console.log(kontak.jabatan);
			if (pilihan.indexOf(kontak.jabatan) == -1) {
				pilihan.push(kontak.jabatan);
				console.log(pilihan);
		     }else{
		    	 alert(kontak.jabatan+" sudah ada di daftar pilihan");
		     }
			
		};
		
		$scope.hapus_pilihan = function(kontak){
			var index = pilihan.indexOf(kontak.node);
			pilihan.splice(index,1);
		};
		
		
		$scope.toggle = function(obj, parent1, parent2, parent3, parent4){
			console.log(obj);
			console.log("parent : "+parent1);
			if(obj.expanded){
				console.log("is collapsed");
				console.log(obj);
				$scope.collapse(obj, parent1, parent2, parent3, parent4);
				obj.expanded = false;
				
			}else{
				console.log("is not collapsed");
				console.log(obj);
				$scope.expand(obj, parent1, parent2, parent3, parent4);
				obj.expanded = true;
			}
		}
		
		$scope.expand = function(catobj, parent1, parent2, parent3, parent4){
			console.log("expanded : "+catobj.expanded);
			if(catobj != null && parent1 != null && parent2 != null && parent4 != null){
				
			}else{
				if(catobj != null && parent1 != null && parent2 != null && parent3 != null){
					
				}else
					{
					if(catobj != null &&parent1 != null && parent2 != null){
						$http.get('//' + namaUrl + '/web/scx/addrbook/cat/'+parent1+'/'+parent2+'/'+catobj.jabatan).then(function(response){
							$scope.catlist = response.data.data;
							catobj.nodes = $scope.catlist;
				  		  });
					}else{
						if(catobj != null && parent1 != null){
							$http.get('//' + namaUrl + '/web/scx/addrbook/cat/'+parent1+'/'+catobj.jabatan).then(function(response){
								$scope.catlist = response.data.data;
								catobj.nodes = $scope.catlist;
					  		  });
						}else{
							if(catobj != null){
								$http.get('//' + namaUrl + '/web/scx/addrbook/cat/'+catobj.category).then(function(response){
									$scope.catlist = response.data.data;
									catobj.nodes = $scope.catlist;
					  		  	});
							}
						}
					}
					}
			}
		}		
		
		$scope.collapse = function(catobj, parent1, parent2, parent3, parent4){
			console.log(catobj);
			catobj.nodes = [];
			
		};
}]);

addr_book.controller('ContentCtrl',['$scope',function($scope){
	$scope.$on('LOAD',function(){$scope.loading=true});
	$scope.$on('UNLOAD',function(){$scope.loading=false});
}]);