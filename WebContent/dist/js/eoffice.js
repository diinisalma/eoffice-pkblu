// environment setting
var env = {};

// import variables if presents (from env.js)
if(window) {
	//Object.assign(env, window.__env);
	env = $.extend({}, window.__env);
}

// Eoffice.js
var routerApp = angular.module('eofficeApp', ['ui.router','ui.bootstrap','ui.tinymce','ngSanitize','ngAnimate','ui.tree','checklist-model','ui.sortable','truncate','angular-loading-bar','ngFileUpload','ng-mfb', 'angularMoment', 'pdf']);
routerApp.constant('__env', env);

var namaUrl = window.location.hostname;
//var isMobile = (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4)));
var isMobile = null;
var w = null;
go();
window.addEventListener('resize', go);

function go(){
	w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	if(w >= 768){
		isMobile = false;
		
	}else{
		isMobile = true;
	}
}

var letter_unread_temp = 0;
var pilihan = [];
var pilihan_klasifikasi = {kode:'',topik:''};
var body = "";
var aksi = "";
var ab_mode = "";

var inboxFilter = [{name:"all", title:"Semua", role:"*",unread:0},
                   {name:"nota", title:"Nota Dinas", role:"*",unread:0},
                   {name:"tugas", title:"Surat Tugas", role:"*",unread:0},
                   {name:"undangan", title:"Surat Undangan", role:"*",unread:0},
                   {name:"surat", title:"Surat", role:"*",unread:0},
                   {name:"memo", title:"Memo", role:"*",unread:0},
                   {name:"disposisi", title:"Disposisi", role:"*",unread:0},
                   {name:"tanggapan", title:"Tanggapan", role:"*",unread:0}];

var outboxFilter = [{name:"all", title:"Semua", role:"*"},
                   {name:"manual", title:"Surat Masuk Manual", role:"is_sps"},
                   {name:"nota", title:"Nota Dinas", role:"*"},
                   {name:"tugas", title:"Surat Tugas", role:"*"},
                   {name:"undangan", title:"Surat Undangan", role:"*"},
                   {name:"surat", title:"Surat", role:"*"},
                   {name:"memo", title:"Memo", role:"*"},
                   {name:"disposisi", title:"Disposisi", role:"*"},
                   {name:"batal", title:"Batal", role:"*"},
                   {name:"verified", title:"Surat Sudah Finalisasi", role:"is_verifier"}];

function getInboxFilterByName(name) {
	  return inboxFilter.filter(
	    function(inboxFilter) {
	      return inboxFilter.name == name;
	    }
	  );
	}

function getOutboxFilterByName(name) {
	  return outboxFilter.filter(
	    function(outboxFilter) {
	      return outboxFilter.name == name;
	    }
	  );
	}

var nodin = [
   {param: "NotaDinas", title: "Nota Dinas"},
   {param: "SuratTugas", title: "Surat Tugas"},
   {param: "SuratUndangan", title: "Surat Undangan"}
];

var suratExtern = [
	{param: "SuratUndangan", title: "Surat Undangan"},
	{param: "SuratKeluar", title: "Surat"}
];

var data_perpage = 10;

var jabatan = "";

routerApp.config(function($provide){
	$provide.decorator('pdfViewerToolbarDirective', function($delegate){
		var directive = $delegate[0];
		
		// Hapus template bawaan agar tidak bentrok dengan templateUrl
		directive.template = null;
		// Gunakan template dari URL berikut
		directive.templateUrl = 'angular/template/pdf-viewer-toolbar.tpl.html';
		
		return $delegate;
	});
});

routerApp.run(function ($state,$rootScope) {
    $rootScope.uiRouter = $state;
    $rootScope.rootisMobile = isMobile;
    console.log("$rootScope.rootisMobile : ",$rootScope.rootisMobile)
});

//Directive Read More
routerApp.directive('readMore', ['$compile', function($compile) {

 return {
     restrict: 'A',
     scope: true,
     link: function(scope, element, attrs) {

         // start collapsed
         scope.collapsed = false;

         // create the function to toggle the collapse
         scope.toggle = function() {
             scope.collapsed = !scope.collapsed;
         };

         // wait for changes on the text
         attrs.$observe('readMoreText', function(text) {

             // get the length from the attributes
             var maxLength = scope.$eval(attrs.readMoreMaxLength);

             if (text.length > maxLength) {
                 // split the text in two parts, the first always showing
                 var firstPart = String(text).substring(0, maxLength);
                 var secondPart = String(text).substring(maxLength, text.length);

                 // create some new html elements to hold the separate info
                 var firstSpan = $compile('<span>' + firstPart + '</span>')(scope);
                 var secondSpan = $compile('<span ng-if="collapsed">' + secondPart + '</span>')(scope);
                 var moreIndicatorSpan = $compile('<span ng-if="!collapsed">... </span>')(scope);
                 var lineBreak = $compile('<br ng-if="collapsed">')(scope);
                 var toggleButton = $compile('<span class="collapse-text-toggle" ng-click="toggle()">{{collapsed ? "sembunyikan" : "tampilkan"}}</span>')(scope);

                 // remove the current contents of the element
                 // and add the new ones we created
                 element.empty();
                 element.append(firstSpan);
                 element.append(secondSpan);
                 element.append(moreIndicatorSpan);
                 element.append(lineBreak);
                 element.append(toggleButton);
             }
             else {
                 element.empty();
                 element.append(text);
             }
         });
     }
 };
}]);

//Referensi modal
routerApp.controller('ReferensiModal', ['$scope', '$uibModal','$injector' ,function ($scope, $uibModal, $injector) {
	var $ctrl = this;
	$scope.refs = {};
	
	$ctrl.open = function(resolve, refsNo, refsData) {		
		var modalInstance = $uibModal.open({
			animation: $ctrl.animationsDisabled,
		    ariaLabelledBy: 'modal-title',
		    ariaDescribedBy: 'modal-body',
		    templateUrl: 'pages/forms/referensi-modal.html',
		    controller: 'ReferensiModalCtr',
		    controllerAs: '$ctrl',
		    backdrop: 'static',
		    size: 'lg',
		    resolve: {
				referenceNo: function () {
					return refsNo;
				},
				referenceData: function () {
					return refsData;
				}
			}
		});
		
		modalInstance.result.then(function(items){
			resolve(items);
			
			console.log('XXX referensi', items);
		}, function(error){
			console.log('XXX error referensi', error);
		});
	}	
}]);
routerApp.controller('ReferensiModalCtr', ['$scope','$http','$uibModalInstance', 'referenceNo', 'referenceData', '__env', function ($scope, $http, $uibModalInstance, referenceNo, referenceData, __env) {
	var $ctrl = this;
	let NUM_OF_ITEM_PER_PAGE = 20;
	
	$scope.referenceSubject = referenceNo;
	$scope.referenceData = referenceData;
	$scope.referenceList = Array(referenceNo.length);
	
	$scope.loading = false;
	$scope.showFooter = false;
	$scope.items = [];
	$scope.totalItems = 0;
	$scope.showReferencePanel = true;
	$scope.showSelectedRefPanel = !isMobile || false;
	$scope.showPreview = false;
	$scope.selectedDoc;
	$scope.currentPage = 1;
	
	$scope.mobileView = function() {
		return isMobile;
	}

	$scope.changeContent = function() {
		$scope.loading = true;
		$scope.items = [];
		
		let idxStart = (($scope.currentPage - 1) * NUM_OF_ITEM_PER_PAGE) +  1;

		$http.get( __env.apiUrlPath + '/users/arsip/inbox/'+idxStart+'/'+NUM_OF_ITEM_PER_PAGE+'?dariTgl=&sampaiTgl=&noSurat=&noAgenda=&perihal=&pengirim=&kodeLoker=&jenisSurat=&klasMasalah=&klasifikasi=&prioritas=&penerima=&tembusan=').then(function(response){
			$scope.items = response.data.data;
			$scope.totalItems = response.data.totalcount;
			
			$scope.loading = false;
		}, function(error){
			console.log('XXX error getting document in inbox');
			$scope.loading = false;
		});
	}
	
	$scope.changeContent();
	
	$ctrl.selectedRefs = function() {
		if(isMobile) {
			$scope.showReferencePanel = false;
			$scope.showSelectedRefPanel = true;
		} else {
			$scope.showReferencePanel = true;
			$scope.showSelectedRefPanel = true;
		}
	}
	
	$ctrl.viewRefList = function() {
		if(isMobile) {
			$scope.showReferencePanel = true;
			$scope.showSelectedRefPanel = false;
		} else {
			$scope.showReferencePanel = true;
			$scope.showSelectedRefPanel = true;
		}
	}
	
	$ctrl.isSelected = function(noSurat) {
		return $scope.referenceSubject.indexOf(noSurat) >= 0;
	}
	
	$ctrl.previewDoc = function(ref) {
		$http.get( __env.apiUrlPath + '/letters/inbox/'+ref.id).then(
				function (response) {
					$scope.selectedDoc = response.data.data;
					if(!isMobile) {
						$http.get( __env.apiUrlPath + '/letters/takah/'+response.data.data.takahtype+'/'+response.data.data.id+'?dbLoc=agenda').then(
							function(takah) {
								$scope.selectedDoc.takah = takah.data.data.html_takah;
							}, function(error) {
								console.log('XXX error mengambil takah ', takah);
								alert('Error menampilkan dokumen');
							}
						);
					}
					
					$scope.showPreview = true;
				},
				function (error) {
					console.log('XXX error getting document with id '+ref.id);
					alert('Error getting document');
				}
		);
	};
	
	$ctrl.closePreview = function() {
		$scope.showPreview = false;
		$scope.showPreviewDoc = null;
	}
	
	$ctrl.selectRef = function(UNID, noSurat) {
		console.log('XXX selected: '+UNID+'*'+noSurat);
		let idx = $scope.referenceSubject.indexOf(noSurat);
		if(idx < 0) {
			$scope.referenceList.push(UNID);
			$scope.referenceSubject.push(noSurat);
			$scope.referenceData.push('');
		} else {
			$scope.referenceList.splice(idx, 1);
			$scope.referenceSubject.splice(idx, 1);
			$scope.referenceData.splice(idx, 1);
		}
		
		console.log('XXX UNID ', $scope.referenceList);
		console.log('XXX No ', $scope.referenceSubject);
		console.log('XXX data ', $scope.referenceData);
	}
	
	$ctrl.ok = function () {
		$uibModalInstance.close({list: $scope.referenceList, subject: $scope.referenceSubject, data: $scope.referenceData});
	};

	$ctrl.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
}]);

/* nyoba address book */

routerApp.controller('ModalDemoCtrl',['$scope','$uibModal','$log','$document','$http','$compile','$injector', function ($scope, $uibModal, $log, $document,$http,$compile,$injector) {
	  
	var $ctrl = this;
	 
	  $ctrl.open = function (mode, target, temp) {
	      var modalInstance = $uibModal.open({
	      animation: $ctrl.animationsDisabled,
	      ariaLabelledBy: 'modal-title',
	      ariaDescribedBy: 'modal-body',
	      templateUrl: 'pages/forms/addressbook.html',
	      controller: 'ModalInstanceCtrl',
	      controllerAs: '$ctrl',
	      backdrop: 'static',
	      resolve: {
	    	mode : function(){return mode;},
	        temp: function(){return temp;}
	      }
	    });
	      
	  modalInstance.result.then(function (selectedItem) {
		  
		  var element = document.getElementById(target);
		  if(mode == "multiple"){
			  element.value = selectedItem.join("\n");
			  
		  }else if(mode == "single"){
			  element.value = selectedItem;
			  //temp[0] = selectedItem;
		  }    
	    		   
	  }, function () {
		  //console.log('Modal dismissed at: ' + new Date());
	});   
	      
	};
	    
	}]);

routerApp.controller('ModalInstanceCtrl', ['$scope','$http','$uibModalInstance','mode','temp','$document', '__env', function ($scope,$http,$uibModalInstance,mode,temp,$document, __env) {
	
	var $ctrl = this;
	$scope.loading= false;
	$scope.getMaincat = function(){
		$scope.loading= true;
		$http.get( __env.apiUrlPath + '/addrbook/maincat').then(function(response){
			$scope.maincat = response.data.data;
			$scope.loading= false;
		}, function(error){
			console.log('XXX error getting document in inbox');
			$scope.loading = false;
		});
		
	}
	$scope.getMaincat();
	$scope.temp = temp;
	//console.log("temp : ",temp);
	$scope.pilihan = $scope.temp.slice(0);
	
	$scope.ab_mode = mode; 
	$scope.isMobile = isMobile;
	
	$scope.collapsed = true;
	$scope.addressbook = {};
	$scope.addressbook.searchKeyApi = '';
	$scope.searchResult = [];
	$scope.limitListPersonal = 10;
		$scope.search = function(){
			if($scope.addressbook.searchKeyApi != ""){
				$http.get( __env.apiUrlPath + '/addrbook/all/1/50/'+$scope.addressbook.searchKeyApi,{cache:true}).then(function(response){
					$scope.searchResult = response.data.data;
		  		}, function(error){
					console.log('error search di addressbook');
				});
			}
		}
		
		$scope.personal = function(){
			$scope.loading = true;
				$http.get( __env.apiUrlPath + '/addrbook/all/1/999',{cache:true}).then(function(response){
					$scope.persons = response.data.data;
					$scope.loading = false;
		  		});
		}
			
			$scope.tambah_pilihan = function(kontak, $filter){
				console.log("XXX pilihan: ", $scope.pilihan);
				console.log(kontak.jabatan);
				if($scope.ab_mode == "single"){
					//console.log("push single");
					$scope.pilihan.pop();
					$scope.pilihan.push(kontak.jabatan);
					$ctrl.ok();
				}else{
					console.log("push multiple");
					let idx = $scope.pilihan.indexOf(kontak.jabatan);
					if ($scope.pilihan.indexOf(kontak.jabatan) < 0) {
						$scope.pilihan.push(kontak.jabatan);
						//console.log($scope.pilihan);
				     }else{
				    	 $scope.hapus_pilihan(idx);
				     }
				}

			};
			
			$scope.hapus_pilihan = function(index){
				console.log(index);
				$scope.pilihan.splice(index,1);
				console.log("pilihan : ",$scope.pilihan);
			};
			
			$scope.isSelected = function(kontak) {
				//kontak.isSelected = $scope.pilihan.indexOf(kontak.jabatan) >= 0;
				return $scope.pilihan.indexOf(kontak.jabatan) < 0 ? false:true;
			}
			
			
			$scope.loadmore = function(){
				$scope.limitListPersonal = $scope.limitListPersonal + 10;
			}
			
			$scope.toggle = function(obj, parent1, parent2, parent3, parent4){
				console.log(obj);
				console.log("parent : "+parent1);
				if(obj.expanded){
					console.log("is collapsed");
					console.log(obj);
					$scope.collapse(obj, parent1, parent2, parent3, parent4);
					obj.expanded = false;
					
				}else{
					console.log("is not collapsed");
					console.log(obj);
					$scope.expand(obj, parent1, parent2, parent3, parent4);
					obj.expanded = true;
				}
			}
			
			$scope.expand = function(catobj, parent1, parent2, parent3, parent4){
				console.log("expanded : "+catobj.expanded);
				if(catobj != null && parent1 != null && parent2 != null && parent4 != null){
					$http.get( __env.apiUrlPath + '/addrbook/cat/'+parent1+'/'+parent2+'/'+parent3+'/'+parent4+'/'+catobj.jabatan).then(function(response){
						$scope.catlist = response.data.data;
						catobj.nodes = $scope.catlist;
			  		  });
				}else{
					if(catobj != null && parent1 != null && parent2 != null && parent3 != null){
						$http.get( __env.apiUrlPath + '/addrbook/cat/'+parent1+'/'+parent2+'/'+parent3+'/'+catobj.jabatan).then(function(response){
							$scope.catlist = response.data.data;
							catobj.nodes = $scope.catlist;
				  		  });
					}else
						{
						if(catobj != null &&parent1 != null && parent2 != null){
							$http.get( __env.apiUrlPath + '/addrbook/cat/'+parent1+'/'+parent2+'/'+catobj.jabatan).then(function(response){
								$scope.catlist = response.data.data;
								catobj.nodes = $scope.catlist;
					  		  });
						}else{
							if(catobj != null && parent1 != null){
								$http.get( __env.apiUrlPath + '/addrbook/cat/'+parent1+'/'+catobj.jabatan).then(function(response){
									$scope.catlist = response.data.data;
									catobj.nodes = $scope.catlist;
						  		  });
							}else{
								if(catobj != null){
									$http.get( __env.apiUrlPath + '/addrbook/cat/'+catobj.category).then(function(response){
										$scope.catlist = response.data.data;
										catobj.nodes = $scope.catlist;
						  		  	});
								}
							}
						}
						}
				}
			}		
			
			$scope.collapse = function(catobj, parent1, parent2, parent3, parent4){
				console.log(catobj);
				catobj.nodes = [];
				
			};
			
			
			  $ctrl.ok = function () {
				  while($scope.temp.length > 0) {
						$scope.temp.pop();
				  }
				  for(let i= 0;$scope.pilihan.length > i;i++){
						$scope.temp.push($scope.pilihan[i]);
				  }
				  //console.log("temp ok : ",$scope.temp);
				  $uibModalInstance.close($scope.pilihan);
			  };

			  $ctrl.cancel = function () {
				
				while($scope.pilihan.length > 0) {
					$scope.pilihan.pop();
				}
				for(let i= 0;$scope.temp.length > i;i++){
					$scope.pilihan.push($scope.temp[i]);
			  	}
				
			    $uibModalInstance.dismiss();
			  };
			
			
	}]);
routerApp.controller('ContentCtrl',['$scope',function($scope){
	$scope.$on('LOAD',function(){$scope.loading=true});
	$scope.$on('UNLOAD',function(){$scope.loading=false});
}]);


/*---------Klasifikasi Masalah-------------*/

//klasifikasi service factory
routerApp.factory('KlasifikasiFactory', ['$http', '__env', function($http, __env) {
	 
	return {
		/* dapetin maincatnya aja */
		getMaincat : function(cache) {
		var url = __env.apiUrlPath + '/klasifikasi/maincat';
		console.log("getmaincat service cache" + cache);
			   return $http.get(url,cache).then(function(response){
	  				return response.data.data;
	  			});
	   },
	   /* service expand category */
	   getCat : function(cats,cache) {
		   
		   var url = __env.apiUrlPath + '/klasifikasi/cat';
		   for(var i=0;i<cats.length;i++){
			   url += "/"+cats[i];
		   }
		   return $http.get(url,cache).then(function(response){
		   					 return response.data.data;
		   				 });
	   }
	}
	 }]);


routerApp.controller('KlasifikasiCtrl', function ($uibModal, $log, $document) {
 var $ctrl = this;

 $ctrl.open = function (model) {
     var modalInstance = $uibModal.open({
     animation: $ctrl.animationsDisabled,
     ariaLabelledBy: 'modal-title',
     ariaDescribedBy: 'modal-body',
     templateUrl: 'pages/forms/klasifikasi.html',
     controller: 'ModalKlasifikasiCtrl',
     controllerAs: '$ctrl',
     backdrop: 'static'
   });
     
     // fungi yg bakal dijalanin setelah klik fungsi ok
     modalInstance.result.then(function (selectedItem) {
		  console.log("result : ",selectedItem);
		  model.classificationText = selectedItem.kode + ' - ' + selectedItem.topik;
		  model.kode = selectedItem.kode;
		  model.topik = selectedItem.topik;
		  model.classificationText = selectedItem.kode + ' - ' + selectedItem.topik;
	    		   
	  }, function () {
		  console.log('Modal dismissed at: ' + new Date());
	}); 
     
 };
});
routerApp.controller('ModalKlasifikasiCtrl', function ($scope,$uibModalInstance, $http, KlasifikasiFactory, __env) {
	 var $ctrl = this;
	 $scope.collapsed = true;
	 $scope.klasifikasi = {};
		$scope.klasifikasi.searchKey = "";
		$ctrl.pilihan_klasifikasi = {};
		
		$scope.loading= false;
		$scope.getMaincat = function(){
			$scope.loading= true;
			$http.get( __env.apiUrlPath + '/klasifikasi/maincat').then(function(response){
				$scope.maincat = response.data.data;
				$scope.loading= false;
			}, function(error){
				console.log('XXX error getting document in klasifikasi');
				$scope.loading = false;
			});
			
		}
		
		$scope.getMaincat();
		$scope.search = function(){
			if($scope.klasifikasi.searchKey != ""){
				$http.get( __env.apiUrlPath + '/klasifikasi/all/1/50/'+$scope.klasifikasi.searchKey,{cache:true}).then(function(response){
					$scope.searchResult = response.data.data;
		  		},function(error){
					console.log('error Search di klasifikasi');
				});
			}
		}
		
			$scope.tambah_pilihan = function(klasifikasi, $filter){
				console.log(klasifikasi.kode);
				$ctrl.pilihan_klasifikasi = klasifikasi;
				//setelah klasifikasi dipilih langsung jalanin fungsi tombol ok
				$ctrl.ok();
			};

			$scope.toggle = function(obj, parent1, parent2, parent3, parent4){
				console.log(obj);
				console.log("parent : "+parent1);
				if(obj.expanded){
					console.log("is collapsed");
					console.log(obj);
					$scope.collapse(obj);
					obj.expanded = false;
					
				}else{
					console.log("is not collapsed");
					console.log(obj);
					$scope.expand(obj, parent1, parent2, parent3, parent4);
					obj.expanded = true;
				}
			}
			
			$scope.expand = function(catobj, parent1, parent2, parent3, parent4){
				console.log("expanded : "+catobj.expanded);
				//console.log(catobj.category+"/"+parent1+ "/"+parent2+ "/"+parent3+ "/"+parent4);
				var cats = [];
				if(catobj != null){
					if(parent1 == null){
						cats = [catobj.category];
						//console.log("Cats1 : " + cats);
					}else if(parent2 == null){
						cats = [parent1,catobj.kode];
						//console.log("Cats2 : " + cats);
					}else if(parent3 == null){
						cats = [parent1,parent2,catobj.kode];
						//console.log("Cats3 : " + cats);
					}else if(parent4==null){
						cats = [parent1, parent2, parent3,catobj.kode];
						//console.log("Cats4 : " + cats);
					}else{
						cats = [parent1, parent2, parent3, parent4, catobj.kode];
						//console.log("Cats else : " + cats);
					}
				}
				
				catobj.nodes = KlasifikasiFactory.getCat(cats,{cache:true}).then(function(response){
					catobj.nodes = response;
	  		  	});
				
			}
			
			$scope.collapse = function(catobj){
				console.log(catobj);
				catobj.nodes = [];
				
			};

	  $ctrl.ok = function () {
	    //alert("ok");
		  $uibModalInstance.close($ctrl.pilihan_klasifikasi);
	  };

	  $ctrl.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	  };  
	  
	});



routerApp.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      var keys = Object.keys(props);

      items.forEach(function(item) {
        var itemMatches = false;

        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});

/*===DATEPICKER DIRECTIVE===*/
routerApp.directive('myformat', function(dateFilter) {
  return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModel) {
  		ngModel.$parsers.push(function(viewValue) {
       	return dateFilter(viewValue, 'MM/dd/yyyy');
			});
		}
	}
});

// comparing between two input
routerApp.directive('compareTo', function() {
	return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue === scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
});

routerApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = true;
}])

/*===DATEFORMATER DIRECTIVE===*/
routerApp.directive('dateFormater', function(moment) {
	// formate the string date based on format in attrs.format using momen.js
	function link (scope, element, attrs){
		const DATE_FORMAT_SERVER = 'MM/DD/YYYY hh:mm:ss A';
		
		// tgl dengan format '12/01/2017 12:00:00 AM' memiliki panjang 22 karakter
		let tgl = attrs.dateFormater.substring(0, 22);
		let tglObj = new Date(tgl);
		
		// if format is empty, use default format
		let dateFormat = attrs.format || 'DD MMM YYYY HH:mm';
		if(isMobile) {
			dateFormat = attrs.format || 'DD MMM \'YY';
		}
		
		let obj = moment(tgl, DATE_FORMAT_SERVER);
		
		element.text(obj.format(dateFormat));
	}
  return {
	link: link
  }
});

routerApp.directive('isRead', function() { 	
	return {   
		'scope':false,
	 	'link': function (scope, element, attrs){
	 			scope.$watch(attrs.isRead,function(style){
	 				
	 				if(!style == 0){
	 					return ;
	 				}
	 				attrs.$set('style','cursor:pointer;font-weight:bold');
	 				//alert("isbold");
	 			});
	 	}
	};
});


routerApp.directive("backButton", ['$window','$rootScope','$state', function ($window,$rootScope,$state) {
    return {
        restrict: "A",
        link: function (scope, elem, attrs) {
    		// State yang akan di bypass jika klik tombol back
    		let forms = ['nodin','memo','surat','detail-wait','buat-disposisi','buat-tanggapan', 'detail-draft'];
            elem.bind("click", function () {
            	//previous state tidak kosong
            	if($rootScope.previousState != ""){
            		//previous state adalah form
            		if(forms.indexOf($rootScope.previousState) < 0){
            			//previous state bukan form
            			$window.history.back();
                	}else{
                		//previous state adalah form
                		//dan current state adalah agenda
                		if($rootScope.currentState == 'agenda'){
                			//
	                		switch ($rootScope.previousState){
		                		case 'nodin':
		                			if($rootScope.currentParam.location == "process"){
		                				$state.go("status");
		                			}else if($rootScope.currentParam.location == "outbox"){
		                				$state.go("outbox",{filter:"nota"});
		                			}else if($rootScope.currentParam.location == "draft"){
		                				$state.go("draft");
		                			}
		                			break;
		                		case 'memo':
		                			if($rootScope.currentParam.location == "process"){
		                				$state.go("status");
		                			}else if($rootScope.currentParam.location == "outbox"){
		                				$state.go("outbox",{filter:"memo"});
		                			}else if($rootScope.currentParam.location == "draft"){
		                				$state.go("draft");
		                			}
		                			break;
		                		case 'surat':
		                			if($rootScope.currentParam.location == "process"){
		                				$state.go("status");
		                			}else if($rootScope.currentParam.location == "outbox"){
		                				$state.go("outbox",{filter:"surat"});
		                			}else if($rootScope.currentParam.location == "draft"){
		                				$state.go("draft");
		                			}
		                			break;
		                			
		                		//prevnya perlu persetujuan
		                		case 'detail-wait':
		                			$state.go('wait');
		                			break;

		                		case 'detail-draft':
		                			$state.go('draft');
		                			break;
		                		case 'buat-disposisi':
		                			$state.go('outbox',{filter:'disposisi'});
		                			break;
		                		case 'buat-tanggapan':
		                			$state.go('outbox',{filter:'disposisi'});
		                			break;
		                		default : 
		                			$state.go($rootScope.previousState,$rootScope.previousParam);
		                			break;
	                		}
                		}else{
                			$state.go($rootScope.previousState,$rootScope.previousParam);
                		}	
                	}
            	}else{
            		//saat diload/reload pertama kali, backnya lari ke index
            		//$state.go('index');
            		$window.history.back();
            	}
            	
            });
        }
    };
}]);

routerApp.directive('dynamic', function ($compile) {
	  return {
	    restrict: 'A',
	    replace: true,
	    link: function (scope, ele, attrs) {
	      scope.$watch(attrs.dynamic, function(html) {
	        ele.html(html);
	        $compile(ele.contents())(scope);
	      });
	    }
	  };
	});

routerApp.directive('scrolly', function () {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            var raw = element[0];
                
            element.bind('scroll', function () {
                if (raw.scrollTop +raw.offsetHeight > raw.scrollHeight) {
                    raw.scrollTop = raw.scrollHeight-10;
                    scope.$apply(attrs.scrolly);
                    
                }
            });
        }
    };
});

routerApp.directive('pieChart', function(){
    return{
        restrict: 'E',
        link: function(scope, elem, attrs){

            var chart = null,
            options = {
            		series: {
            			pie: {
               				show: true,
               				label: {
                        		show: true,
                        		radius: 1,
                        		formatter: function (label, series) {
                        			var element = '<div style="background-color:rgba(245,245,245,0.5);font-size:10pt;text-align:center;padding:2px;color:'+series.color+'">'+ series.data[0][1] + ' '+label+'</div>';
                        			return element;
                    			}
                    		}
                    
                		}
            	},
            colors: ["#001F3F","#D81B60", "#605ca8"],
            legend: {
         	   show: true
        	}
	           };

            var data = scope[attrs.ngModel];

            // If the data changes somehow, update it in the chart
            scope.$watch('data', function(v){
                 if(!chart){
                    chart = $.plot(elem, v , options);
                    elem.show();
                }else{
                    chart.setData(v);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
});

/*Single Page*/
routerApp.config(function($stateProvider, $urlRouterProvider) {
    
    $urlRouterProvider.otherwise('/index');
    
    $stateProvider
        
        // INDEX ========================================
        .state('index', {
            url: '/index',
            templateUrl: 'pages/tables/index-unread.html',
            resolve: {
        		agenda: function($http, __env){
		        	return $http.get(__env.apiUrlPath + '/users/self/inbox/letters/agendaunread/1/10',{cache:false})
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});	
        		},
                disposisi: function($http, __env){
        			return $http.get(__env.apiUrlPath + '/users/self/inbox/letters/dispounread/1/10',{cache:false})
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});
        		},
        		counts: function($http, __env){
        			return $http.get(__env.apiUrlPath + '/counts')
	   				 .then(function(response){
		   					return response.data.data;
		   				 },function(error){
				    		return error.data.errormsg;
				    });
        		}
              },
            controller: function($scope, $state, agenda, disposisi, counts, $http){
            	$scope.isMobile = isMobile;
            	    $scope.agendaUnread = agenda;
            	    $scope.dispoUnread = disposisi;
            	    if (/Firefox/i.test(navigator.userAgent)) {
            	    	 $scope.hideChart = true;
            	    }else{
            	    	$scope.hideChart = false;
            	    }
            	    $scope.detailAgenda = function(surat){
            	    	if(surat.takahtype == 'SuratMasukManual'){
            	    		$state.go('agenda', {location:'inbox' ,id: surat.id});
            	    	}else{
            	  			$state.go('detail-inbox', {id: surat.id});
            	    	}
            	  		//alert(unid);
            	  	};
            	  	$scope.detailDispo = function(unid){
            	  		$state.go('detail-disposisi', {id: unid});
            	  		//alert(unid);
            	  	};
            	  	
            	  	if(isMobile == true) {
            	  		$scope.linkSurat = '#!/unread/surat';
            	  		$scope.linkDispo = '#!/unread/disposisi';
            	  	} else {
            	  		$scope.linkSurat = '#!/index#agendaunread';
            	  		$scope.linkDispo = '#!/index#disposisiunread';
            	  	}
            	  	
            	  	$scope.refresh = function(tipe){
            	  		if(tipe == 'agenda'){
	            	  		$http.get(__env.apiUrlPath + '/users/self/inbox/letters/agendaunread/1/10',{cache:false})
	    		    		.then(function(response){
	    		    			$scope.agendaUnread = response.data.data;
	    		    			
	    		    		},function(error){
	    		    			console.log(error.data.errormsg);
	    		    		});
            	  		}else if(tipe == 'disposisi'){
            	  			$http.get(__env.apiUrlPath + '/users/self/inbox/letters/dispounread/1/10',{cache:false})
        		    		.then(function(response){
        		    			$scope.dispoUnread = response.data.data;
        		    			
        		    		},function(error){
        		    			console.log(error.data.errormsg);
        		    		});
            	  		}	
            	  	}
            	  	
            	  	$scope.data = [{label:'Surat Masuk',data:counts.kotak_masuk.all},{label:'In Progress',data:counts.kotak_proses.inprogress},{label:'Completed',data:counts.kotak_proses.completed} ]
              
              }
        })
        
        //UNREAD LETTER & DISPOSISI
        .state('unread', {
        	url: '/unread/:type',
        	templateUrl: 'pages/tables/unread.html',
        	resolve : {
	        	agenda: function($http, __env){
		        	return $http.get(__env.apiUrlPath + '/users/self/inbox/letters/agendaunread/1/10')
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});	
				},
		        disposisi: function($http, __env){
					return $http.get(__env.apiUrlPath + '/users/self/inbox/letters/dispounread/1/10')
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});
				}
		    },
		    controller : function($scope, $location, $stateParams, agenda, disposisi){
            	//$scope.isMobile = isMobile;
        	    $scope.agendaUnread = agenda;
        	    $scope.dispoUnread = disposisi;
        	    if($stateParams.type == 'surat'){
        	    	$scope.displaySurat = true;
        	    	$scope.displayDispo = false;
        	    }else if($stateParams.type == 'disposisi'){
        	    	$scope.displaySurat = false;
        	    	$scope.displayDispo = true;
        	    }

        	    $scope.detailAgenda = function(unid){
        	  		$location.path("/detail-inbox/"+unid);
        	  		//alert(unid);
        	  	};
        	  	$scope.detailDispo = function(unid){
        	  		$location.path("/detail-disposisi/"+unid);
        	  		//alert(unid);
        	  	};
		    }
        })
        
        // BUAT SURAT KELUAR ========================================
        .state('surat', {
            url: '/surat?UNID&noSurat',
            templateUrl: 'pages/forms/surat.html',
            controller: ['$scope','$http','$stateParams','$state','$location','Upload','composeSvc', '$rootScope', '__env', function($scope, $http, $stateParams, $state, $location, Upload, composeSvc, $rootScope,__env){
	        	$scope.surat = {
	        			id : '',
	        			abstrak: '',
	        			kepada : [],
	        			kepadaEks : [],
	        			kota_tujuan : '',
	        			tembusanIn : [],
	        			tembusanEks : [],
	        			manualSendTo : [],
	        			fromtitle : [],
	        			pemeriksa: [],
	        			pemesan: [],
	        			pemesanCatt: '',
	        			subject: '',
	        			kode: '',
	        			topik: '',
	        			body: '',
	        			sifat: 'Biasa',
	        			atasnama: '',
	        			catatan: '',
	        			takahtype: 'SuratKeluar',
	        			tipeGenerik: '',
	        			attachment: [],
	        			referenceDocs: {list: []},
	        			referensi: [],
	        			referensiNo: [],
	        			workflow: '',
	        			action: '',
	        			classificationText:'',
	        			tandaTangan: 'Scan',
	        			attCount: '0'
	        	};
	        	
	        	$scope.tembusanIn = [];
	        	$scope.pemesan = [];
	        	$scope.fromtitle = [];
	        	$scope.pemeriksa = [];
	        	
	        	$scope.detailPengirim = {};
	        	
	        	// UNID dan No Surat dari dokumen yang ingin langsung dimasukan sebagai referensi
	        	$scope.mainRefDocs = $stateParams.UNID;
                $scope.mainRefDocsNo = $stateParams.noSurat;
	        	
	        	//Attachment
	        	$scope.uploadFiles = function ($files) {
        			$scope.surat.attachment = $scope.surat.attachment.concat($files);
        			$scope.surat.attCount = $scope.surat.attachment.length;
        			console.log('XXX attachment length ', $files.length);
        			console.log('XXX manual attachment ', $files);
        		}
        		
        		$scope.deleteFile = function ($index) {
        			$scope.surat.attachment.splice($index, 1);
        			$scope.surat.attCount--;
        		}
	        	
	        	//Set Date now
	        	$scope.date = new Date();
	        	
	        	if($scope.pemesan[0] === undefined){
        			$scope.pemesan[0] = '';
        		}
	        	
	        	$scope.resolveReferences = function(data) {
	        		data.list = data.list.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		data.data = data.data.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		//$scope.surat.referenceDocs = data;
	        		$scope.surat.referenceDocs.list.push(data.list.toString());
	        		$scope.surat.referensiNo = data.subject;
	        		$scope.surat.referensi = data.data;
	        	}
	        	
	        	//Detail Pengirim
	        	$scope.blur = function () {
	        		$http({
		        		url : __env.apiUrlPath + '/detailpengirim?pemesan=' + $scope.pemesan[0],
		        		method: 'post',
		        		headers: {'Content-Type': 'application/json'},
		        		data : JSON.stringify({
		        			data : {
		        				pengirim : $scope.fromtitle[0]
			        		}
		        		})
		        	}).then(function onSuccess(data, status, headers, config) {
		        		$scope.detailPengirim = data.data.data;
		        		console.log('nama pengirim');
		        		console.log($scope.detailPengirim);
		        	}).catch(function onError(data, status, headers, config) {
		        		$scope.status = status + ' ' + headers;
			       	});
	        	};
	        	
	        	$scope.compose = function(action, workflow) {
	        		if($scope.mainRefDocs) {
	        			$scope.surat.referenceDocs.list.unshift($scope.mainRefDocs);
	        			$scope.surat.referensiNo.unshift($scope.mainRefDocsNo);
	        		}
        			if($scope.surat.referenceDocs.list && $scope.surat.referenceDocs.list.length > 0) {
        				$http.get(__env.apiUrlPath + '/referensi?docunid='+$scope.surat.referenceDocs.list.toString(), {responseType: 'json'}).then(
        					function (response) {
        	        			$scope.surat.referensi = $scope.surat.referensi.concat(response.data.data.refDetail);
        	        			// remove duplicat
        	        			$scope.surat.referensi = $scope.surat.referensi.filter(function(value, idx, self){
        	        				return self.indexOf(value) == idx;
        	        			});
        	        			$scope.surat.referensiNo = $scope.surat.referensiNo.filter(function(value, idx, self){
        	        				return self.indexOf(value) == idx;
        	        			});
        						send(action, workflow);
        					}, function (error) {
        						console.log('XXX error getting references ', error);
        					}
        				);
        			} else {
        				//$scope.surat.referensi = [];
        				//$scope.surat.referensiNo = [];
        				send(action, workflow);
        			}
        		}
	        		    		
	        	function send(action,workflow){
	        		/*var kpdEks = document.getElementById('kpdEks').value;
	    			var temEks = document.getElementById('temEks').value;
	    			$scope.surat.manualSendTo = kpdEks.split('\n');
	    			$scope.surat.tembusanEks = temEks.split('\n');
	    			console.log('Kepada Eks : ', kpdEks);
	    			console.log('Tembusan Eks : ', temEks);*/
	       			$scope.surat.tembusanIn = $scope.tembusanIn;
	    			$scope.surat.fromtitle = $scope.fromtitle;
	        		$scope.surat.pemeriksa = $scope.pemeriksa;
	    			$scope.surat.pemesan = $scope.pemesan;
        			$scope.surat.workflow = workflow;
	        		$scope.surat.action = action;
	        		
	        		/*if($scope.surat.pemeriksa.length == 0){
	        			if($scope.surat.fromtitle[0] != $scope.infoUser.jabatan){
		        			$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		        		}else{
		        			$scope.surat.pemeriksa = [];
		        		}
	        		} else if ($scope.surat.pemeriksa.length == 1) {
	        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[0]){
	        				$scope.surat.pemeriksa.splice(i,1);
	        			}else{
	        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	        			}
	        		} else{
		        		for(var i=0; i<$scope.surat.pemeriksa.length; i++){
		        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[i]){
		        				$scope.surat.pemeriksa.splice(i,1);
		        			}else{
		        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		        			}
		        		}
	        		}*/
        			console.log("Surat Keluar ", $scope.surat);
        			if(action == "Submit"){
    	        		swal({
	        			  title: "<i class='fa fa-paper-plane-o fa-4x text-primary'></i>",
	        			  text: "Apakah anda yakin ingin mengirim surat ini?",
						  html:true,
	        			  showCancelButton: true,
	        			  closeOnConfirm: false,
	        			  showLoaderOnConfirm: true
	        			},
	        			function(){
	        				if($scope.surat.pemeriksa.length == 0){
	    	        			if($scope.surat.fromtitle[0] != $rootScope.profile.jabatan){
	    		        			$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	    		        		}else{
	    		        			$scope.surat.pemeriksa = [];
	    		        		}
	    	        		} else if ($scope.surat.pemeriksa.length == 1) {
	    	        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[0]){
	    	        				$scope.surat.pemeriksa.splice(i,1);
	    	        			}else{
	    	        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	    	        			}
	    	        		} else{
	    		        		for(var i=0; i<$scope.surat.pemeriksa.length; i++){
	    		        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[i]){
	    		        				$scope.surat.pemeriksa.splice(i,1);
	    		        			}else{
	    		        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	    		        			}
	    		        		}
	    	        		}
	        				var postResult = composeSvc.postData($scope.surat, $scope.detailPengirim, 'SuratKeluar');
	        				postResult.then(function(response) {
	        					$scope.response = response;
	        					console.log('XXX cek response1: ', $scope.response);
	        					if($scope.response.status != 'error'){
	        						if($scope.response.data.MDStatus == 'Complete'){
	        							$scope.response.flow = 'Berhasil Dikirim';
	        							$scope.response.message = 'Surat berhasil dikirim';	
	        							$scope.response.location = 'outbox';
	        							//$scope.response.redirect_path = '/agenda/outbox/'+$scope.response.data.id;
	    							}else{
	    								$scope.response.flow = 'Berhasil Diproses';
	    								$scope.response.message = 'Surat sedang dalam proses persetujuan pemeriksa'	
	    								$scope.response.location = 'process';
		        						//$scope.response.redirect_path = '/agenda/process/'+$scope.response.data.id;
	    							}
	        						
	        						swal({
		        							title:'Terkirim', 
	        								text: $scope.response.message,
	        								timer: 1500,
	        								showConfirmButton: false,
	        								type: "success"
        								},
        								function(dismiss){
	        								if (dismiss == 'timer') {
	        									swal.close();
	        									return $state.go('agenda',{location:$scope.response.location,id:$scope.response.data.id});
	        								
	        								} else {
	        									swal.close();
	        									return $state.go('agenda',{location:$scope.response.location,id:$scope.response.data.id});
	        								}
	        							});
		        				}else{
		        					swal({
		        						title:"Error",
		        						text: $scope.response.message,
		        						type: "error"
		        					}, function () {
		        						return $location.path('index');
		        					});
		        				}
	        				}, function(error) {
	        					console.log('XXX error promise: ', error);
	        				});
	        				console.log('XXX cek response2: ', $scope.response);
	        			});
	        		}else if(action == "Draft"){
	        			swal({
		        			  title: "Simpan ke dalam draft",
		        			  text: "Apakah anda ingin menyimpan surat ini?",
		        			  type: "warning",
		        			  showCancelButton: true,
		        			  closeOnConfirm: false,
		        			  showLoaderOnConfirm: true
		        			},
		        			function(){
		        				var draftResult = composeSvc.postData($scope.surat, $scope.detailPengirim, 'SuratKeluar');
		        				draftResult.then(function (response) {
		        					$scope.response = response;
		        					if($scope.response.status != 'error'){
	        							swal({
	        								title:'Berhasil disimpan!',
	        								text: "Surat berhasi disimpan ke dalam draft",
	        								timer: 1500,
	        								showConfirmButton: false,
	        								type: "success"
	        							},
	        							function(dismiss){
	        								if (dismiss == 'timer') {
	        									swal.close();
	        									return $state.go('agenda',{location:'draft',id:$scope.response.data.id});
	        								} else {
	        									swal.close();
	        									return $state.go('agenda',{location:'draft',id:$scope.response.data.id});
	        								}
	        							});
	        						}else{
		        						swal({
		        							title:"Error",
		        							text: $scope.response.message,
		        							type: "error"
		        						}, function () {
			        						return $location.path('index');
			        					});
	        						}
		        				}, function(error){
		        					console.log('Error promise : ',error);
		        				});
		        			});
	        		}
	        	}
	        }]
        })

        // BUAT NOTA DINAS ========================================
        .state('nodin', {
            url: '/nodin?UNID&noSurat&takahType',
            templateUrl: 'pages/forms/nodin.html',
            controller: ['$scope','$http','$stateParams','$state','$location', '$rootScope','Upload','composeSvc', '__env', function($scope, $http, $stateParams, $state, $location, $rootScope,Upload, composeSvc, __env){
	        	$scope.surat = {
        			id : '',
        			abstrak: '',
        			kepada : [],
        			tembusanIn : [],
        			tembusanEks : [],
        			manualSendTo : [],
        			kota_tujuan : '',
        			fromtitle : [],
        			pemeriksa: [],
        			pemesan: [],
        			pemesanName: '',
        			pemesanCatt: '',
        			subject: '',
        			kode: '',
        			topik: '',
        			body: '',
        			catatan: '',
        			MDDueDate: null,
        			sifat: 'Biasa',
        			balas: '',
        			atasnama: '',
        			takahtype: 'NotaDinas',
        			tipeGenerik: $stateParams.takahType,
        			attachment: [],
        			referenceDocs: {'list': []},
        			referensi: [],
        			referensiNo: [],
        			workflow: '',
        			action: '',
        			classificationText:'',
        			tandaTangan: 'Scan',
        			attCount: '0',
        			MDJenisSMM: ''
	        	};
	        	$scope.detailPengirim = {};
	        	$scope.title = '';
                $scope.isDueDateOpen = false;
                $scope.dateOptions2 = {
                        minDate : new Date(),
                        formatDay: 'dd',
                        formatMonth: 'MM',
                        formatYear: 'yyyy',
                        showWeeks : false
                    };
                let index = nodin.findIndex(x => x.param === $stateParams.takahType);
                console.log('Index ', index);
                $scope.title = nodin[index].title;
                $scope.surat.MDJenisSMM = $scope.title;
             // UNID dan No Surat dari dokumen yang ingin langsung dimasukan sebagai referensi
                $scope.mainRefDocs = $stateParams.UNID;
                $scope.mainRefDocsNo = $stateParams.noSurat;
                
                var oneDay = 24*60*60*1000;
                $scope.dueDateChange = function() {
                    // surat rahasia tidak terkait langsung dengan due date 
                    if($scope.surat.sifat === 'Rahasia')
                        return;
                    
                    var receivedDate = new Date();
                    var dueDate = new Date($scope.surat.MDDueDate);
                    var diff = (dueDate - receivedDate) / oneDay;
                    
                    if(diff <= 1)
                        $scope.surat.sifat = 'Sangat Segera';
                    else if(diff <= 4)
                        $scope.surat.sifat = 'Segera';
                    else
                        $scope.surat.sifat = 'Biasa';
                }
                
	        	$scope.kepadaRequired = function(){
	        		if($scope.surat.kepada == undefined){
	        			$scope.surat.kepada = [];
	        		}
	        		return $scope.surat.kepada.length <= 0;
	        	}
	        	
	        	$scope.resolveReferences = function(data) {
	        		data.list = data.list.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		data.data = data.data.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		//$scope.surat.referenceDocs = data;
	        		$scope.surat.referenceDocs.list.push(data.list.toString());
	        		console.log('ReferenceDocs gab : ', $scope.surat.referenceDocs.list);
	        		$scope.surat.referensiNo = data.subject;
	        		$scope.surat.referensi = data.data;
	        	}
	        	
	        	$scope.blur = function(){
	        		if($scope.surat.pemesan[0] == undefined){
	        			$scope.surat.pemesan[0] = '';
	        		}
	        		
		        	if($scope.surat.fromtitle[0]  != undefined){
			        		$http({
				        		url : __env.apiUrlPath + '/detailpengirim?pemesan=' + $scope.surat.pemesan[0],
				        		method: 'post',
				        		headers: {'Content-Type': 'application/json'},
				        		data : JSON.stringify({
				        			data : {
				        				pengirim : $scope.surat.fromtitle[0]
					        		}
				        		})
				        	}).then(function onSuccess(data, status, headers, config) {
				        		console.log("$scope.surat.fromtitle : ",$scope.surat.fromtitle);
				        		$scope.detailPengirim = data.data.data;
				        		console.log($scope.surat.fromtitle[0]);
				        		console.log($scope.detailPengirim);
				        	}).catch(function onError(data, status, headers, config) {
				        		$scope.status = status + ' ' + headers;
					       	});
		        	}
	        	}
	        	
	        	$scope.uploadFiles = function ($files) {
        			$scope.surat.attachment = $scope.surat.attachment.concat($files);
        			$scope.surat.attCount = $scope.surat.attachment.length; 
        		}
        		
        		$scope.deleteFile = function ($index) {
        			$scope.surat.attachment.splice($index, 1);
        			$scope.surat.attCount--;
        		}
	        	
	        	//Set Date now
	        	$scope.date = new Date();
	        		        	
        		$scope.compose = function(action, workflow) {
        			if($scope.mainRefDocs) {
	        			$scope.surat.referenceDocs.list.unshift($scope.mainRefDocs);
	        			$scope.surat.referensiNo.unshift($scope.mainRefDocsNo);
        			}
        			
        			if($scope.surat.referenceDocs.list && $scope.surat.referenceDocs.list.length > 0) {
        				$http.get(__env.apiUrlPath + '/referensi?docunid='+$scope.surat.referenceDocs.list.toString(), {responseType: 'json'}).then(
        					function (response) {
        	        			$scope.surat.referensi = $scope.surat.referensi.concat(response.data.data.refDetail);
        	        			console.log('Referensi ', $scope.surat.referensi);
        	        			console.log('Response referensi ', response.data.data.refDetail);
        	        			console.log('$scope.surat.referenceDocs.list : ', $scope.surat.referenceDocs.list.toString());
        	        			// remove duplicat
        	        			$scope.surat.referensi = $scope.surat.referensi.filter(function(value, idx, self){
        	        				return self.indexOf(value) == idx;
        	        			});
        	        			$scope.surat.referensiNo = $scope.surat.referensiNo.filter(function(value, idx, self){
        	        				return self.indexOf(value) == idx;
        	        			});
        						send(action, workflow);
        					}, function (error) {
        						console.log('XXX error getting references ', error);
        					}
        				);
        			} else {
        				//$scope.surat.referensi = [];
        				//$scope.surat.referensiNo = [];
        				send(action, workflow);
        			}
        		}
        			
        		function send(action, workflow){
        			
        			$scope.surat.workflow = workflow;
	        		$scope.surat.action = action;
	        		
	        		if(action == "Submit"){
	        		swal({
	        			  title: "<i class='fa fa-paper-plane-o fa-4x text-primary'></i>",
	        			  text: "Apakah anda yakin ingin mengirim nota dinas ini?",
						  html:true,
	        			  showCancelButton: true,
	        			  closeOnConfirm: false,
	        			  showLoaderOnConfirm: true
	        			},
	        			function(){
	        				console.log('$scope.surat.pemeriksa.length : ', $scope.surat.pemeriksa.length);
	        				if($scope.surat.pemeriksa.length == 0){
	    	        			if($scope.surat.fromtitle[0] != $rootScope.profile.jabatan){
	    		        			$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	    		        		}else{
	    		        			$scope.surat.pemeriksa = [];
	    		        		}
	    	        		} else if ($scope.surat.pemeriksa.length == 1) {
	    	        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[0]){
	    	        				$scope.surat.pemeriksa.splice(i,1);
	    	        			}else{
	    	        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	    	        			}
	    	        		} else{
	    		        		for(var i=0; i<$scope.surat.pemeriksa.length; i++){
	    		        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[i]){
	    		        				$scope.surat.pemeriksa.splice(i,1);
	    		        			}else{
	    		        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	    		        			}
	    		        		}
	    	        		}
	        				var postResult = composeSvc.postData($scope.surat, $scope.detailPengirim, 'NotaDinas');
	        				postResult.then(function(response) {
	        					$scope.response = response;
	        					console.log('XXX cek response1: ', $scope.response);
	        					if($scope.response.status != 'error'){
	        						if($scope.response.data.MDStatus == 'Complete'){
	        							$scope.response.flow = 'Berhasil Dikirim';
	        							$scope.response.message = $scope.title + ' berhasil dikirim';	
	        							$scope.response.location = "outbox";
	    							}else{
	    								$scope.response.flow = 'Berhasil Diproses';
	    								$scope.response.message = $scope.title + ' sedang dalam proses persetujuan pemeriksa';	
	    								$scope.response.location = "process";
	    							}
	        						
	        						swal({
	        							title:'Terkirim', 
        								text: $scope.response.message,
        								timer: 1500,
        								showConfirmButton: false,
        								type: "success"
        							},
        								function(dismiss){
	        								
	        							swal.close();
	        							return $state.go('agenda',{location:$scope.response.location,id:$scope.response.data.id});
	        							});
		        				}else{
		        					swal({
		        						title:"Error",
		        						text: $scope.response.message,
		        						type: "error"
		        					}, function(error){
			        					console.log('Error promise : ',error);
			        				});
		        				}
	        				}, function(error) {
	        					console.log('XXX error promise: ', error);
	        				});
	        				console.log('XXX cek response2: ', $scope.response);
	        			});
	        		}else if(action == "Draft"){
	        			swal({
		        			  title: "Simpan ke dalam draft",
		        			  text: "Apakah anda ingin menyimpan " + $scope.title + " ini?",
		        			  type: "warning",
		        			  showCancelButton: true,
		        			  closeOnConfirm: false,
		        			  showLoaderOnConfirm: true
		        			},
		        			function(){
		        				var draftResult = composeSvc.postData($scope.surat, $scope.detailPengirim, 'NotaDinas');
		        				draftResult.then(function (response) {
		        					$scope.response = response;
		        					if($scope.response.status != 'error'){
	        							swal({
	        								title:'Berhasil disimpan!',
	        								text: $scope.title + " berhasil disimpan ke dalam draft",
	        								type: "success",
	        								timer: 1500,
	        								showConfirmButton: false
	        							},
	        							function(dismiss){
	        								if (dismiss == 'timer') {
	        									swal.close();
	        									return $state.go('agenda',{location:'draft',id:$scope.response.data.id});
	        								} else {
	        									swal.close();
	        									return $state.go('agenda',{location:'draft',id:$scope.response.data.id});
	        								}
	        							});
	        						}else{
		        						swal({
		        							title:"Error",
		        							text: $scope.response.message,
		        							type: "error"
		        						});
	        						}
		        				}, function(error){
		        					console.log('Error promise : ',error);
		        				});	
		        			});
	        		}
	        	}
        	}]	
        })
        
        // BUAT MEMO =================================
        .state('memo', {
            url: '/memo?UNID&noSurat',
            templateUrl: 'pages/forms/memo.html',
            controller: ['$scope', '$http', '$stateParams', 'Upload', 'composeSvc', '$location','$state', '$rootScope', '__env', function($scope, $http, $stateParams, Upload, composeSvc, $location, $state, $rootScope, __env){
        		var klasifikasi = [];
	        	$scope.surat = {
	        		id : '',
	        		abstrak: '',
	        		kepada : [],
	        		tembusanIn : [],
        			fromtitle : [$rootScope.profile.jabatan],
        			manualSendTo : [],
        			tembusanEks : [],
        			pemesan : [],
        			kota_tujuan : '',
        			kode: '',
        			topik: '',
        			subject: '',
        			body: '',
        			pemeriksa: [],
        			sifat: '',
        			atasnama: '',
        			takahtype: 'Memo',
        			tipeGenerik: '',
        			attachment: [],
        			referenceDocs: {list: []},
        			referensi: [],
        			referensiNo: [],
        			workflow: '',
        			catatan: '',
        			action: '',
        			classificationText:'',
        			tandaTangan: 'Scan',
        			attCount: '0',
        			MDJenisSMM: 'Memo'
	        	};
	        	
	        	// UNID dan No Surat dari dokumen yang ingin langsung dimasukan sebagai referensi
                $scope.mainRefDocs = $stateParams.UNID;
                $scope.mainRefDocsNo = $stateParams.noSurat;
                console.log('mainRefDocs ', $scope.mainRefDocs);
                console.log('mainRefDocsNo ', $scope.mainRefDocsNo);
	        	
	        	$scope.kepadaRequired = function(){
	        		if($scope.surat.kepada == undefined){
	        			$scope.surat.kepada = [];
	        		}
	        		return $scope.surat.kepada.length <= 0;
	        	}
	        	
	        	$scope.click = function(){
	        		console.log($scope.surat);
	        	}
	        	$scope.detailPengirim = {};
	        	
	        	$scope.resolveReferences = function(data) {
	        		data.list = data.list.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		data.data = data.data.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		$scope.surat.referenceDocs = data;
	        		$scope.surat.referensiNo = data.subject;
	        		$scope.surat.referensi = data.data;
	        	}
	        	
	        	$scope.uploadFiles = function ($files) {
        			$scope.surat.attachment = $scope.surat.attachment.concat($files);
        			$scope.surat.attCount = $scope.surat.attachment.length;
        			console.log('XXX attachment length ', $files.length);
        			console.log('XXX manual attachment ', $files);
        		}
        		
        		$scope.deleteFile = function ($index) {
        			$scope.surat.attachment.splice($index, 1);
        			$scope.surat.attCount--;
        		}
	        	
	        	//Set Date now
	        	$scope.date = new Date();
	        	
	        	if($scope.surat.pemesan[0] == undefined){
        			$scope.surat.pemesan[0] = '';
        		}
	        	
	        	$http({
	        		url : __env.apiUrlPath + '/detailpengirim?pemesan=' + $scope.surat.pemesan[0],
	        		method: 'post',
	        		headers: {'Content-Type': 'application/json'},
	        		data : JSON.stringify({
	        			data : {
	        				pengirim : $scope.surat.fromtitle[0]
		        		}
	        		})
	        	}).then(function onSuccess(data, status, headers, config) {
	        		$scope.detailPengirim = data.data.data;
	        	}).catch(function onError(data, status, headers, config) {
	        		$scope.status = status + ' ' + headers;
		       	});
	        	
	        	$scope.compose = function(action, workflow) {
	        		if($scope.mainRefDocs) {
	        			$scope.surat.referenceDocs.list.unshift($scope.mainRefDocs);
	        			$scope.surat.referensiNo.unshift($scope.mainRefDocsNo);
        			}
	        		
        			if($scope.surat.referenceDocs.list && $scope.surat.referenceDocs.list.length > 0) {
        				$http.get(__env.apiUrlPath + '/referensi?docunid='+$scope.surat.referenceDocs.list.toString(), {responseType: 'json'}).then(
        					function (response) {
        	        			$scope.surat.referensi = $scope.surat.referensi.concat(response.data.data.refDetail);
        	        			// remove duplicat
        	        			$scope.surat.referensi = $scope.surat.referensi.filter(function(value, idx, self){
        	        				return self.indexOf(value) == idx;
        	        			});
        						send(action, workflow);
        					}, function (error) {
        						console.log('XXX error getting references ', error);
        					}
        				);
        			} else {
        				//$scope.surat.referensi = [];
        				//$scope.surat.referensiNo = [];
        				send(action, workflow);
        			}
        		}
	        	
	        	function send (action, workflow){
	        		
	        		$scope.surat.workflow = workflow;
	        		$scope.surat.action = action;
	        		$scope.response = {};
	        		
	        		if(action == "Submit"){
		        		swal({
	        			  title: "<i class='fa fa-paper-plane-o fa-4x text-primary'></i>",
	        			  text: "Apakah anda yakin ingin mengirim memo ini?",
						  html:true,
	        			  showCancelButton: true,
	        			  closeOnConfirm: false,
	        			  showLoaderOnConfirm: true
	        			},
	        			function(){
	        				var postResult = composeSvc.postData($scope.surat, $scope.detailPengirim, 'Memo');
	        				postResult.then(function(response) {
	        					$scope.response = response;
	        					console.log('XXX cek response1: ', $scope.response);
	        					if($scope.response.status != 'error'){
        							$scope.response.flow = 'Berhasil Dikirim';
        							$scope.response.message = 'Memo berhasil dikirim';	
        							$scope.response.redirect_path = '/agenda/outbox/'+$scope.response.data.id;
	        						
	        						swal({
	        							title:'Terkirim', 
        								text: $scope.response.message,
        								type: "success",
        								timer: 1500,
        								showConfirmButton: false
        							},
    								function(dismiss){
        								if (dismiss == 'timer') {
        									swal.close();
        									return $state.go('agenda',{location:'outbox',id:$scope.response.data.id});
        								} else {
        									swal.close();
        									return $state.go('agenda',{location:'outbox',id:$scope.response.data.id});
        								}
        							});
		        				}else{
		        						swal({title:"Error",
		        							text: $scope.response.message,
		        							type: "error"});
		        				}
	        				}, function(error) {
	        					console.log('XXX error promise: ', error);
	        				});
	        				console.log('XXX cek response2: ', $scope.response);	        				
	        			});
	        		}else if(action == "Draft"){
	        			swal({
		        			  title: "Simpan ke dalam draft",
		        			  text: "Apakah anda ingin menyimpan memo ini?",
		        			  type: "warning",
		        			  showCancelButton: true,
		        			  closeOnConfirm: false,
		        			  showLoaderOnConfirm: true
		        			},
		        			function(){
		        				var draftResult = composeSvc.postData($scope.surat, $scope.detailPengirim, 'Memo');
		        				draftResult.then(function (response) {
		        					$scope.response = response;
		        					if($scope.response.status != 'error'){
	        							swal({
	        								title:'Berhasil disimpan!',
	        								text: "Memo berhasi disimpan sebagai draft",
	        								type: "success",
	        								timer: 1500,
	        								showConfirmButton: false
	        							},
	        							function(dismiss){
	        								if (dismiss == 'timer') {
	        									swal.close();
	        									return $state.go('agenda',{location:'draft',id:$scope.response.data.id});
	        								} else {
	        									swal.close();
	        									return $state.go('agenda',{location:'draft',id:$scope.response.data.id});
	        								}
	        							});
	        						}else{
		        						swal({
		        							title:"Error",
		        							text: $scope.response.message,
		        							type: "error"
		        						});
	        						}
		        				}, function(error){
		        					console.log('Error promise : ',error);
		        				});
		        			});
	        		}
	        		
	        		console.log($scope.surat);	        		
	        	};
	        }]
        })
        
        // surat masuk manual
        .state('surat-manual', {
        	url: '/surat-manual',
        	templateUrl: 'pages/forms/surat-manual.html',
        	resolve: {
        		jenisSurat : function($http, __env){
    	        	return $http.get(__env.apiUrlPath + '/lookup/jenissmm').then(
    	        			function (response) {
    	        				return response.data.data;
    	        			}
    		        	);
    	        }
    		},
        	controller: ['$scope', 'moment', 'composeSvc', '$state', '$http', 'jenisSurat', function($scope, moment, composeSvc, $state, $http, jenisSurat) {
        		var romanMonths = ['','I','II','III','IV','V','VI','VII','VIII','IX','X','XI','XII'];
        		var monthIdx = parseInt(moment().format('M'));
		
        		$scope.noAgenda = '[klasifikasi]/[no.urut]/'+romanMonths[monthIdx]+'/'+moment().format('YYYY');
        		$scope.tglTerima = moment().format('DD-MMM-YYYY');
        		
        		$scope.isDatePickerOpen = false;
        		$scope.dateOptions = {
            		maxDate : new Date(),
            		formatDay: 'dd',
            		formatMonth: 'MM',
            		formatYear: 'yyyy',
            		showWeeks : false
            	};
        		        		
        		$scope.isDueDateOpen = false;
        		$scope.dateOptions2 = {
            		minDate : new Date(),
            		formatDay: 'dd',
            		formatMonth: 'MM',
            		formatYear: 'yyyy',
            		showWeeks : false
            	};
        		
        		$scope.isDateInvitation = false;
        		$scope.dateOptions3 = {
            		minDate : new Date(),
            		formatDay: 'dd',
            		formatMonth: 'MM',
            		formatYear: 'yyyy',
            		showWeeks : false
            	};
        		
        		$scope.surat = {
        			MDJenisSMM : 'Surat Dinas',
        			MDReceiveDate : moment().format('MM/DD/YYYY'),
        			MDNoFull : '',
        			MDTanggalSurat : null,
//        			MDKlasifikasiMasalah : '',
//        			MDKlasifikasiMasalahDesc : '',
        			hari: '',
        			tanggal: null,
        			tempat: '',
        			waktu: '',
        			MDFromName : '',
        			MDSendTo : [],
        			MDCopyTo : [],
        			Subject : '',
        			MDAttachCount : '',
        			MDType : 'Biasa',
        			MDDueDate: null,
        			MDInstruksi : '',
        			classificationText : '',
        			kode : '',
        			topik : '',
        			attachment : []
        		};
        		$scope.ext = [];
        		console.log('Jenis surat ', jenisSurat);
        		$scope.jenis = jenisSurat;
        		
        		$scope.cekNoIsExisting = function(){
        			if($scope.surat.MDNoFull.length > 1){
	        			$http.get(__env.apiUrlPath+'/ceknomor?nosurat='+$scope.surat.MDNoFull).then(function(response){
		        		
			        		$scope.isNumExist = response.data.data.isExisting;
			        	});
        			}else{
        				$scope.isNumExist = '';
        			}
        		}
        		
        		var oneDay = 24*60*60*1000;
        		$scope.dueDateChange = function() {
        			// surat rahasia tidak terkait langsung dengan due date 
        			if($scope.surat.MDType === 'Rahasia')
        				return;
        			
        			var receivedDate = new Date();
        			var dueDate = new Date($scope.surat.MDDueDate);
        			var diff = (dueDate - receivedDate) / oneDay;
        			
        			if(diff <= 1)
        				$scope.surat.MDType = 'Sangat Segera';
        			else if(diff <= 4)
        				$scope.surat.MDType = 'Segera';
        			else
        				$scope.surat.MDType = 'Biasa';
        		}
        		
        		$scope.uploadFiles = function ($files) {
        			$scope.surat.attachment = $scope.surat.attachment.concat($files);
        			//$scope.ext = $scope.ext.push($scope.surat.attachment.name.split('.').pop());
        			for (let i=0; i<$files.length; i++) {
        				$scope.ext.push($files[i].type);
        			}
        			console.log('XXX attachment length ', $files.length);
        			console.log('XXX manual attachment ', $files);
        			console.log('$scope.surat.attachment ', $scope.surat.attachment);
        			console.log('Extension ', $scope.ext);
        		}
        		
        		$scope.deleteFile = function ($index) {
        			$scope.surat.attachment.splice($index, 1);
        			$scope.ext.splice($index, 1);
        		}
        		
        		$scope.compose = function(action) {
        			if(action == 'Submit') {

		        		swal({
	        			  title: "<i class='fa fa-paper-plane-o fa-4x text-primary'></i>",
	        			  text: "Apakah anda yakin ingin mengirim Surat Manual ini?",
						  html:true,
	        			  showCancelButton: true,
	        			  closeOnConfirm: false,
	        			  showLoaderOnConfirm: true
	        			},
	        			function(){
	        				var postResult = composeSvc.postSuratManual($scope.surat);
	        				console.log('$scope.surat : ', $scope.surat);
	        				postResult.then(function(response) {
	        					$scope.response = response;
	        					console.log('XXX cek response1: ', $scope.response);
	        					if($scope.response.status != 'error'){
        							$scope.response.flow = 'Berhasil Dikirim';
        							$scope.response.message = 'Surat Manual berhasil dikirim';	
        							$scope.response.redirect_path = '/agenda/inbox/'+$scope.response.data.id;
	        						
	        						swal({
	        							title:'Terkirim', 
        								text: $scope.response.message,
        								type: "success",
        								timer: 1500,
        								showConfirmButton: false
        							},
    								function(dismiss){
        								if (dismiss == 'timer') {
        									swal.close();
        									return $state.go('outbox', {filter: 'all'});
        								} else {
        									swal.close();
        									return $state.go('outbox', {filter: 'all'});
        								}
        							});
		        				}else{
		        						swal({title:"Error",
		        							text: $scope.response.message,
		        							type: "error"});
		        				}
	        				}, function(error) {
	        					console.log('XXX error promise: ', error);
	        				});
	        				console.log('XXX cek response2: ', $scope.response);	        				
	        			});
        			}
        		}
        	}]
        })

        // PROFIL USER ===============================
        .state('profil', {
            url: '/profil',
            templateUrl: 'pages/tables/profile.html',
            resolve: {
		    	tandaTangan: function ($rootScope, $http) {
	        		return $http.get(__env.apiUrlPath+'/profpic?jenis=ttd').then(function(response){
		        		return response.data.data;
			        });
			    }
		    },
            controller: ['$scope', '$http', '$rootScope', 'Upload', '$location', '$window', '__env', 'tandaTangan', function($scope, $http, $rootScope, Upload, $location, $window, __env, tandaTangan){
        		// update profile di $rootScope agar view profil juga terupdate
	        	$http.get(__env.apiUrlPath+'/users/self/unread').then(function(response){
	        		$rootScope.profile = response.data.data;
		        });
	        	
	        	$scope.tandaTangan = tandaTangan;
        		console.log('Foto ', $scope.tandaTangan);
	        	
	        	$scope.ttd = null;
				console.log($scope.ttd);
				$scope.uploadTtd = function ($file) {
	    			$scope.ttd = $file;
	    			console.log('Tanda tangan : ', $scope.ttd);
	    		}
				
				console.log('DOCUNID TTD USER ', $rootScope.profile.ttdUNID);
				
				$scope.unggahTtd = function () {
					let formData = new FormData();
					formData.append('UNID', $scope.tandaTangan.docunid);
					formData.append('file', $scope.ttd);
					formData.append('__Click', 0);
					let xhr = new XMLHttpRequest();
	    			xhr.open('POST', __env.apiUrlPath + '/profil/sign');
	    			xhr.addEventListener('loadend', function (evt) {
	    				if (xhr.readyState === xhr.DONE) {
	    					if (xhr.status === 200) {
	    						console.log('XXX response ', xhr.response);
	    						$window.location.reload();
	    						$window.location.reload();
	    					}
	    				}
	    			});
	    			xhr.send(formData);
				}
	        }]
        })
        
        .state('profil-photo', {
        	url: '/profil-photo',
        	templateUrl: 'pages/tables/profile-photo.html',
        	controller: function($scope, $http, $state, __env) {
        		var cropper;
        		$scope.loading = false;
        		
	        	$http.get(__env.apiUrlPath+'/profpic').then(function(response){
	        		$scope.pic_unid = response.data.data.docunid;
	        	});
        		
	        	$scope.selectPhoto = function(file, idElement) {
        			if(file) {
	        			$scope.loading = true;
	        			
	        			var fileReader = new FileReader();
	        			fileReader.addEventListener('load', function() {
	        				$scope.loading = false;
	        				
	        				// jika cropper object sudah ada, berarti sudah ada gambar yang di-load. 
	        				// Harus dihancurkan terlebih dahulu agar gambar yang baru bisa ditampilkan
	        				// dengan baik
	        				if(cropper) {
	        					cropper.destroy();
	        				}
	        				
	        				cropper = new Croppie(document.getElementById(idElement), {
	        					url: fileReader.result,
	        					showZoomer: false,
	        					boundary: {width: 300, height: 300},
	        					viewport: {width: 200, height: 200, type: 'circle'}
	        				});
	        			});
	        			
	        			fileReader.readAsDataURL(file);
        			}
        		}
	        	
	        	$scope.updatePhoto = function() {
	        		if(cropper) {
	        			cropper.result({
	        				type: 'base64',
	        				size: {width: 100, height: 100}
	        			}).then(function(resp){

	        				var formData = new FormData();
	        				formData.append('UNID', $scope.pic_unid);
	        				formData.append('file', resp);

	        				var xhrReq = new XMLHttpRequest();
	        				xhrReq.open('POST', __env.apiUrlPath+'/profil/photo');
	        				xhrReq.addEventListener('loadend', function(evt){
	        					console.log('upload selesai ');
	        					
	        					if(xhrReq.status === 200) { 
	        						if(evt.target.response && evt.target.response.success) {
	        							$state.go('profil');
	        						} else {
	        							alert(evt.target.response.message);
	        						}
	        					} else {
	        						console.log("Response status: "+xhrReq.status+" "+xhrReq.statusText);
	        						alert('Gagal mengganti photo karena '+xhrReq.statusText+'. Coba beberapa saat lagi');
	        					}
	        				});
	        				xhrReq.addEventListener('error', function(evt){
	        					console.log('upload error', evt);
	        				});
	        				xhrReq.responseType = 'json';
	        				xhrReq.send(formData);
	        			});
	        		} else {
	        			console.log('XXX croppie is null');
	        			alert('Error memproses gambar');
	        		}
	        	}
	        }
        })
        
        .state('ganti-password', {
			url: '/ganti-password',
		    templateUrl: 'pages/tables/ganti-password.html',
		       	controller: function($scope, $http, $state, __env) {
					$scope.password1 = '';
					$scope.password2 = '';
					$scope.message = '';
					$scope.alertClass='';
					
			       	$scope.updatePassword = function() {
						if($scope.password1 === $scope.password2) {
							var data = {
								data: {new_pass: $scope.password1, confirm_pass: $scope.password2}
							};
							$http.post(__env.apiUrlPath + '/password', data).then(function(response) {
								$scope.message = response.data.data.errormsg;
								$scope.alertClass = 'alert alert-success';
								
								setTimeout(function(){
									$state.go('profil');
								}, 5000);
								
							}, function(error) {
								$scope.message = response.data.data.errormsg;
								$scope.alertClass = 'alert alert-danger';
							});
						}
					};
			       	
			       	$scope.cancel = function() {
			       		$state.go('profil');
			       	};
			    }
		})
		
		//Ganti pin ============================
		
		.state('ganti-pin', {
			url: '/ganti-pin',
		    templateUrl: 'pages/tables/ganti-pin.html',
		       	controller: function($scope, $http, $state, $rootScope, __env) {
					$scope.pin1 = '';
					$scope.pin2 = '';
					$scope.message = '';
					$scope.alertClass='';
					$scope.action = '';
					
					$http.get(__env.apiUrlPath + '/approvalpass').then(function(response){	
	    				if(response.data.data.hasOwnProperty('errormsg')){
	    						console.log("ada error msg");
	    						$scope.action = '?OpenForm&Seq=1&action=CreateDocument';
	    					}else{
	    						$scope.action = '/0/'+response.data.data.id+'?EditDocument';
	    					}
    					
    				});
					
					
					$scope.updatePin = function () {
		        		let formData = new FormData();
		        		formData.append('KeyApprovalPass', $rootScope.profile.id);
		    			formData.append('ApprovalPass', $scope.pin1);
		    			formData.append('__Click', 0);
		    			let xhr = new XMLHttpRequest();
		    			xhr.open('POST', __env.serverUrl + '/ndprofiles.nsf/fApprovePass'+$scope.action);
		    			xhr.addEventListener('loadend', function (evt) {
		    				if (xhr.readyState === xhr.DONE) {
		    					if (xhr.status === 200) {
		    						

		    							swal({
		    								title: 'Berhasil', 
		    								text: 'PIN berhasil diubah',
		    								type: "success",
		    								timer: 1500,
		    								showConfirmButton: false
		    							},
		    							function(dismiss){
		    								/*if (dismiss == 'timer') {
		    									swal.close();
		    									return $location.path('outbox/disposisi');
		    								} else {
		    									*/
		    									swal.close();
		    									return $state.go('profil');
		    								//}
		    							});
		    						} else {
		    							console.log('error');
		    						}
		    						
		    					
		    				}
		    			});
		    			xhr.send(formData);
		        	}
					
			       	
			       	$scope.cancel = function() {
			       		$state.go('profil');
			       	};
			    }
		})
		
		//UPLOAD TANDA TANGAN ====================
		.state('tanda-tangan', {
			url: '/tanda-tangan',
			templateUrl: 'pages/forms/ubah-ttd.html',
			controller: ['$scope', '$rootScope', '$state', '$http', '$rootScope', 'Upload', 'tandaTangan', '__env',function ($scope, $rootScope, $state, $http, $rootScope, Upload, tandaTangan, __env) {
				$scope.ttd = null;
				console.log($scope.ttd);
				$scope.uploadTtd = function ($file) {
	    			$scope.ttd = $file;
	    			console.log('Tanda tangan : ', $scope.ttd);
	    		}
				
				$scope.unggahTtd = function () {
					let formData = new FormData();
					formData.append('UNID', $rootScope.profile.ttdUNID);
					formData.append('file', $scope.ttd);
					let xhr = new XMLHttpRequest();
	    			xhr.open('POST', __env.apiUrlPath + '/profil/sign');
	    			xhr.addEventListener('loadend', function (evt) {
	    				if (xhr.readyState === xhr.DONE) {
	    					if (xhr.status === 200) {
	    						console.log('XXX response ', xhr.response);
	    					}
	    				}
	    			});
	    			xhr.send(formData);
				}
			}]
		})
        
        //KOTAK MASUK ============================
        .state('inbox', {
            url: '/inbox/:filter',
            templateUrl: 'pages/tables/all-inbox.html',
            resolve: {
        		total: function(inboxListSvc){
        					return inboxListSvc.getCounts();	
        				},
                data: function(inboxListSvc,$stateParams){
        					console.log($stateParams.filter);
        			var params= {
        					takahType:$stateParams.filter,
        					currPage : "1",
        					perPage : data_perpage,
        					searchKey : ""};
		        	
		        	return inboxListSvc.getData(params,{cache:false});
        		}
              },
            controller: ['$scope','data','total', 'inboxListSvc','$stateParams','$uibModal','$log', '$document', '$state', function($scope,data,total,inboxListSvc,$stateParams,$uibModal,$log, $document, $state){

            	$scope.pageTitle = getInboxFilterByName($stateParams.filter)[0].title;
            	$scope.isMobile = isMobile;
            	    $scope.dataSuratAll = data;
            	    $scope.total = total;
            	    $scope.searchKey = "";
                	$scope.searchKeyApi = "";
                	$scope.detailState = "detail-inbox";
                	if($stateParams.filter == "disposisi" || $stateParams.filter == "tanggapan"){
                		$scope.detailState = "detail-disposisi";
                		$scope.display = true;
                	}
                	
                	$scope.params= {takahType:$stateParams.filter,
        					currPage : 1,perPage : data_perpage,searchKey : "", 
        					maxSize:3, total:eval("total."+$stateParams.filter)};
            	    
            	    $scope.click = function(doc){
        	  			doc.isread = 1;
        	  			console.log('Doc ', doc);
        	  			if(doc.takahtype == 'SuratMasukManual' && $stateParams.filter !== "disposisi") {
            	  			$state.go('agenda', {location: 'inbox', id:doc.id});
            	  		} else {
            	  			$state.go($scope.detailState, {id: doc.id});
            	  		}
            	  	};
            	  	
            	  	var oneDay = 24*60*60*1000;
            	  	$scope.calcDueDate = function(theDate) {
            	  		if(theDate) {
            	  			var diff = Math.abs(new Date(theDate) - new Date()) / oneDay;
            	  			if(diff <= 1)
            	  				return 'danger';
            	  			else if(diff <= 2)
            	  				return 'warning';
            	  		}
            	  		return '';
            	  	}
            	  	
            	  	$scope.refresh = function(){
            	  		
            	  		inboxListSvc.getData($scope.params,{cache:false}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});
            	  		
            	  		inboxListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval("response."+$stateParams.filter);
            	  		});
            	  	}

            	  	
            	  	$scope.pageChanged = function() {
            	  		$("html, body").animate({ scrollTop: 0 }, 500);
            	  		inboxListSvc.getData($scope.params,{cache:true}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});    			 
            	  	};
            	  	
            	  	$scope.search = function (){
                		if($scope.searchKey !=""){
                			$scope.searchKeyApi = "?search="+$scope.searchKey;
                			$scope.params.searchKey = $scope.searchKeyApi;

                	  		inboxListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  			$scope.params.maxSize=0;
                    			$scope.params.total = 999;
                    			$scope.params.currPage = 1;
                	  		});
                  			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                    	}else{
                    		$scope.searchKeyApi = "";
                    		$scope.params.searchKey = $scope.searchKeyApi;
                    		$scope.params.currPage = 1;
                    		inboxListSvc.getCounts().then(function(response){
                    			$scope.params.total = eval("response."+$stateParams.filter);
                    			$scope.params.maxSize=3;
                	  		});
                	  		inboxListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});
                    	}
              			
              		};
              }]
        })

 
        
        //KOTAK KELUAR ALL ============================
        .state('outbox', {
            url: '/outbox/:filter',
            templateUrl: 'pages/tables/all-outbox.html',
            resolve: {
        		total: function(outboxListSvc){
        					return outboxListSvc.getCounts();	
        				},
                data: function(outboxListSvc,$stateParams){
        			var params= {
        					takahType:$stateParams.filter,
        					currPage : "1",
        					perPage : data_perpage,
        					searchKey : ""};
		        	return outboxListSvc.getData(params,{cache:false});
        		}
              },
            controller: function($scope, data,total, outboxListSvc,$stateParams,$uibModal, $http, $state, __env){
            	$scope.pageTitle = getOutboxFilterByName($stateParams.filter)[0].title;  	
            	$scope.isMobile = isMobile;
            	    $scope.dataSuratAll = data;
            	    $scope.total = total;
            	    $scope.searchKey = "";
                	$scope.searchKeyApi = "";
                	$scope.detailState = "detail-outbox";
                	if($stateParams.filter == "disposisi"){
                		$scope.detailState = "detail-disposisi";
                		$scope.display = true;
                	}
                	
                	$scope.params= {takahType:$stateParams.filter,
        					currPage : 1,perPage : data_perpage,searchKey : "", 
        					maxSize:3, total:eval("total."+$stateParams.filter)};
            	    
            	    $scope.click = function(doc){
            	    	if(doc.takahtype == 'SuratMasukManual' && $stateParams.filter !== "disposisi")
            	  			$state.go('agenda', {location: 'outbox', id: doc.id});
            	  		else
            	  			$state.go($scope.detailState, {id: doc.id});
            	  			//$location.path("/"+$scope.detailState+"/"+unid);
            	  		//alert(unid);
            	  	};
            	  	
            	  	$scope.showInfo = function(id){                		
                		$uibModal.open({
                        	templateUrl: 'info.html',
                        	controller: 'modalInfoCtrl',
                        	resolve: {
	                			info : function(){
		            	        	return $http.get(__env.apiUrlPath + '/letters/outbox/' + id +'/agenda').then(function(response){
		            					return response.data.data;
		            				});
		            	        }
		            		}
                        });
                	}
            	  	
            	  	$scope.refresh = function(){
            	  		
            	  		outboxListSvc.getData($scope.params,{cache:false}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});
            	  		
            	  		outboxListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval("response."+$stateParams.filter);
            	  		});
            	  		//$scope.dataSuratAll = inboxListSvc.getData(params,false);
            	  		
            	  	}

            	  	
            	  	$scope.pageChanged = function() {
            	  		$("html, body").animate({ scrollTop: 0 }, 500);
            	  		outboxListSvc.getData($scope.params,{cache:true}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});    			 
            	  	};
            	  	
            	  	$scope.search = function (){
                		if($scope.searchKey !=""){
                			$scope.searchKeyApi = "?search="+$scope.searchKey;
                			$scope.params.searchKey = $scope.searchKeyApi;
                			
                			outboxListSvc.getData($scope.params,{cache:true}).then(function(response){
                				$scope.params.currPage = 1;
                	  			$scope.params.maxSize=0;
                    			$scope.params.total = 999;
                				$scope.dataSuratAll = response;
                	  		});
                  			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                    	}else{
                    		$scope.searchKeyApi = "";
                    		
                    		$scope.params.searchKey = $scope.searchKeyApi;
                    		outboxListSvc.getCounts().then(function(response){
                    			$scope.params.total = eval("response."+$stateParams.filter);
                    			$scope.params.maxSize=3;
                        		$scope.params.currPage = 1;
                	  		});
                    		outboxListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});
                    	}
              			
              		};
              }
        })

        //KOTAK PROSES PERLU PERSETUJUAN=========================
        .state('wait', {
            url: '/involved/perlu-persetujuan',
            templateUrl: 'pages/tables/wait.html',
            resolve: {
        		total: function(involvedListSvc){
        					return involvedListSvc.getCounts();	
        				},
                data: function(involvedListSvc){
        			var params= {
        					view:"inbox",
        					status:"involved",
        					currPage : "1",
        					perPage : data_perpage,
        					searchKey : ""};
		        	
		        	return involvedListSvc.getData(params,{cache:false});
        		}
              },
            controller: ['$scope','data','total', 'involvedListSvc','$location','$stateParams',function($scope, data,total, involvedListSvc,$location, $stateParams){
        		$scope.pageTitle = "Persetujuan";
            	$scope.isMobile = isMobile;
            	    $scope.dataSuratAll = data;
            	    $scope.searchKey = "";
                	$scope.searchKeyApi = "";
                	$scope.detailState = "detail-wait";
                	$scope.display = "true";
                	console.log('Display', $scope.display);

                	$scope.params= {view:"inbox",status:"involved",
        					currPage : 1,perPage : data_perpage,searchKey : "", 
        					maxSize:3, total:total.involved};
                	
                	$scope.counter = {'involved':total.involved,'tobeapproved':total.tobeapproved};
                	
                	$scope.perluPersetujuan = function(persetujuan){
                		console.log("persetujuan : "+persetujuan);
                		$scope.params.status = persetujuan;
                		$scope.search();
                		//console.log(status);
                		involvedListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval("response."+persetujuan);
            	  			$scope.counter[persetujuan] = response[persetujuan];
            	  		});
                		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
                			$scope.dataSuratAll = response;
                		});
                		console.log($scope.dataSuratReturned);
                	}
            	    
            	    $scope.click = function(unid, status){
            	    	if (status === 'Returned -- Need Resubmit') {
            	    		$location.path("/detail-draft/" + unid);
            	    	} else {
            	    		$location.path("/"+$scope.detailState+"/"+unid);
            	    	}
            	  		//alert(unid);
            	  	};
            	  	$scope.showInfo = function(){
                		alert("show info");
                	}
            	  	
            	  	$scope.refresh = function(){
            	  		involvedListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval('response.'+$scope.params.status);
            	  			$scope.counter[persetujuan] = response[persetujuan];
            	  		});
            	  		
            	  		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});
            	  	}
	
            	  	$scope.pageChanged = function() {
            	  		$("html, body").animate({ scrollTop: 0 }, 500);
            	  		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});    			 
            	  	};
            	  	
            	  	$scope.search = function (){
                		if($scope.searchKey !=""){
                			$scope.searchKeyApi = "?search="+$scope.searchKey;
                			$scope.params.searchKey = $scope.searchKeyApi;
                			
                			involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.params.maxSize=0;
                    			$scope.params.total = 999;
                    			$scope.params.currPage = 1;
                				$scope.dataSuratAll = response;
                	  		});
                  			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                    	}else{
                    		$scope.searchKeyApi = "";
                    		$scope.params.searchKey = $scope.searchKeyApi;
                    		
                    		involvedListSvc.getCounts().then(function(response){
                    			$scope.params.total = eval('response.'+$scope.params.status);
                    			$scope.params.maxSize=3;
                        		$scope.params.currPage = 1;
                	  		});
                    		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});
                    	}
              			
              		};
              }]
        })
        
        //KOTAK PROSES PERLU FINALISASI=========================
        .state('finalisasi', {
            url: '/involved/perlu-finalisasi',
            templateUrl: 'pages/tables/finalisasi.html',
            resolve: {
        		total: function(involvedListSvc){
        					return involvedListSvc.getCounts();
        				},
                data: function(involvedListSvc){
        			var params= {
        					view:"inbox",
        					status:"finalisasi",
        					currPage : "1",
        					perPage : data_perpage,
        					searchKey : ""};
		        	
		        	return involvedListSvc.getData(params,{cache:false});
        		}
              },
            controller: ['$scope','data','total', 'involvedListSvc','$location','$stateParams', '$state', function($scope, data,total, involvedListSvc,$location, $stateParams, $state){
            	$scope.pageTitle = "Perlu Finalisasi";
            	$scope.isMobile = isMobile;
            	    $scope.dataSuratAll = data;
            	    $scope.searchKey = "";
                	$scope.searchKeyApi = "";
                	$scope.detailState = "detail-finalisasi";
                	$scope.display = "false";
                	console.log('Display', $scope.display);
                	
                	$scope.params= {view:"inbox",status:"finalisasi",
        					currPage : 1,perPage : data_perpage,searchKey : "", 
        					maxSize:3, total:total.needverification};
            	    
            	    $scope.click = function(unid){
            	  		$location.path("/detail-finalisasi/"+unid);
            	  		//alert(unid);
            	  	}
            	  	$scope.showInfo = function(){
                		alert("show info");
                	}
            	  	
            	  	$scope.refresh = function(){
            	  		
            	  		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});
            	  		
            	  		involvedListSvc.getCounts().then(function(response){
            	  			$scope.params.total = response.perlu_persetujuan;
            	  		});
            	  		
            	  	}

            	  	
            	  	$scope.pageChanged = function() {
            	  		$("html, body").animate({ scrollTop: 0 }, 500);
            	  		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});    			 
            	  	};
            	  	
            	  	$scope.search = function (){
                		if($scope.searchKey !=""){
                			$scope.searchKeyApi = "?search="+$scope.searchKey;
                			$scope.params.searchKey = $scope.searchKeyApi;
                			
                			involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.params.maxSize=0;
                    			$scope.params.total = 999;
                    			$scope.params.currPage = 1;
                				$scope.dataSuratAll = response;
                	  		});
                  			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                    	}else{
                    		$scope.searchKeyApi = "";
                    		$scope.params.searchKey = $scope.searchKeyApi;
                    		
                    		involvedListSvc.getCounts().then(function(response){
                    			$scope.params.total = response.perlu_persetujuan;
                    			$scope.params.maxSize=3;
                        		$scope.params.currPage = 1;
                	  		});
                    		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});
                    	}
              			
              		};
              }]
        })

        //KOTAK PROSES PEMESAN
        .state('pemesan', {
            url: '/involved/pemesan',
            templateUrl: 'pages/tables/pemesan.html',
            resolve: {
        		total: function(involvedListSvc){
        					return involvedListSvc.getCounts();	
        				},
                data: function(involvedListSvc){
        			var params= {
        					view:"involved",
        					status:"requestedIP",
        					currPage : "1",
        					perPage : data_perpage,
        					searchKey : ""};
		        	
		        	return involvedListSvc.getData(params,{cache:false});
        		}
              },
            controller: ['$scope','data','total', 'involvedListSvc','$location','$stateParams','$state',function($scope, data,total, involvedListSvc,$location, $stateParams,$state){
            	$scope.pageTitle = "Pemesan";
            	$scope.isMobile = isMobile;
            	    $scope.dataSuratAll = data;
            	    $scope.searchKey = "";
                	$scope.searchKeyApi = "";
                	$scope.detailState = "detail-status";
                	$scope.params= {view:"involved",status:"requestedIP",
        					currPage : 1,perPage : data_perpage,searchKey : "", 
        					maxSize:3, total:total.requestedIP};
                	
                	$scope.pemesan = function(status){
                		$scope.params.status = status;
                		$scope.search(status);
                		//console.log(status);
                		involvedListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval("response."+status);
            	  		});
                		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
                			$scope.dataSuratAll = response;
                		});
                		console.log($scope.dataSuratReturned);
                	}
                	
            	    $scope.click = function(unid){
            	    	//console.log('UNID ', unid);
            	    	$state.go($scope.detailState,{id: unid});
            	  		//$location.path("/detail-status/"+unid);
            	  		//alert(unid);
            	  	};
            	  	
            	  	$scope.showInfo = function(){
                		alert("show info");
                	}
            	  	
            	  	$scope.refresh = function(status){
            	  		$scope.params.status = status;
            	  		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});
            	  		
            	  		involvedListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval("response."+status);
            	  		});
            	  		
            	  	}

            	  	
            	  	$scope.pageChanged = function(status) {
            	  		$("html, body").animate({ scrollTop: 0 }, 500);
            	  		$scope.params.status = status;
            	  		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});    			 
            	  	};
            	  	
            	  	$scope.search = function (status){
                		if($scope.searchKey !=""){
                			$scope.searchKeyApi = "?search="+$scope.searchKey;
                			$scope.params.searchKey = $scope.searchKeyApi;
                			$scope.params.status = status;
                			involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.params.maxSize=0;
                    			$scope.params.total = 999;
                    			$scope.params.currPage = 1;
                				$scope.dataSuratAll = response;
                	  		});
                  			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                    	}else{
                    		$scope.searchKeyApi = "";
                    		$scope.params.searchKey = $scope.searchKeyApi;
                    		$scope.params.status = status;
                    		involvedListSvc.getCounts().then(function(response){
                    			$scope.params.total = eval("response."+status);
                    			$scope.params.maxSize=3;
                        		$scope.params.currPage = 1;
                	  		});
                    		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});
                    	}
              			
              		};
              }]
        })
        //KOTAK PROSES DRAFT
        .state('draft', {
            url: '/involved/draft',
            templateUrl: 'pages/tables/draft.html',
            resolve: {
    		total: function(involvedListSvc){
    					return involvedListSvc.getCounts();	
    				},
            data: function(outboxListSvc){
    			var params= {
    					takahType:"draft",
    					currPage : "1",
    					perPage : data_perpage,
    					searchKey : ""};
	        	return outboxListSvc.getData(params,{cache:false});
    		}
          },
        controller: function($scope, data,total, outboxListSvc,involvedListSvc,$location){
        	$scope.pageTitle = "Draft";  	
        	$scope.isMobile = isMobile;
        	    $scope.dataSuratAll = data;
        	    $scope.searchKey = "";
            	$scope.searchKeyApi = "";
            	$scope.detailState = "detail-draft";
            	
            	$scope.params= {takahType:"draft",
    					currPage : 1,perPage : data_perpage,searchKey : "", 
    					maxSize:3, total:eval(total.draft)};
        	    
        	    $scope.click = function(unid){
        	  		$location.path("/"+$scope.detailState+"/"+unid);
        	  		//alert(unid);
        	  	};
        	  	
        	  	$scope.showInfo = function(){
            		alert("show info");
            	}
        	  	
        	  	
        	  	$scope.refresh = function(){
        	  		
        	  		outboxListSvc.getData($scope.params,{cache:false}).then(function(response){
        	  			$scope.dataSuratAll = response;
        	  		});
        	  		
        	  		involvedListSvc.getCounts().then(function(response){
        	  			$scope.params.total = response.draft;
        	  		});
        	  		//$scope.dataSuratAll = inboxListSvc.getData(params,false);
        	  		
        	  	}

        	  	
        	  	$scope.pageChanged = function() {
        	  		$("html, body").animate({ scrollTop: 0 }, 500);
        	  		outboxListSvc.getData($scope.params,{cache:true}).then(function(response){
        	  			$scope.dataSuratAll = response;
        	  		});    			 
        	  	};
        	  	
        	  	$scope.search = function (){
            		if($scope.searchKey !=""){
            			$scope.searchKeyApi = "?search="+$scope.searchKey;
            			$scope.params.searchKey = $scope.searchKeyApi;
            			
            			outboxListSvc.getData($scope.params,{cache:true}).then(function(response){
            				$scope.params.currPage = 1;
            	  			$scope.params.maxSize=0;
                			$scope.params.total = 999;
            				$scope.dataSuratAll = response;
            	  		});
              			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                	}else{
                		$scope.searchKeyApi = "";
                		$scope.params.searchKey = $scope.searchKeyApi;
                		involvedListSvc.getCounts().then(function(response){
                			$scope.params.total = response.draft;
                			$scope.params.maxSize=3;
                    		$scope.params.currPage = 1;
            	  		});
                		outboxListSvc.getData($scope.params,{cache:true}).then(function(response){
            	  			$scope.dataSuratAll = response;
            	  		});
                	}
          			
          		};
          }
        })

        //STATUS
        .state('status', {
            url: '/involved/status',
            templateUrl: 'pages/tables/status.html',
            resolve: {
        		total: function(involvedListSvc){
        					return involvedListSvc.getCounts();	
        				},
                data: function(involvedListSvc){
        			var params= {
        					view:"involved",
        					status:"inprogress",
        					currPage : "1",
        					perPage : data_perpage,
        					searchKey : ""};
		        	
		        	return involvedListSvc.getData(params,{cache:false});
        		}
              },
            controller: ['$scope','data','total', 'involvedListSvc','$location','$state',function($scope, data, total, involvedListSvc,$location,$state){
            	$scope.pageTitle = "Status";  	
            	$scope.isMobile = isMobile;
            	//$scope.dataSuratAll = data
            	$scope.searchKey = "";
            	$scope.searchKeyApi = "";
                $scope.detailState = "detail-status";
                $scope.detailCompleted = "detail-outbox";
            	$scope.params = {view:"involved",status:"inprogress",
    					currPage : 1,perPage : data_perpage,searchKey : "", 
    					maxSize:3, total:total.inprogress};
            	$scope.counter = {
                        'inprogress': total.inprogress,
                        'completed': total.completed,
                        'returned': total.returned
                    }
                	$scope.status = function(status){
                		$scope.params.status = status;
                		$scope.search(status);
                		//console.log(status);
                		involvedListSvc.getCounts().then(function(response){
            	  			$scope.params.total = eval("response."+status);
            	  			$scope.counter[status] = response[status];
            	  		});
                		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
                			$scope.dataSuratAll = response;
                		});
                		
                		console.log($scope.dataSuratReturned);
                	}
                	
                	$scope.changeHash = function(status) {
                		  window.location.hash = "#/"+status;
                		};

                	    $scope.click = function(doc){
                	  		$state.go($scope.detailState,{id:doc.id});
                	  		//alert(unid);
                	  	};
                	  	$scope.clickCompleted = function(doc){
                	  		$state.go($scope.detailCompleted,{id:doc.id});
                	  		//alert(unid);
                	  	};
                	  	
                	  	$scope.refresh = function(status){
                	  		$scope.params.status = status;
                	  		involvedListSvc.getData($scope.params,{cache:false}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});
                	  		
                	  		involvedListSvc.getCounts().then(function(response){
                	  			$scope.params.total = eval("response."+status);
                	  		});

                	  	}

                	  	
                	  	$scope.pageChanged = function(status) {
                	  		$("html, body").animate({ scrollTop: 0 }, 500);
                	  		$scope.params.status = status;
                	  		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                	  			$scope.dataSuratAll = response;
                	  		});    			 
                	  	};
                	  	
                	  	$scope.search = function (status){
                    		if($scope.searchKey !=""){
                    			$scope.searchKeyApi = "?search="+$scope.searchKey;
                    			$scope.params.searchKey = $scope.searchKeyApi;
                    			
                    			involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                    	  			$scope.dataSuratAll = response;
                    	  		});
                      			console.log("search : "+$scope.searchKey+" -> "+$scope.searchKeyApi);
                        	}else{
                        		$scope.searchKeyApi = "";
                        		$scope.params.searchKey = $scope.searchKeyApi;
                        		involvedListSvc.getCounts().then(function(response){
                    	  			$scope.total = response;
                    	  		});
                        		involvedListSvc.getData($scope.params,{cache:true}).then(function(response){
                        			$scope.params.total = eval("response."+status);
                        			$scope.params.maxSize=3;
                            		$scope.params.currPage = 1;
                        		});
                        	}
                  			
                  		}; 	
              }]
        })

        //TOOLS DELEGASI
        .state('delegasi', {
            url: '/delegasi',
            templateUrl: 'pages/tables/delegasi.html',
            controller: function($http, $scope, $state, $location, __env){
	    		$http.get( __env.apiUrlPath + '/delegation/1/20' ).then(function(response){
	    			$scope.dataDelegasi = response.data.data;
	    		});
	    		
	    		$scope.click = function(unid){
         	  		$location.path("/lihat-delegasi/"+unid);
         	  	};
         	  	
         	  	$scope.batal = function(delegasiObj){
         	  		var confirmDelete = confirm('Hapus delegasi kepada '+delegasiObj.delegated+' ?');
         	  		if(confirmDelete) {
	         	  		$http({
    	        			url : __env.apiUrlPath + '/users/self/delegation/',
    	        			method : 'POST',
    	        			data : {
    		                    	data : {
    	        					  id : delegasiObj.id,
    			                      delegator: delegasiObj.delegator,
    			                      delegated: delegasiObj.delegated,
    			                      date_start: delegasiObj.date_start,
    			                      date_stop: delegasiObj.date_stop,
    			                      reason: delegasiObj.reason,
    			                      action: 'unset'
    			                    }
    		                    },
    		                headers: {'Content-Type': 'application/json'},
    		                responseType: 'json'
    	        		}).then(function onSuccess(response) {
    	        			if(response.data.data.errormsg) {
    	        				alert('Error: '+response.data.data.errormsg);
    	        			} else {
    	        				location.reload();
    	        			}
    	        		}, function onError(response) {
    	        			console.log("XXX error menghapus delegasi: ",response);
    	        		});	         	  		
         	  		}
         	  	}
	    	}
        })

        //TOOLS SEKRETARIS
        .state('sekretaris',{
            url: '/sekretaris/:jabatan',
            templateUrl: 'pages/forms/sekretaris.html',
            resolve: {
                data: function($http, $stateParams, __env){
        			return $http.get(__env.apiUrlPath + '/users/self/assistance/'+$stateParams.jabatan)
        			.then(function(response){
        				//console.log(response.data.data);
        				return response.data.data;
        			});
        		}
              },
            controller: ['$scope','$http','$stateParams','data', '$rootScope', '__env',function($scope, $http, $stateParams, data, $rootScope,__env){
        		$scope.sekretaris = data;
        		$scope.sekretaris.jabatansec = [];
        		console.log('Get Sekretaris : ', $scope.sekretaris);
        		if($scope.sekretaris.status == 'set'){
        			$scope.aktif = false;
        		}else{
        			$scope.aktif = $scope.aktif;
        		}
	        	
	        	$scope.aktif = function(){
	        		$http({
	        			url : __env.apiUrlPath + '/users/self/assistance/',
	        			method : 'POST',
	        			data : JSON.stringify({
		                    	data : {
			                      id: '',
			                      jabatanmain: $rootScope.profile.jabatan,
			                      jabatansec: $scope.sekretaris.jabatansec[0],
			                      status: 'set',
			                      isowner: 'yes'
			                    }
		                    }),
		                headers: {'Content-Type': 'application/json'}
	        		}).then(function onSuccess(response) {
	        			$scope.errormsg = response.data.data.errormsg || '';
	        			if(response.data.data.errormsg) {
	        				
	        			} else {
	        				$scope.aktif = !$scope.aktif;
		        			location.reload();
	        			}
	        			
	        		}, function onError(response) {
	        			console.log("XXX error: ", response);
	        		});
	        	}
	        	
	        	$scope.batal = function(status){
	        		$http({
	        			url : __env.apiUrlPath + '/users/self/assistance/',
	        			method : 'POST',
	        			data : JSON.stringify({
		                    	data : {
			                      id: '',
			                      jabatanmain: $rootScope.profile.jabatan,
			                      jabatansec: $scope.sekretaris.sekretaris,
			                      status: 'unset',
			                      isowner: 'yes'
			                    }
		                    }),
		                headers: {'Content-Type': 'application/json'}
	        		}).then(function onSuccess(data, status, headers, config) {
	        			console.log('Data batal ', data);
	        			$scope.aktif = $scope.aktif;
	        			location.reload();
	        		}).catch(function onError(data, status, headers, config) {
	        		    $scope.status = status + ' ' + headers;
	        		});
	        	}
	        }]
        })
        
        // tools unduh laporan
        .state('laporan', {
        	url: '/laporan',
        	templateUrl: 'pages/forms/laporan.html',
        	controller: function($scope, $http, $stateParams, dateFilter, $rootScope, __env) {
        		$scope.laporanRequest = {agenda: 'masuk', bentuk: 'standar', mulai: null, selesai: null};
        		$scope.popup1 = false;
        		$scope.popup2 = false;
				$scope.alertClass = '';
				$scope.message = '';
				$scope.dateOptions = {
	            		maxDate : new Date(),
	            		showWeeks : false
	            	};
				
				$scope.dateOptions2 = {
						minDate : $scope.laporanRequest.mulai,
	            		maxDate : new Date(),
	            		showWeeks : false
	            	};
				
				$scope.dateOnlyOne = false;
				
				$scope.cekStatus = function() {
					$scope.dateOnlyOne = ( $scope.laporanRequest.mulai || $scope.laporanRequest.selesai ) && !( $scope.laporanRequest.mulai && $scope.laporanRequest.selesai );
				}
				
				$scope.laporanRequest.id = $rootScope.profile.id;
    			$scope.laporanRequest.name = $rootScope.profile.full_name;
    			
        		$scope.open1 = function() {
        			$scope.popup1 = !$scope.popup1;
        		};
        		
        		$scope.open2 = function() {
        			$scope.popup2 = !$scope.popup2;
        		};
        		
	        	$scope.unduh = function() {
	        		var config = {
	                        headers : {
	                            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
	                        },
	                        responseType: 'arraybuffer'
	                    }
	        			        		
	        		var tmp = $scope.laporanRequest.tglAwal;
	        		
	        		$scope.laporanRequest.tglAwal = dateFilter($scope.laporanRequest.mulai, 'MM/dd/yyyy');
	        		$scope.laporanRequest.tglAkhir = dateFilter($scope.laporanRequest.selesai, 'MM/dd/yyyy');
	        		
	        		var params = $.param($scope.laporanRequest);
	        		
	        		$http.post(__env.apiUrlPath + '/laporan', params, config)
	        			.then(function successCallback(response) {
	        				$scope.alertClass = 'alert alert-success';
							$scope.message = 'Permintaan berhasil dikirim';
							
	        				// membaca response header
							var headers = response.headers();
	        				
							// mengambil nama file, jika ada
	        				var filename = "laporan.xlsx";
	        		        var disposition = headers['content-disposition'];
	        		        if (disposition && disposition.indexOf('attachment') !== -1) {
	        		            var filenameRegex = /filename[^;=\n]*=((['"]).*?\2|[^;\n]*)/;
	        		            var matches = filenameRegex.exec(disposition);
	        		            if (matches != null && matches[1]) filename = matches[1].replace(/['"]/g, '');
	        		        }
	        		        
	        		        // memproses stream yang dikirim oleh server
							var blob = new Blob([response.data], {type: headers['content-type']});
							
							// code di bawah ini adalah hack agar bisa mengunduh file melalui ajax post request
							// membuat html element link <a> yang merujuk ke file yang diterima dari server
							var link = document.createElement('a');
						    link.href = URL.createObjectURL(blob);
						    // nama file yang akan digunakan ketika save as
						    link.download = filename;
						    // display = none agar tidak ditampilkan ke user
						    link.style.display = 'none';
						    
						    // menambahkan ke body
						    document.body.appendChild(link);
						    
						    // panggil event click untuk mengunduh file
						    link.click();
						    
	        			}, function errorCallback(response){
	        				console.log("Gagal kirim form laporan ", response);

							$scope.alertClass = 'alert alert-error';
							$scope.message = 'Gagal mengirim permintaan unduh laporan';
	        			});
	        	};
	        }
        })
        
        .state('arsip', {
        	url: '/arsip/:jenisDokumen',
        	templateUrl: 'pages/tables/arsip-masuk.html',
        	controller: function($scope, $http, $location, $stateParams, dateFilter, $rootScope, $state, __env) {
        		$scope.params = {
        			total: 0,
        			currPage: 1,
        			maxSize: 5,
        			perPage: 20
        		};
        		
        		let profile = $rootScope.profile;
    			
        		$scope.detailUrl = '';
        		if($stateParams.jenisDokumen === 'masuk') {
        			$scope.jenisDokumen = 'Masuk';
        			
        			if(profile.is_sps == 'yes')
        				$scope.letterBox = 'spsIn';
        			else
        				$scope.letterBox = 'inbox';
        			
        			$scope.detailUrl = '/detail-inbox';
        		} else if($stateParams.jenisDokumen === 'keluar') {
        			$scope.jenisDokumen = 'Keluar';
        			
        			if(profile.is_sps == 'yes')
        				$scope.letterBox = 'spsOu';
        			else
        				$scope.letterBox = 'outbox';
        			
        			$scope.detailUrl = '/detail-outbox';
        		} else if($stateParams.jenisDokumen === 'disposisi') {
        			$scope.jenisDokumen = 'Disposisi';
        			
        			if(profile.is_sps == 'yes')
        				$scope.letterBox = 'spsDisp';
        			else
        				$scope.letterBox = 'disposisi';
        			
        			$scope.detailUrl = '/detail-disposisi';
        		} else {
        			if(profile.is_sps == 'no') {
        				// redirect ke perlu persetujuan
        				return $state.go('wait');
        			}
        			
        			$scope.jenisDokumen = 'SPS';
        			$scope.letterBox = 'sps';
        			$scope.detailUrl = '/detail-status';
        		}
        		
        		$scope.showFilter = true;
        		$scope.open = {
        			'sort': false,
        			'date': false,
        			'view': false,
        			'other': false
        		};
        		
        		$scope.defaultFilter = {
        				sortBy: 'MDNoFull',
        				orderBy: 'Ascending',
            			dariTgl: null,
            			sampaiTgl: null,
            			noSurat: '',
            			noAgenda: '',
            			perihal: '',
            			pengirim: '',
            			kodeLoker: '',
            			jenisSurat: '',
            			klasMasalah: '',
            			klasifikasi: '',
            			prioritas: '',
            			penerima: '',
            			tembusan: ''
            		};
        		
        		$scope.filter = {
        			sortBy: 'MDNoFull',
        			orderBy: 'Ascending'
        		};
        		
        		$scope.datas = {};
        		
        		$scope.dateOptionsDari = {
        			    maxDate: new Date(),
        			    startingDay: 1
        			  };
        		
        		$scope.dateOptionsSampai = {
        			    minDate: $scope.filter.dariTgl,
        			    startingDay: 1
        			  };
        		
        		$scope.calendar = {
        			'dari': false,
        			'sampai': false
        		}
        		
        		$scope.dariTglOpen = function() {
        			$scope.calendar.dari = !$scope.calendar.dari;
        		}
        		
        		$scope.sampaiTglOpen = function() {
        			$scope.calendar.sampai = !$scope.calendar.sampai;
        		}
        		
        		$scope.filterArsip = function() {
        			var mergeFilter = $.extend({}, $scope.defaultFilter, $scope.filter);
        			mergeFilter.dariTgl = dateFilter(mergeFilter.dariTgl, 'MM/dd/yyyy');
        			mergeFilter.sampaiTgl = dateFilter(mergeFilter.sampaiTgl, 'MM/dd/yyyy');
        			
        			var parameterQuery = $.param(mergeFilter);
        			
        			var firstIdx = (($scope.params.currPage - 1) * 20) + 1;
        			$http.get(__env.apiUrlPath + '/users/arsip/'+$scope.letterBox+'/'+firstIdx+'/20?'+parameterQuery).then(function successCallback(response){
            			$scope.datas = response.data.data;
            			$scope.params.total = response.data.totalcount;
            		}, function errorCallback(response){
            			console.log('XXX data arsip masuk gagal', response);
            		});
        		}
        		
        		$scope.resetFilter = function() {
        			$scope.filter = $scope.defaultFilter;
        			$scope.filterArsip();
        		}
        		
        		$scope.lihatSurat = function(doc) {
        			if(doc.tipeGenerik !== ''){
        			console.log("XXX lihat surat: "+$scope.detailUrl+'/'+doc.id);
        				$location.path($scope.detailUrl+'/'+doc.id);
        			}else{
        				$location.path($scope.detailUrl+'/'+doc.id);
        			}
        		}
        		
//        		$http.get(this.url+'?'+$.param($scope.defaultFilter)).then(function successCallback(response){
        		$http.get(__env.apiUrlPath + '/users/arsip/'+$scope.letterBox+'/1/20?'+$.param($scope.defaultFilter)).then(function successCallback(response){
        			$scope.datas = response.data.data;
        			$scope.params.total = response.data.totalcount;
        		}, function errorCallback(response){
        			console.log('XXX data arsip masuk gagal', response);
        		});
	        }
        })
        
        //SETTINGS
        .state('settings', {
        	url: '/settings',
        	templateUrl: 'pages/settings.html'
        })

        //DETAIL INBOX
        .state('detail-inbox', {
        	url: '/detail-inbox/:id',
        	templateUrl: 'pages/mailbox/inbox.html',
        	resolve: {
                data: function($http,$stateParams, $state, __env){
		        	return $http.get(__env.apiUrlPath + '/letters/inbox/' + $stateParams.id)
		        	.then(function(response){
		        		if(response.data.data.errormsg !== '-'){
		        			console.log(response.data.data);
		        			swal({
								title: 'Terkunci', 
								text: 'Surat tidak dapat dibuka.',
								type: "error"
							},
							function(dismiss){
								swal.close();
							});
		        			return $state.go('index');
		        		}
		        		let referensiObj = [];
		        		for(let ref of response.data.data.referensi) {
		        			let tmp = ref.split('~');
		        			
		        			let refObj = {noSurat: tmp[0]};
		        			let jenisDok = tmp[tmp.length - 1];
		        			if(jenisDok == 'SuratKeluar') {
		        				refObj.takahType = 'sk';
		        			} else if(jenisDok == 'NotaDinas') {
		        				refObj.takahType = 'nd';
		        			} else if(jenisDok == 'SuratMasukManual') {
		        				refObj.takahType = 'SuratMasukManual';
		        			} else {
		        				refObj.takahType = 'me';
		        			}
		        			
		        			let storage = tmp[1].split('\\')[1];
		        			refObj.storage = storage;
		        			
		        			refObj.id = tmp[2];
		        			referensiObj.push(refObj);
		        		}
		        		for (var i = 0; i < referensiObj.length; i++) {
		        			referensiObj[i].agendaUNID = response.data.data.refagendaunid[i];
		        			console.log(referensiObj.length);
		        		}
		        		response.data.data.referensiObj = referensiObj;
		        		console.log('Data ', response.data.data);
			        	$http.get(__env.apiUrlPath + '/letters/takah/'+response.data.data.takahtype+'/'+response.data.data.id+'/'+response.data.data.tipeGenerik+'?dbLoc=agenda')
			        	.then(function(takah){
			        		response.data.data.html_takah = takah.data.data.html_takah;
			        		console.log(takah.data.data);
			        	},function errorCallback(response){
			        		alert('Data tidak ada');
			        	});
			        	console.log("2nd resp : ",response.data.data);
		        		return response.data.data;
		        	},function(error){
		        		return error.data.data;
		        		
		        	});
        		}
        		
        	},
        	controller: function($scope, data, $http, $uibModal, $state, __env){
        		$scope.isMobile = isMobile;        		
        		$scope.detailInbox = data;

	        	$scope.previewRef;
	        	
	        	$scope.print = function() {
  	        		if($scope.isMobile){
  	        			$scope.isMobile = false;
  	        			var beforePrint = function() {
  	        				$scope.isMobile = false;
  	        				console.log('Print Dialog before open..');
  	        				console.log('isMobile : ',$scope.isMobile);
  	        		    };

  	        		    var afterPrint = function() {
  	        		    	console.log('Print Dialog Closed..');
  	        		    	$scope.isMobile = true;
  	        		    	console.log('isMobile : ',$scope.isMobile);
  	        		    };
  	        		    
  	        		    if (window.matchMedia) {
  	        		        var mediaQueryList = window.matchMedia('print');
  	        		        mediaQueryList.addListener(function(mql) {
  	        		            if (mql.matches) {
  	        		            	
  	        		                beforePrint();
  	        		            } else {
  	        		            	
  	        		                afterPrint();
  	        		            }
  	        		        });
  	        		    }
  	        		  window.onbeforeprint = beforePrint;
  	        		  setTimeout(window.print,2000);
  	        		  window.onafterprint = afterPrint;
  	        		}else{
  	        			window.print();
  	        		}

  	        	}
	        	
	        	var versiMobile = (isMobile) ? 'yes' : 'no';
	        	
	        	$scope.previewReferensi = function(nomor, storage, unid, type, agendaId) {
	        		console.log('Tipe surat ', type);
	        		console.log('Storage ', storage);
	        		if (type === 'NotaDinas') {
	        			console.log('Tipe Nota Dinas');
	        			if (nomor == 'Und') {
	        				console.log('Masuk undangan');
	        				$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'/SuratUndangan?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then (
	        				function (response) {
	        					$scope.previewRef = response.data.data;
	        					$scope.agenda = {};
	        					$scope.agenda = $scope.previewRef;
	        					console.log('PreviewRef Agenda : ', $scope.previewRef);
	        					$scope.previewRef.takahType = type;
	        					var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
	        				}, function (error) {
	        					console.log('XXX error mengambil dokumen referensi');
	        					console.log(error);
	        				});
	        			} else if (nomor == 'ST') {
	        				console.log('Masuk surat tugas');
	        				$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'/SuratTugas?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then (
	        				function (response) {
	        					$scope.previewRef = response.data.data;
	        					$scope.agenda = {};
	        					$scope.agenda = $scope.previewRef;
	        					console.log('PreviewRef Agenda : ', $scope.previewRef);
	        					$scope.previewRef.takahType = type;
	        					var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
	        				}, function (error) {
	        					console.log('XXX error mengambil dokumen referensi');
	        					console.log(error);
	        				});
	        			} else if (nomor == 'ND') {
	        				console.log('Masuk nota dinas');
	        				$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'/NotaDinas?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then (
	        				function (response) {
	        					$scope.previewRef = response.data.data;
	        					$scope.agenda = {};
	        					$scope.agenda = $scope.previewRef;
	        					console.log('PreviewRef Agenda : ', $scope.previewRef);
	        					$scope.previewRef.takahType = type;
	        					var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
	        				}, function (error) {
	        					console.log('XXX error mengambil dokumen referensi');
	        					console.log(error);
	        				});
	        			}
	        		} else if (type === 'SuratMasukManual') {
	        			console.log('Agenda UNID : ', agendaId);
	        			$http.get(__env.apiUrlPath + '/letters/inbox/' + agendaId + '/agenda').then(
	        				function (response) {
	        					$scope.previewRef = response.data.data;
	        					$scope.agenda = {};
	        					$scope.agenda = $scope.previewRef;
	        					console.log('PreviewRef Agenda : ', $scope.previewRef);
	        					$scope.previewRef.takahType = type;
	        					var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
	        				}, function (error) {
	        					console.log('XXX error mengambil dokumen referensi');
	        					console.log(error);
	        				}
	        			);
	        		} else {
	        			$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'/SuratUndangan'+'?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then(
    	        			function (response) {
    	        				if(isMobile) {
    	        					$scope.previewRef = response.data.data;
    	        				} else {
    	        					$scope.previewRef = response.data.data.html_takah;
    	        				}
    	        				$scope.previewRef.takahType = type;
    	        				var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
    	        			}, function (error) {
    	        				console.log('XXX error mengambil dokumen referensi');
    	        			}
    	        		);
	        		}
	        	};
	        	
	        	$scope.closePreview = function(modal) {
	        		modal.$close();
	        	}
	        	
	        	
        		if($scope.detailInbox.fileattname[0] != "" && "fileattname" in $scope.detailInbox){
	        		$scope.detailInbox.attachments = $scope.detailInbox.fileattname.map(function(value, index) {
	      			    return {
	      			        name: value,
	      			        url: $scope.detailInbox.fileatturl[index]
	      			    }
	      			});
        		}
        		$scope.detailInbox.bookmarked = true;
        		$scope.toggleBookmark = function(){
        			$scope.detailInbox.bookmarked = $scope.detailInbox.bookmarked == true?false:true;
        				//service api bookmarknya disini
        				//console.log($scope.detailInbox.bookmarked);
        		}
        		/*
        		if($scope.detailInbox.takahtype == "nd"){
        			$scope.url = "//dev2.codephile.id/eoffice/ndxpage.nsf/notadinas.xsp?documentID="+$scope.detailInbox.storageid+"&action=openDocument";
        		}else if($scope.detailInbox.takahtype == "me"){
            		$scope.url = "//dev2.codephile.id/eoffice/ndxpage.nsf/memo.xsp?documentID="+$scope.detailInbox.storageid+"&action=openDocument";
        		}else if ($scope.detailInbox.takahtype == "sk"){
            		$scope.url = "//dev2.codephile.id/eoffice/ndxpage.nsf/suratkeluar.xsp?documentID="+$scope.detailInbox.storageid+"&action=openDocument";
        		}
        		*/
        		//console.log("map attachments");
        		//console.log($scope.detailInbox.attachments);
        		
	        }
        })
        
        //DETAIL OUTBOX
        .state('detail-outbox',{
            url: '/detail-outbox/:id',
            templateUrl: 'pages/mailbox/outbox.html',
            resolve: {
                data: function($http,$stateParams, __env){
		        	return $http.get(__env.apiUrlPath + '/letters/outbox/'+$stateParams.id)
		        	.then(function(response){
		        		let referensiObj = [];
		        		for(let ref of response.data.data.referensi) {
		        			let tmp = ref.split('~');
		        			
		        			let refObj = {noSurat: tmp[0]};
		        			
		        			let jenisDok = tmp[tmp.length - 1];
		        			if(jenisDok == 'SuratKeluar') {
		        				refObj.takahType = 'sk';
		        			} else if(jenisDok == 'NotaDinas') {
		        				refObj.takahType = 'nd';
		        			} else {
		        				refObj.takahType = 'me';
		        			}
		        			
		        			let storage = tmp[1].split('\\')[1];
		        			refObj.storage = storage;
		        			
		        			refObj.id = tmp[2];
		        			referensiObj.push(refObj);
		        		}
		        		response.data.data.referensiObj = referensiObj;
		        		
		        		$http.get(__env.apiUrlPath + '/letters/takah/'+response.data.data.takahtype+'/'+response.data.data.id+'/'+response.data.data.tipeGenerik+'?dbLoc=agenda')
			        	.then(function(takah){
			        		response.data.data.html_takah = takah.data.data.html_takah;
			        		console.log(takah.data.data);
			        	},function(error){
			        		//return error.data;
			        		alert('Data tidak ada');
			        	});

		        		return response.data.data;
		        	},function(error){
		        		return error.data.data.errormsg;
		        	});
        		}
        	},
            controller: function($scope, data, $http, $uibModal, __env){
        		$scope.isMobile = isMobile;
	        	$scope.detailOutbox = data;
	        	$scope.previewRef;
	        	
	        	$scope.print = function() {
  	        		if($scope.isMobile){
  	        			$scope.isMobile = false;
  	        			var beforePrint = function() {
  	        				$scope.isMobile = false;
  	        				console.log('Print Dialog before open..');
  	        				console.log('isMobile : ',$scope.isMobile);
  	        		    };

  	        		    var afterPrint = function() {
  	        		    	console.log('Print Dialog Closed..');
  	        		    	$scope.isMobile = true;
  	        		    	console.log('isMobile : ',$scope.isMobile);
  	        		    };
  	        		    
  	        		    if (window.matchMedia) {
  	        		        var mediaQueryList = window.matchMedia('print');
  	        		        mediaQueryList.addListener(function(mql) {
  	        		            if (mql.matches) {
  	        		            	
  	        		                beforePrint();
  	        		            } else {
  	        		            	
  	        		                afterPrint();
  	        		            }
  	        		        });
  	        		    }
  	        		  window.onbeforeprint = beforePrint;
  	        		  setTimeout(window.print,2000);
  	        		  window.onafterprint = afterPrint;
  	        		}else{
  	        			window.print();
  	        		}

  	        	}
	        	
	        	let versiMobile = (isMobile) ? 'yes' : 'no';
	        	
	        	$scope.previewReferensi = function(storage, unid, type) {
	        		$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then(
	        			function (response) {
	        				if(isMobile) {
	        					$scope.previewRef = response.data.data;
	        				} else {
	        					$scope.previewRef = response.data.data.html_takah;
	        				}
	        				
	        				var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
	        			}, function (error) {
	        				console.log('XXX error mengambil dokumen referensi');
	        			}
	        		);
	        		
	        	};
	        	
	        	$scope.closePreview = function(modal) {
	        		modal.$close();
	        	}
	        	
	        	if($scope.detailOutbox.pictunid == ""){
	        		$scope.detailOutbox.pictunid = __env.serverUrl + '/ndprofiles.nsf/nophoto01.jpg';
	        	}
	        		
	        	if($scope.detailOutbox.fileattname[0] != ""){
		        	$scope.detailOutbox.attachments = $scope.detailOutbox.fileattname.map(function(value, index) {
		        		return {
		      			        name: value,
		      			        url: $scope.detailOutbox.fileatturl[index]
		      			    }
		      			});
	        	}
	        }
        })
        
        //DETAIL DISPOSISI
        .state('detail-disposisi',{
            url: '/detail-disposisi/:id',
            templateUrl: 'pages/mailbox/disposisi.html',
            resolve: {
                data: function($http, $stateParams, __env){
			        	return $http.get(__env.apiUrlPath + '/disposisi/'+ $stateParams.id)
			        	.then(function(response){
		        			return $http.get(__env.apiUrlPath + '/letters/inbox/'+ response.data.data.letterid).then(function(surat){
		        				let referensiObj = [];
				        		for(let ref of surat.data.data.referensi) {
				        			let tmp = ref.split('~');
				        			
				        			let refObj = {noSurat: tmp[0]};
				        			
				        			let jenisDok = tmp[tmp.length - 1];
				        			if(jenisDok == 'SuratKeluar') {
				        				refObj.takahType = 'sk';
				        			} else if(jenisDok == 'NotaDinas') {
				        				refObj.takahType = 'nd';
				        			} else {
				        				refObj.takahType = 'me';
				        			}
				        			
				        			let storage = tmp[1].split('\\')[1];
				        			refObj.storage = storage;
				        			
				        			refObj.id = tmp[2];
				        			referensiObj.push(refObj);
				        		}
				        		surat.data.data.referensiObj = referensiObj;
				        		if(surat.data.data.takahtype != 'SuratMasukManual') {
			        				$http.get(__env.apiUrlPath + '/letters/takah/'+surat.data.data.takahtype+'/'+surat.data.data.id+'?dbLoc=agenda')
						        	.then(function(takah){
						        		response.data.data.html_takah = takah.data.data.html_takah;
						        		//console.log('Data takah :', takah.data.data.html_takah);
						        	},function errorCallback(response){
						        		alert('Data tidak ada');
						        	});
			        			}
		        				response.data.data.letter = surat.data.data;
		        				console.log('Data :', response.data.data);
			        			return response.data.data;
			        		},function(error){
			        			console.log(error.data.data.errormsg);
			        			return error.data.data.errormsg;
			        		});
		        		},function(error){
		        			console.log(error.data.data.errormsg);
		        			return error.data.data.errormsg;
		        		});
        			}

              },
            controller: function($scope, data, $uibModal, $http, __env, pdfDelegate){
            	  $scope.detailDisposisi = data;
            	  $scope.detailSuratDispo = data.letter;
            	  $scope.detailSuratDispo.html_takah = $scope.detailDisposisi.html_takah;
            	  $scope.previewRef;
            	  /*console.log('Detail Disposisi ', $scope.detailDisposisi);
            	  console.log('Detail Surat Disposisi ', $scope.detailSuratDispo);
            	  console.log('Takah ', $scope.detailDisposisi.html_takah);*/
            	  $scope.isMobile = isMobile;
            	  let versiMobile = (isMobile) ? 'yes' : 'no';
            	  $scope.previewReferensi = function(storage, unid, type) {
  	        		$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then(
  	        			function (response) {
  	        				if(isMobile) {
  	        					$scope.previewRef = response.data.data;
  	        				} else {
  	        					$scope.previewRef = response.data.data.html_takah;
  	        				}
  	        				
  	        				var modalInstance = $uibModal.open({
  	        				      ariaLabelledBy: 'modal-title',
  	        				      ariaDescribedBy: 'modal-body',
  	        				      templateUrl: 'myModalContent.html',
  	        				      size: 'lg',
  	        				      scope: $scope
  	        				    });
  	        			}, function (error) {
  	        				console.log('XXX error mengambil dokumen referensi');
  	        			}
  	        		);
  	        		
  	        	};
  	        	
  	        	$scope.closePreview = function(modal) {
  	        		modal.$close();
  	        	}
            	  
            	  if($scope.detailSuratDispo.fileattname[0] != ""){
  	        		$scope.detailSuratDispo.attachments = $scope.detailSuratDispo.fileattname.map(function(value, index) {
  	      			    return {
  	      			        name: value,
  	      			        url: $scope.detailSuratDispo.fileatturl[index]
  	      			    }
  	      			});
          		  }
            	  
            	  $scope.viewPdf = function() {
    	    			for(var att of $scope.detailSuratDispo.attachments) {
    	    				if(att.name.toLowerCase().endsWith('.pdf')) {
    	    					$scope.pdfFile = att.name;
    	    					$scope.previewPdf(att.url);
    	    					break;
    	    				}
    	    			}
    	    		}
    	    		
    	    		$scope.previewPdf = function(url) {
    	    			console.log('XXX loading PDF from ', url);
    	    			$scope.isPdfLoading = true;
    	    			
    	    			var pdfPromise = pdfDelegate.$getByHandle('my-pdf-container').load(url);
    	    			pdfPromise.then(function(){
    	    				console.log('XXX finish loading PDF');
    	    				$scope.isPdfLoading = false;
    	    			}).catch(function(reason){
    	    				console.log('XXX error loading PDF caused by ', reason);
    	    				$scope.isPdfLoading = false;
    	    			});
    	    		}
            	  
        	}
        })
        
        //DETAIL PERLU FINALISASI
        .state('detail-finalisasi', {
        	url: '/detail-finalisasi/:id',
        	templateUrl: 'pages/mailbox/finalisasi.html',
        	resolve: {
        		data: function ($http, $stateParams, __env) {
        			return $http.get(__env.apiUrlPath + '/letters/process/'+ $stateParams.id)
        			.then (function (response) {
        				console.log('Masuk API Detail dan Takah');
        				let referensiObj = [];
		        		for(let ref of response.data.data.referensi) {
		        			let tmp = ref.split('~');
		        			
		        			let refObj = {noSurat: tmp[0]};
		        			
		        			let jenisDok = tmp[tmp.length - 1];
		        			if(jenisDok == 'SuratKeluar') {
		        				refObj.takahType = 'sk';
		        			} else if(jenisDok == 'NotaDinas') {
		        				refObj.takahType = 'nd';
		        			} else if(jenisDok == 'SuratMasukManual') {
		        				refObj.takahType = 'sm';
		        			} else {
		        				refObj.takahType = 'me';
		        			} 
		        			
		        			let storage = tmp[1].split('\\')[1];
		        			refObj.storage = storage;
		        			
		        			refObj.id = tmp[2];
		        			referensiObj.push(refObj);
		        		}
		        		response.data.data.referensiObj = referensiObj;
		        		console.log('Referensi ', response.data.data.referensiObj);
        				$http.get(__env.apiUrlPath + '/letters/takah/'+response.data.data.takahtype+'/'+response.data.data.id+'/'+response.data.data.tipeGenerik+'?dbLoc=keluar')
			        	.then(function(takah){
			        		response.data.data.html_takah = takah.data.data.html_takah;
			        		console.log(takah.data.data);
			        	},function errorCallback(response){
			        		alert('Data tidak ada');
			        	});
        				console.log('Data ', response.data.data);
		        		return response.data.data;
        			}, function (error) {
        				return error.data.error_msg;
        			});
		        },
		        agenda: function ($http, $stateParams, __env) {
		        	return $http.get (__env.apiUrlPath + '/letters/process/'+ $stateParams.id + '/agenda')
		        	.then (function (response) {
		        		response.data.data.headline = 'Detail Agenda Surat Perlu Finalisasi';
		        		console.log('Agenda : ', response.data.data);
		        		return response.data.data;
		        	});
		        }
	        },
	        controller: function ($scope, data, $state, $http, $uibModal, __env) {
	        	$scope.detailFinalisasi = data;
	        	$scope.isMobile = isMobile;
        		console.log(data);
	        	$scope.previewRef;
	        	$scope.showField = false;
	        	$scope.nomor = '';
	        	$scope.print = function() {
	        		window.print();
	        	}
	        	
	        	let versiMobile = (isMobile) ? 'yes' : 'no';
	        	
	        	$scope.previewReferensi = function(storage, unid, type) {
	        		if (type === 'sm') {
	        			$http.get(__env.apiUrlPath + '/letters/inbox/'+data.id+'/agenda', {cache: true}).then (
	        				function (response) {
	        					$scope.previewRef = response.data.data;
	        				}
	        			);
	        		} else {
	        			$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then(
    	        			function (response) {
    	        				if(isMobile) {
    	        					$scope.previewRef = response.data.data;
    	        				} else {
    	        					$scope.previewRef = response.data.data.html_takah;
    	        				}    	        				
    	        			}, function (error) {
    	        				console.log('XXX error mengambil dokumen referensi');
    	        			}
    	        		);
	        		}
    				var modalInstance = $uibModal.open({
    				      ariaLabelledBy: 'modal-title',
    				      ariaDescribedBy: 'modal-body',
    				      templateUrl: 'myModalContent.html',
    				      size: 'lg',
    				      scope: $scope
    				    });
	        	};
	        	
	        	$scope.email = function () {
	        		var modalEmail = $uibModal.open({
        		      ariaLabelledBy: 'modal-title',
        		      ariaDescribedBy: 'modal-body',
        		      templateUrl: 'modalEmail.html',
        		      controller: 'modalEmailCtrl',
        		      size: 'md'
        		    });
	        	}
	        	
	        	$scope.finalisasi = function (id) {
	        		if($scope.nomor == ''){
	        			swal({
	  	        		  title: "Nomor surat",
	          			  text: "Apakah anda yakin tidak mengubah nomor surat?",
	          			  type: "warning",
	          			  showCancelButton: true,
	          			  closeOnConfirm: false
	          			},
	        			function () {
	  	        			$scope.doFinalize(id);
	  	        		});
	        		}else{
	        			$scope.doFinalize(id);			
	        		}
	        		
	        		
	        	}
	        	
	        	$scope.doFinalize = function(id){
	        		console.log("id :"+id);
	        		swal({
		        		  title: "Finalisasi surat",
	        			  text: "Apakah anda ingin finalisasi surat ini?",
	        			  type: "warning",
	        			  showCancelButton: true,
	        			  closeOnConfirm: false,
	        			  showLoaderOnConfirm: true
	        			},
	        			function () {
	        				console.log('Nomor ', $scope.nomor);
	    	        		let formData = new FormData();
	    	    			formData.append('isVerified', 'yes');
	    	    			formData.append('MDAction', 'Approve');
	    	    			formData.append('MDNoManual', $scope.nomor);
	    	    			formData.append('MDIsWorkflowRequired', 1);
	    	    			formData.append('__Click', 0);
	    	    			let xhr = new XMLHttpRequest();
	    	    			xhr.open('POST', __env.serverUrl + '/ndkeluar.nsf/0/' + id + '?EditDocument');
	    	    			xhr.addEventListener('loadend', function (evt) {
	    	    				if (xhr.readyState === xhr.DONE) {
	    	    					if (xhr.status === 200) {
	    	    						console.log('XXX response finalisasi : ', xhr.response);
	    	    						let resp = JSON.parse(xhr.response);
	    	    						console.log('XXX response finalisasi from server: ', resp);
	    	    						if (resp.data.status === 'Sukses') {
	    	    							swal({
	    	    								title: 'Berhasil', 
	    	    								text: 'Surat telah difinalisasi',
	    	    								type: "success",
	    	    								timer: 1500,
	    	    								showConfirmButton: false
	    	    							},
	    	    							function(dismiss){
	    	    								/*if (dismiss == 'timer') {
	    	    									swal.close();
	    	    									return $location.path('outbox/disposisi');
	    	    								} else {
	    	    									*/
	    	    									swal.close();
	    	    									return $state.go('finalisasi');
	    	    								//}
	    	    							});
	    	    						} else {
	    	    							console.log('error');
	    	    						}
	    	    					}
	    	    				}
	    	    			});
	    	    			xhr.send(formData);
	        			});
	        			
	        	}
	        	
	        	$scope.closePreview = function(modal) {
	        		modal.$close();
	        	}
	        	
        		if($scope.detailFinalisasi.fileattname[0] != ""){
	        		$scope.detailFinalisasi.attachments = $scope.detailFinalisasi.fileattname.map(function(value, index) {
	      			    return {
	      			        name: value,
	      			        url: $scope.detailFinalisasi.fileatturl[index]
	      			    }
	      			});
        		}
	        }
        })
        
        //Upload Finalisasi
        .state('upload-finalisasi', {
        	url: '/upload-finalisasi/:id',
        	templateUrl: 'pages/forms/finalisasi.html',
        	controller: ['$scope', '$location', '$http', '__env', '$stateParams', function ($scope, $location, $http, __env, $stateParams) {
        		$scope.finalisasi = {
        			attachment: []
        		};
        		
        		$scope.uploadFiles = function ($files) {
        			$scope.finalisasi.attachment = $scope.finalisasi.attachment.concat($files);
        			console.log('XXX Attachment length ', $files.length);
        			console.log('XXX Manual Attachment ', $files);
        		}
        		
        		$scope.deleteFile = function ($index) {
        			$scope.finalisasi.attachment.splice($index, 1);
        		}
        	}]
        })
        
        //DETAIL PERLU PERSETUJUAN
        .state('detail-wait',{
            url: '/detail-wait/:id',
            templateUrl : 'pages/mailbox/wait-surat.html',
            resolve: {
                data: function($http, $stateParams, $state, __env){       	
		        	return $http.get(__env.apiUrlPath + '/letters/process/'+ $stateParams.id)
	        		.then(function(response){
	        			if(response.data.data.errormsg !== '-'){
		        			console.log(response.data.data);
		        			swal({
								title: 'Terkunci', 
								text: 'Surat tidak dapat dibuka.',
								type: "error"
							},
							function(dismiss){
								swal.close();
								$state.go('index');
							});
		        			return $state.go('index');
		        		}
	        			return response.data.data;
	        		},function(error){
	        			return error.data.error_msg;
	        		});
        		},
        		
        		agenda: function($http, $stateParams, __env){
        			return $http.get(__env.apiUrlPath + '/letters/process/' + $stateParams.id +'/agenda')
	    			.then(function (response){
	    				response.data.data.headline = 'Detail Agenda Surat In Progress';
	    				response.data.data.tenggat = new Date(response.data.data.tenggat);
	    				return response.data.data;
	    			});
        		}
              },            
            controller: ['$scope', 'data','agenda', 'Upload', 'actionWaitSvc', '$location','$http','$state', '$uibModal', '__env',function($scope, data, agenda, Upload, actionWaitSvc, $location, $http, $state, $uibModal, __env){
            	  
            	  $scope.detailWait = {
            		 abstrak : data.abstrak,
            		 attDelete : '',
            		 attachment : [],
            		 klasifikasi : data.kode_klasifikasi + ' - ' + data.topik_klasifikasi,
            		 id : data.id,
            		 subject : data.subject,
            		 createddate : data.createddate,
            		 fromtitle : data.fromtitle,
            		 fromname : data.fromname,
            		 kepadaIn : data.kepadaIn,
            		 kepadaEks : data.kepadaEks,
            		 tembusanIn : data.tembusanIn,
            		 tembusanEks : data.tembusanEks,
            		 nosurat : data.nosurat,
            		 pemesan : data.pemesan,
            		 abstrak: data.abstrak,
            		 pemesanCatt : data.pemesanCatt,
            		 pemeriksa : data.pemeriksa,
            		 fileattname : data.fileattname,
            		 fileatturl : data.fileatturl,
            		 referenceDocs: {list: []},
            		 referensi: data.referensi,
            		 referensiNo: data.referensiNo,
            		 body : data.body,
            		 pictunid : data.pictunid,
            		 takahtype : data.takahtype,
            		 kode_klasifikasi : data.kode_klasifikasi,
            		 topik_klasifikasi : data.topik_klasifikasi,
            		 lettertype : data.lettertype,
            		 lampiran : data.lampiran,
            		 prioritas : data.prioritas,
            		 sifat : data.sifat,
            		 kodedireksi : data.kodedireksi,
            		 namapejabat : data.namapejabat,
            		 NUP : data.NUP,
            		 direktorat : data.direktorat,
            		 subdirektorat : data.subdirektorat,
            		 atasnama : data.atasnama,
            		 lokasikedudukan1 : data.lokasikedudukan1,
            		 lokasikedudukan2 : data.lokasikedudukan2,
            		 kodeunit : data.kodeunit,
            		 kota : data.kota,
            		 kota_tujuan : data.kota_tujuan,
            		 tahun : data.nosurat.slice(-4),
            		 errormsg : data.errormsg
            	  }
            	  $scope.attFile = {}; 
            	  $scope.attDelete = [];
            	  
            	  console.log('Detail wait ', $scope.detailWait);
            	  
            	 // $scope.detailWait.klasifikasi = $scope.detailWait.kode_klasifikasi + ' - ' + $scope.detailWait.topik_klasifikasi;
            	  console.log('AttDelete : ',$scope.detailWait.attDelete);
            	  $scope.agenda = agenda;
            	  if(data.fileattname[0] != ""){
  	        		$scope.attFile = data.fileattname.map(function(value, index) {
  	      			    return {
  	      			        name: value,
  	      			        url: data.fileatturl[index],
  	      			        aktif : true
  	      			    }
  	      			});
          		  }
            	  
            	  //$scope.attFile.aktif = true;
            	  $scope.uploadFiles = function ($files) {
	          			$scope.detailWait.attachment = $scope.detailWait.attachment.concat($files);
	          			console.log('XXX attachment length ', $files.length);
	          			console.log('XXX manual attachment ', $files);
	          	  }
	          		
	          	  $scope.deleteFile = function ($index) {
	          			$scope.detailWait.attachment.splice($index, 1);
	          	  }
            	  
            	  $scope.removeAttDraft = function(name,index){
  	      			$scope.attDelete.push(name);
  	      			console.log('Push remove');
  	      			console.log($scope.attDelete);
  	      			$scope.attFile[index].aktif = false;
  	      		  }
            	  
            	  $scope.resolveReferences = function(data) {
  	        		data.list = data.list.filter(function(val){
  	        			if(val)
  	        				return true;
  	        			else
  	        				return false;
  	        		});
  	        		
  	        		data.data = data.data.filter(function(val){
  	        			if(val)
  	        				return true;
  	        			else
  	        				return false;
  	        		});
  	        		
  	        		console.log('XXX data selected ', data);
  	        		
  	        		//$scope.detailWait.referenceDocs = data;
  	        		$scope.detailWait.referenceDocs.list.push(data.list.toString());
  	        		$scope.detailWait.referensiNo = data.subject;
  	        		$scope.detailWait.referensi = data.data;
  	        	  }
            	  
            	  $scope.draftWait = function (action,body,abstrak,kepada,tembusan,sifat) {
	          			if($scope.detailWait.referenceDocs.list && $scope.detailWait.referenceDocs.list.length > 0) {
	          				$http.get(__env.apiUrlPath + '/referensi?docunid='+$scope.detailWait.referenceDocs.list.toString(), {responseType: 'json'}).then(
	          					function (response) {
	          	        			$scope.detailWait.referensi = $scope.detailWait.referensi.concat(response.data.data.refDetail);
	          	        			// remove duplicat
	          	        			$scope.detailWait.referensi = $scope.detailWait.referensi.filter(function(value, idx, self){
	          	        				return self.indexOf(value) == idx;
	          	        			});
	          	        			console.log('Referensi ', $scope.detailWait.referensi);
	          	        			console.log('ReferensiNo ', $scope.detailWait.referensNo);
	          						send(action,body,abstrak,kepada,tembusan,sifat);
	          					}, function (error) {
	          						console.log('XXX error getting references ', error);
	          					}
	          				);
	          			} else {
	          				//$scope.detailWait.referensi = [];
	          				//$scope.detailWait.referensiNo = [];
	          				send(action,body,abstrak,kepada,tembusan,sifat);
	          				console.log('Referensi ', $scope.detailWait.referensi);
      	        			console.log('ReferensiNo ', $scope.detailWait.referensiNo);
	          			}
	          	  }
	        	              	  
          		  function send (action,body,abstrak,kepada,tembusan,sifat) {
          		   	if ($scope.attDelete.length > 0) {
          		   		$scope.detailWait.attDelete = $scope.attDelete.join(';');
          		   	}
	          		if(tembusan.length > 0){
	          			tembusan = tembusan.join(';');
	      			}else{
	      				tembusan = '';
	      			}
	
	      			if(kepada.length > 0){
	      				kepada = kepada.join(';');
	      			}else{
	      				kepada = '';
	      			}
	      			
	      			var id = document.getElementById('id_surat').value;
          			var jabatan = document.getElementById('jabatan').value;
            		var jabatanFullName = document.getElementById('fullNameJabatan').value;
        	    	$scope.waitData = {
        				id : id,
        				perihal : $scope.detailWait.subject,
        				isi_surat : body,
        				abstrak: abstrak,
        				kepada : kepada,
        				tembusan : tembusan,
        				attachment : $scope.detailWait.attachment,
        				attDelete : $scope.detailWait.attDelete,
        				komentar : '',
        				jabatan : jabatan,
        				jabatanFullName : jabatanFullName,
        				referensi: $scope.detailWait.referensi,
        				referensiNo: $scope.detailWait.referensiNo,
        				sifat: sifat,
        				aksi : action,
        				tenggat: $scope.agenda.tenggat,
        				tandaTangan: $scope.agenda.flagttd
        			}
        	    	console.log('Wait Data : ', $scope.waitData);
        	    	var approvalResult = actionWaitSvc.actionWait($scope.waitData, 0);
        			approvalResult.then(function (response) {
        				$scope.response = response;
        				console.log('XXX cek response draft approval1: ', $scope.response);
        				if ($scope.response.status != 'error') {
        					$scope.response.flow = 'Sukses',
        					$scope.response.message = 'Anda telah menyimpan surat ini'
        					//$scope.response.redirect_path = '/agenda/process/'+id;
        					swal({
        						title: $scope.response.flow, 
        						text: $scope.response.message,
        						type: "success",
        						timer: 1500,
        						showConfirmButton: false
        					},
							function(dismiss){
								if (dismiss == 'timer') {
									swal.close();
									return $state.go('agenda',{location:'process',id:id});
								} else {
									swal.close();
									return $state.go('agenda',{location:'process',id:id});
								}
							});
        				} else {
        					swal({
        						title:"Error",
        						text: $scope.response.message,
        						type: "error"
        					});
        				}
        			}, function (error) {
        				console.log('XXX error wait promise: ', error);
        			});
	        	  };
          		  console.log('AttFile', $scope.attFile);
            	  console.log("data");
            	  console.log($scope.detailWait);
	        }]
        })
        
        //DETAIL STATUS
        .state('detail-status', {
        	url: '/detail-status/:id',
        	templateUrl: 'pages/mailbox/status.html',
        	resolve: {
                data: function($http, $stateParams, __env){       	
		        	return $http.get(__env.apiUrlPath + '/letters/process/'+ $stateParams.id)
	        		.then(function(response){
	        			let referensiObj = [];
		        		for(let ref of response.data.data.referensi) {
		        			let tmp = ref.split('~');
		        			
		        			let refObj = {noSurat: tmp[0]};
		        			
		        			let jenisDok = tmp[tmp.length - 1];
		        			if(jenisDok == 'SuratKeluar') {
		        				refObj.takahType = 'sk';
		        			} else if(jenisDok == 'NotaDinas') {
		        				refObj.takahType = 'nd';
		        			} else {
		        				refObj.takahType = 'me';
		        			}
		        			
		        			let storage = tmp[1].split('\\')[1];
		        			refObj.storage = storage;
		        			
		        			refObj.id = tmp[2];
		        			referensiObj.push(refObj);
		        		}
		        		response.data.data.referensiObj = referensiObj;
		        		
	        			$http.get(__env.apiUrlPath + '/letters/takah/'+response.data.data.takahtype+'/'+response.data.data.id+'/'+response.data.data.tipeGenerik+'?dbLoc=keluar')
			        	.then(function(takah){
			        		response.data.data.html_takah = takah.data.data.html_takah;
			        		//console.log(takah.data.data);
			        	},function(error){
			        		return error.data.data.errormsg;
			        	});
	        			return response.data.data;
	        			
	        		},function(error){
	        			return error.data.error_msg;
	        		});
        		}
              },
        	controller: function($scope, data, $uibModal, $http, __env){
            	  $scope.isMobile = isMobile;
            	  $scope.detailStatus = data;
            	  $scope.previewRef;
            	  
            	  let versiMobile = (isMobile) ? 'yes' : 'no';
            	  
            	  $scope.previewReferensi = function(storage, unid, type) {
  	        		$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then(
  	        			function (response) {
  	        				if(isMobile) {
  	        					$scope.previewRef = response.data.data;
  	        				} else {
  	        					$scope.previewRef = response.data.data.html_takah;
  	        				}
  	        				
  	        				var modalInstance = $uibModal.open({
  	        				      ariaLabelledBy: 'modal-title',
  	        				      ariaDescribedBy: 'modal-body',
  	        				      templateUrl: 'myModalContent.html',
  	        				      size: 'lg',
  	        				      scope: $scope
  	        				    });
  	        			}, function (error) {
  	        				console.log('XXX error mengambil dokumen referensi');
  	        			}
  	        		);
  	        		
  	        	};
  	        	
  	        	$scope.print = function() {
  	        		if($scope.isMobile){
  	        			$scope.isMobile = false;
  	        			var beforePrint = function() {
  	        				$scope.isMobile = false;
  	        				console.log('Print Dialog before open..');
  	        				console.log('isMobile : ',$scope.isMobile);
  	        		    };

  	        		    var afterPrint = function() {
  	        		    	console.log('Print Dialog Closed..');
  	        		    	$scope.isMobile = true;
  	        		    	console.log('isMobile : ',$scope.isMobile);
  	        		    };
  	        		    
  	        		    if (window.matchMedia) {
  	        		        var mediaQueryList = window.matchMedia('print');
  	        		        mediaQueryList.addListener(function(mql) {
  	        		            if (mql.matches) {
  	        		            	
  	        		                beforePrint();
  	        		            } else {
  	        		            	
  	        		                afterPrint();
  	        		            }
  	        		        });
  	        		    }
  	        		  window.onbeforeprint = beforePrint;
  	        		  setTimeout(window.print,2000);
  	        		  window.onafterprint = afterPrint;
  	        		}else{
  	        			window.print();
  	        		}

  	        	}

  	        	$scope.closePreview = function(modal) {
  	        		modal.$close();
  	        	}
	        		
	        		//$scope.detailStatus.klasifikasi = '';
	        		
	        		if($scope.detailStatus.fileattname[0] != ""){
	  	        		$scope.detailStatus.attachments = $scope.detailStatus.fileattname.map(function(value, index) {
	  	      			    return {
	  	      			        name: value,
	  	      			        url: $scope.detailStatus.fileatturl[index]
	  	      			    }
	  	      			});
	  	        		//$scope.detailStatus.attachments.push({name:"testattachment2.doc",url:"/eoffice/eoffice.nsf/index.html"});
	  	        		
	        		}
	        }
        })
        
        //DETAIL DRAFT
        .state('detail-draft',{
            url: '/detail-draft/:id',
            templateUrl: 'pages/mailbox/draft.html',
            resolve: {
                data: function($http, $stateParams, __env){       	
		        	return $http.get(__env.apiUrlPath + '/letters/process/'+ $stateParams.id)
	        		.then(function(response){
	        			return response.data.data;
	        			
	        		},function(error){
	        			return error.data.errormsg;
	        		});
        		}
        	},
            controller : ['$stateParams','$scope','$http','data','composeSvc','$state', '$rootScope', '__env',
                          function($stateParams, $scope, $http, data, composeSvc,$state, $rootScope, __env){
        		//console.log($stateParams.id);
            	$scope.takah = data.takahtype;
        		//$scope.surat = data;
        		console.log("takahtype : "+data.takahtype);
        		$scope.attFile = {}
        		$scope.aktif = true;
        		
        		if(data.fileattname[0] != ""){
	        		$scope.attFile = data.fileattname.map(function(value, index) {
	      			    return {
	      			        name: value,
	      			        url: data.fileatturl[index],
	      			        aktif : true
	      			    }
	      			});
        		}
	        	        		
    			if(data.takahtype == 'nd'){
    				
    				$scope.surat = {
    					id : data.id,
    					nosurat : data.nosurat,
    					abstrak: data.abstrak,
						tembusanEks : data.tembusanEks,
	        			manualSendTo : data.kepadaEks,
	        			kota_tujuan : '',
	        			kepada : data.kepadaIn,
	        			tembusanIn : data.tembusanIn,
	        			fromtitle : [data.fromtitle],
	        			pemeriksa: data.pemeriksa,
	        			pemesan: [data.pemesan],
	        			pemesanCatt: data.pemesanCatt,
	        			subject: data.subject,
	        			kode: data.kode_klasifikasi,
	        			topik: data.topik_klasifikasi,
	        			body: data.body,
	        			catatan: data.note,
	        			sifat: data.sifat,
	        			MDDueDate: data.tenggat ? new Date(data.tenggat) : undefined,
	        			atasnama: data.atasnama,
	        			//balas: data.balas,
	        			tipeGenerik: data.tipeGenerik,
	        			takahtype: 'NotaDinas',
	        			attachment: [],
	        			referenceDocs: [],
	        			referensi: data.referensi,
	        			referensiNo: data.referensiNo,
	        			attDelete: [],
	        			workflow: '',
	        			action: '',
	        			classificationText: '',
	        			status: data.status,
	        			attCount: data.fileattname.length,
	        			MDJenisSMM: ''
    	        	};
    				
    				if ($scope.surat.tipeGenerik === 'NotaDinas') {
    					$scope.surat.MDJenisSMM = 'Nota Dinas';
    				} else if($scope.surat.tipeGenerik === 'SuratTugas') {
    					$scope.surat.MDJenisSMM = 'Surat Tugas';
    				} else if($scope.surat.tipeGenerik === 'SuratUndangan') {
    					$scope.surat.MDJenisSMM = 'Surat Undangan';
    				}
    					
    				if((data.topik_klasifikasi == "") || (data.kode_klasifikasi == "")){
            			$scope.surat.classificationText = '';
            		} else {
            			$scope.surat.classificationText = data.kode_klasifikasi +" - "+data.topik_klasifikasi;
            		}
        		} else if(data.takahtype == 'sk'){

        			$scope.surat = {
        				id : data.id,
        				nosurat : data.nosurat,
    					abstrak: data.abstrak,
        				kepada : [],
    					manualSendTo : data.kepadaEks,
	        			tembusanIn : [],
	        			tembusanEks : data.tembusanEks,
	        			kota_tujuan : data.kota_tujuan,
	        			fromtitle : [],
	        			pemeriksa: [],
	        			pemesan: [],
	        			pemesanCatt: data.pemesanCatt,
	        			subject: data.subject,
	        			kode: data.kode_klasifikasi,
	        			topik: data.topik_klasifikasi,
	        			body: data.body,
	        			sifat: data.sifat,
	        			MDDueDate: data.tenggat ? new Date(data.tenggat) : undefined,
	        			atasnama: data.atasnama,
	        			takahtype: 'SuratKeluar',
	        			attachment: [],
	        			referenceDocs: [],
	        			referensi: data.referensi,
	        			referensiNo: data.referensiNo,
	        			attDelete: [],
	        			workflow: '',
	        			action: '',
	        			classificationText: '',
	        			status: data.status,
	        			attCount: data.fileattname.length
    	        	};
    				
    				$scope.tembusanIn = data.tembusanIn;
    	        	$scope.pemesan = [data.pemesan];
    	        	$scope.fromtitle = [data.fromtitle];
    	        	$scope.pemeriksa = data.pemeriksa;
    	        	
    	        	$scope.surat.tembusanIn = $scope.tembusanIn;
    	        	$scope.surat.pemesan = $scope.pemesan;
    	        	$scope.surat.fromtitle = $scope.fromtitle;
    	        	$scope.surat.pemeriksa = $scope.pemeriksa;
    			
        			if((data.topik_klasifikasi == "") || (data.kode_klasifikasi == "")){
            			$scope.surat.classificationText = '';
            		} else {
            			$scope.surat.classificationText = data.kode_klasifikasi +" - "+data.topik_klasifikasi;
            		}
        		}else if(data.takahtype == 'me'){
        			
        			$scope.surat = {
        				id : data.id,
        				nosurat : data.nosurat,
    					abstrak: data.abstrak,
    					tembusanIn : [],
	        			tembusanEks : [],
	        			manualSendTo : [],
	        			kota_tujuan : '',
	        			pemeriksa: [],
	        			pemesan: '',
	        			pemesanCatt: '',
	        			kepada : data.kepadaIn,
	        			fromtitle : [data.fromtitle],
	        			subject: data.subject,
	        			kode: data.kode_klasifikasi,
	        			topik: data.topik_klasifikasi,
	        			body: data.body,
	        			sifat: data.sifat,
	        			atasnama: data.atasnama,
	        			takahtype: 'Memo',
	        			attachment: [],
	        			referenceDocs: [],
	        			referensi: data.referensi,
	        			referensiNo: data.referensiNo,
	        			attDelete: [],
	        			workflow: '',
	        			action: '',
	        			classificationText: '',
	        			status: data.status,
	        			attCount: data.fileattname.length,
	        			MDJenisSMM: 'Memo'
    	        	};
    				
        			if((data.topik_klasifikasi == "") || (data.kode_klasifikasi == "")){
            			$scope.surat.classificationText = '';
            		} else {
            			$scope.surat.classificationText = data.kode_klasifikasi +" - "+data.topik_klasifikasi;
            		}
        			
        		}
    			
    			$scope.surat.tahun = [];
    			$scope.surat.tahun = $scope.surat.nosurat.split('/');
    			
    			if($scope.surat.pemesan[0] == undefined){
        			$scope.surat.pemesan[0] = '';
        		}
    			
    			//$scope.surat.referenceDocs.list = 
    			
    			$scope.resolveReferences = function(data) {
	        		data.list = data.list.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		data.data = data.data.filter(function(val){
	        			if(val)
	        				return true;
	        			else
	        				return false;
	        		});
	        		
	        		console.log('XXX data selected ', data);
	        		
	        		$scope.surat.referenceDocs = data;
	        		$scope.surat.referensiNo = data.subject;
	        		$scope.surat.referensi = data.data;// error, over write
	        	}
    			
    			//Attachment
    			$scope.uploadFiles = function ($files) {
        			$scope.surat.attachment = $scope.surat.attachment.concat($files);
        			$scope.surat.attCount += $scope.surat.attachment.length; 
        			console.log('XXX attachment length ', $files.length);
        			console.log('XXX manual attachment ', $files);
        		}
        		
    			//Delete Attachment
        		$scope.deleteFile = function ($index) {
        			$scope.surat.attachment.splice($index, 1);
        			$scope.surat.attCount--;
        		}
    			
        		$http({
	        		url : __env.apiUrlPath + '/detailpengirim?pemesan=' + $scope.surat.pemesan,
	        		method: 'post',
	        		headers: {'Content-Type': 'application/json'},
	        		data : JSON.stringify({
	        			data : {
	        				pengirim : $scope.surat.fromtitle[0]
		        		}
	        		})
	        	}).then(function onSuccess(data, status, headers, config) {
	        		$scope.detailPengirim = data.data.data;
	        		console.log('Detail Pengirim : ', $scope.detailPengirim);
	        	}).catch(function onError(data, status, headers, config) {
	        		$scope.status = status + ' ' + headers;
		       	});
        		
        		//Post detail Pengirim
        		$scope.blur = function(){
	        		console.log("change pengirim : "+$scope.surat.fromtitle[0]);
	        		console.log("Pemesan : " + $scope.surat.pemesan[0]);
	        		
	        		if($scope.surat.pemesan[0] == undefined){
	        			$scope.surat.pemesan[0] = '';
	        		}
	        		
		        	if($scope.surat.fromtitle[0]  != undefined || $scope.surat.fromtitle[0] != ""){
			        		$http({
				        		url : __env.apiUrlPath + '/detailpengirim?pemesan=' + $scope.surat.pemesan[0],
				        		method: 'post',
				        		headers: {'Content-Type': 'application/json'},
				        		data : JSON.stringify({
				        			data : {
				        				pengirim : $scope.surat.fromtitle[0]
					        		}
				        		})
				        	}).then(function onSuccess(data, status, headers, config) {
				        		$scope.detailPengirim = data.data.data;
				        		console.log($scope.detailPengirim);
				        	}).catch(function onError(data, status, headers, config) {
				        		$scope.status = status + ' ' + headers;
					       	});
		        	}
	        	}
        	
        		$scope.removeAttDraft = function(name,index){
	      			$scope.surat.attDelete.push(name);
	      			console.log('Push remove');
	      			console.log($scope.surat.attDelete);
	      			$scope.attFile[index].aktif = false;
	      			$scope.surat.attCount--;
	      		}
        		
        		$scope.composeDraft = function(action, workflow) {       			
        			if ($scope.surat.pemesan[0] == '' && $scope.surat.fromtitle[0] == '') {
        				$scope.detailPengirim = {
        					nama_pemesan_full: '',
        					nama_pemesan: '',
        					nama_pejabat_full: '',
        					nama_pejabat: '',
        					NUP: '',
        					positionID: '',
        					direktorat: '',
        					sub_direktorat: '',
        					lokasi_1: '',
        					lokasi_2: '',
        					kodeunit: '',
        					kota: ''
        				}
        			}
        			
        			if($scope.surat.referenceDocs.list && $scope.surat.referenceDocs.list.length > 0) {
        				$http.get(__env.apiUrlPath + '/referensi?docunid='+$scope.surat.referenceDocs.list.toString(), {responseType: 'json'}).then(
        					function (response) {
        	        			$scope.surat.referensi = $scope.surat.referensi.concat(response.data.data.refDetail);
        	        			// remove duplicat
        	        			$scope.surat.referensi = $scope.surat.referensi.filter(function(value, idx, self){
        	        				return self.indexOf(value) == idx;
        	        			});
        						send(action, workflow);
        					}, function (error) {
        						console.log('XXX error getting references ', error);
        					}
        				);
        			} else {
        				//$scope.surat.referensi = [];
        				//$scope.surat.referensiNo = [];
        				send(action, workflow);
        			}
        		}
        			
        		function send (action, workflow){
	        		$scope.surat.workflow = workflow;
	        		$scope.surat.action = action;
	        		console.log('Surat setelah diklik :', $scope.surat);
	        		if($scope.surat.takahtype == 'SuratKeluar'){
	        			var kpdEks = document.getElementById('kpdEks').value;
		    			var temEks = document.getElementById('temEks').value;
		    			$scope.surat.manualSendTo = kpdEks.split('\n');
		    			$scope.surat.tembusanEks = temEks.split('\n');
		    			$scope.surat.tembusanIn = $scope.tembusanIn;
		    			$scope.surat.fromtitle = $scope.fromtitle;
		        		$scope.surat.pemeriksa = $scope.pemeriksa;
		    			$scope.surat.pemesan = $scope.pemesan;
	        		}
	        		//console.log($scope.surat.tembusanEks);

	        		/*if($scope.surat.pemeriksa.length == 0){
	        			if($scope.surat.fromtitle[0] != $scope.infoUser.jabatan){
		        			$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		        		}else{
		        			$scope.surat.pemeriksa = [];
		        		}
	        		} else if ($scope.surat.pemeriksa.length == 1) {
	        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[0]){
	        				$scope.surat.pemeriksa.splice(i,1);
	        			}else{
	        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
	        			}
	        		} else{
		        		for(var i=0; i<$scope.surat.pemeriksa.length; i++){
		        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[i]){
		        				$scope.surat.pemeriksa.splice(i,1);
		        			}else{
		        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		        			}
		        		}
	        		}*/
	        		
	        		if(action == "Submit"){
		        		swal({
		        			  title: "<i class='fa fa-paper-plane-o fa-4x text-primary'></i>",
		        			  text: "Apakah anda yakin ingin mengirim surat ini?",
							  html:true,
		        			  showCancelButton: true,
		        			  closeOnConfirm: false,
		        			  showLoaderOnConfirm: true
		        			},
		        			function(){
		        				if($scope.surat.pemeriksa.length == 0){
		    	        			if($scope.surat.fromtitle[0] != $rootScope.profile.jabatan){
		    		        			$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		    		        		}else{
		    		        			$scope.surat.pemeriksa = [];
		    		        		}
		    	        		} else if ($scope.surat.pemeriksa.length == 1) {
		    	        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[0]){
		    	        				$scope.surat.pemeriksa.splice(i,1);
		    	        			}else{
		    	        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		    	        			}
		    	        		} else{
		    		        		for(var i=0; i<$scope.surat.pemeriksa.length; i++){
		    		        			if($scope.surat.fromtitle[0] == $scope.surat.pemeriksa[i]){
		    		        				$scope.surat.pemeriksa.splice(i,1);
		    		        			}else{
		    		        				$scope.surat.pemeriksa.push($scope.surat.fromtitle[0]);
		    		        			}
		    		        		}
		    	        		}
		        				var postResult = composeSvc.postDataDraft($scope.surat, $scope.detailPengirim);
		        				postResult.then(function(response) {
		        					$scope.response = response;
		        					console.log('XXX cek response1: ', $scope.response);
		        					if($scope.response.status != 'error'){
		        						if($scope.response.data.MDStatus == 'Complete'){
		        							$scope.response.flow = 'Berhasil Dikirim';
		        							$scope.response.message = $scope.title + ' berhasil dikirim';	
		        							$scope.response.location = "outbox";
		    							}else{
		    								$scope.response.flow = 'Berhasil Diproses';
		    								$scope.response.message = $scope.title + ' sedang dalam proses persetujuan pemeriksa';	
		    								$scope.response.location = "process";
		    							}
		        						
		        						swal({
		        							title:'Terkirim', 
	        								text: $scope.response.message,
	        								type: "success",
	        								timer: 1500,
	        								showConfirmButton: false
	        							},
        								function(dismiss){
	        								if (dismiss == 'timer') {
	        									swal.close();
	        									return $state.go('agenda',{location: $scope.response.location,id: $scope.response.data.id});
	        								} else {
	        									swal.close();
	        									return $state.go('agenda',{location: $scope.response.location,id: $scope.response.data.id});
	        								}
	        							});
			        				}else{
			        					swal({
			        						title:"Error",
			        						text: $scope.response.message,
			        						type: "error"
			        					});
			        				}
		        				}, function(error) {
		        					console.log('XXX error promise: ', error);
		        				});
		        				console.log('XXX cek response2: ', $scope.response);
		        			});
		        		}else if(action == "Draft"){
		        			swal({
			        			  title: "Simpan ke dalam draft",
			        			  text: "Apakah anda ingin menyimpan surat ini?",
			        			  type: "warning",
			        			  showCancelButton: true,
			        			  closeOnConfirm: false,
			        			  showLoaderOnConfirm: true
			        			},
			        			function(){
			        				var draftResult = composeSvc.postDataDraft($scope.surat, $scope.detailPengirim);
			        				draftResult.then(function (response) {
			        					$scope.response = response;
			        					if($scope.response.status != 'error'){
		        							swal({
		        								title:'Berhasil disimpan!',
		        								text: "Surat Anda berhasil disimpan ke dalam draft",
		        								type: "success",
		        								timer: 1500,
		        								showConfirmButton : false
		        							},
		        							function(dismiss){
		        								if (dismiss == 'timer') {
		        									swal.close();
		        									return $state.go('agenda',{location: 'draft',id: $scope.response.data.id});
		        								} else {
		        									swal.close();
		        									return $state.go('agenda',{location: 'draft',id: $scope.response.data.id});
		        								}
		        							});
		        						}else{
			        						swal({
			        							title:"Error",
			        							text: $scope.response.message,
			        							type: "error"
			        						});
		        						}
			        				}, function(error){
			        					console.log('Error promise : ',error);
			        				});	
			        			});
		        		}
	        			        		
	        		//composeSvc.postDataDraft($scope.surat, $scope.detailPengirim);
	        	};
        	}]
        })

        //DAFTAR DISPOSISI
        .state('daftar-disposisi', {
            url: '/daftar-disposisi/:id',
            templateUrl: 'pages/tables/tabel-disposisi.html',
            resolve: {
	        	data: function($http, $stateParams, __env){       	
		        	return $http.get(__env.apiUrlPath + '/letters/'+$stateParams.id+'/disposisi/1/20')
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});
				},
				surat: function($http, $stateParams, __env){       	
		        	return $http.get(__env.apiUrlPath + '/letters/inbox/'+ $stateParams.id)
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});
				},
				agenda: function($http, $stateParams, __env){       	
		        	return $http.get(__env.apiUrlPath + '/letters/inbox/'+ $stateParams.id+'/agenda')
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});
				}
        	},
            controller: function($scope, data, surat, agenda){
        		$scope.daftarDisposisi = data;
        		$scope.detailSurat = surat;
        		$scope.riwayatBalasan = agenda.alurBalasan;
        		//console.log("riwayat balasan",$scope.riwayatBalasan);
	        }
        })
        
        

        //BUAT DISPOSISI
        .state('buat-disposisi', {
            url: '/buat-disposisi/:id/:lettertype',
            templateUrl: 'pages/forms/buat-disposisi.html',
            resolve: {
	        	surat: function($http, $stateParams, __env){
        			if ($stateParams.lettertype === 'disposisi') {
        				$http.get(__env.apiUrlPath + '/disposisi/'+ $stateParams.id)
    		    		.then(function(response){
    		    			return $http.get(__env.apiUrlPath + '/letters/inbox/'+ response.data.data.letterid)
        		    		.then(function(data){
        		    			return response.data.data;        		    			
        		    		},function(error){
        		    			return error.data.errormsg;
        		    		});    		    			
    		    		},function(error){
    		    			return error.data.errormsg;
    		    		});
        			} else {
        				return $http.get(__env.apiUrlPath + '/letters/inbox/'+ $stateParams.id)
    		    		.then(function(response){
    		    			return response.data.data;    		    			
    		    		},function(error){
    		    			return error.data.errormsg;
    		    		});
        			}
				},
				notatindakan: function($http, __env){       	
		        	return $http.get(__env.apiUrlPath + '/lookup/tindakan')
		    		.then(function(response){
		    			return response.data.data;
		    			
		    		},function(error){
		    			return error.data.errormsg;
		    		});
				}
        	},
            controller: function($http, $scope, $state, $stateParams, $location, surat, notatindakan, __env){
        		$scope.detailSurat = surat;
	        	$scope.Notatindakan = notatindakan;
	        	
	        	$scope.buatDisposisi = {
	        		tindakan: [],
	        		kepada: [],
	        		catatan: ''
	        	};
	        	$scope.kepadaRequired = function(){
	        		if($scope.buatDisposisi.kepada == undefined){
	        			$scope.buatDisposisi.kepada = [];
	        		}
	        		return $scope.buatDisposisi.kepada.length <= 0;
	        	}
	        	
	        	// jabatan bisa mengambil dari $rootScope.profile
	        	$scope.save = function(jabatan){
	        		//var penerima = [$scope.buatDisposisi.dspmdsendto];
	        		
	        		swal({
	        			  title: "<i class='fa fa-paper-plane-o fa-4x text-primary'></i>",
	        			  text: "Apakah anda yakin ingin mengirim disposisi ini?",
						  html:true,
	        			  showCancelButton: true,
	        			  closeOnConfirm: false,
	        			  showLoaderOnConfirm: true
	        			},
	        			function(){
	        		
				        	$http({
				    			url : __env.apiUrlPath + '/letters/disposisi',
				    			method : 'POST',
				    			data : JSON.stringify({
				                    	data : {
					                      id: $stateParams.id,
					                      penerima: $scope.buatDisposisi.kepada,
					                      pengirim: jabatan,
					                      tindakan: $scope.buatDisposisi.tindakan,
					                      catatan: $scope.buatDisposisi.catatan,
					                      tipe: 'Disposisi'
					                    }
				                    }),
				                headers: {'Content-Type': 'application/json'}
				    		}).then(function onSuccess(data, status, headers, config) {
				    			swal({
									title: 'Terkirim', 
									text: 'Disposisi berhasil dikirim',
									type: "success",
									timer: 2000,
									showConfirmButton: false
								},
								function(dismiss){
									if (dismiss == 'timer') {
										swal.close();
										return $state.go('outbox',{filter:'disposisi'});
										//return $location.path('outbox/disposisi');
									} else {
										swal.close();
										return $state.go('outbox',{filter:'disposisi'});
										//return $location.path('outbox/disposisi');
									}
								});
				    		}).catch(function onError(data, status, headers, config) {
				    			swal({
									title: 'Gagal', 
									text: 'Anda tidak bisa mengirim disposisi. Silakan coba beberapa saat lagi.',
									type: "error"},
									function(){
										return $location.path('index');
								});
				    		});
	        			});
	        	}
		    }
        })

        //FORWARD
        .state('teruskan', {
            url: '/teruskan/:id',
            templateUrl:'pages/forms/forward.html',
            resolve: {
	        	surat: function($http, $stateParams, __env){
        			return $http.get(__env.apiUrlPath + '/letters/inbox/' + $stateParams.id)
    		    		.then(function(response){
    		    			return response.data.data;        		    			  		    			
    		    		},function(error){
    		    			return error.data.errormsg;
    		    		});
				}
        	},
            controller: function($scope, $stateParams, $http, $location, $state, $rootScope, surat, __env){
	        		$scope.detailForward = surat;
	        		var id = $stateParams.id;
					$scope.kirim = function(catatan){
						$http.get(__env.apiUrlPath + '/users/self/assistance/'+ $rootScope.profile.jabatan)
						.then(function (response) {
							$scope.sekretaris = response.data.data;
							console.log('data sekretaris : ', $scope.sekretaris);
							$http({
			        			url : __env.apiUrlPath + '/letters/forward/' + id,
			        			method : 'POST',
			        			data : {
			                    	data : {
				                      id: id,
				                      jabatan: $scope.sekretaris.atasan,
				                      catatan: catatan,
				                      action: "forward"
				                    }
			                    },
				                headers: {'Content-Type': 'application/json'}
			        		}).then(function onSuccess(data){
			        			swal({
									title: 'Berhasil!', 
									text: 'Surat berhasil diteruskan',
									type: "success",
									timer: 1500,
									showConfirmButton: false
								},
								function(dismiss){
									if (dismiss == 'timer') {
										swal.close();
										return $state.go('detail-inbox',{id:id});
										//return $location.path('detail-inbox/' + id);
									} else {
										swal.close();
										return $state.go('detail-inbox',{id:id});
										//return $location.path('detail-inbox/' + id);
									}
								});
			        			console.log('Sukses');
							},function onError(data){
								swal({
									title: 'Gagal', 
									text: 'Anda tidak bisa meneruskan surat ini. Silakan coba beberapa saat lagi.',
									type: "error"},
									function(){
										return $state.go('detail-inbox',{id:id});
										//return $location.path('detail-inbox/' + id);
								});
							});
						},function (error) {
							$scope.sekretaris = error.data.errormsg;
						});
		        	}
	        	
	        }
        })

        //TAMBAH DELEGASI
        .state('tambah-delegasi', {
            url: '/tambah-delegasi',
            templateUrl: 'pages/forms/delegasi.html',
            controller: ['$http', '$scope', '$state', '$state', '$rootScope', 'dateFilter', '__env', function($http, $scope, $state, $state, $rootScope, dateFilter, __env){
            	$scope.delegasi = {
            		delegated: []
            	};
            	
            	$scope.alert = {};

            	$scope.updateStartDate = function(){
            		$scope.dateOptions2.minDate = new Date(dateFilter($scope.delegasi.date_start, 'yyyy-MM-dd'));
            	}
            	
            	$scope.dateOptions = {
            		minDate : new Date(),
            		showWeeks : false
            	};
            	
            	$scope.dateOptions2 = {
                	minDate : new Date(),
                	showWeeks : false
                };
            	
            	$scope.open1 = function() {
            	    $scope.popup1.opened = true;
            	};
            	$scope.open2 = function() {
            	    $scope.popup2.opened = true;
            	};

            	$scope.popup1 = {
            	    opened: false
            	};
            	$scope.popup2 = {
            	    opened: false
            	};
            	
	    		$scope.delegate = function(){
	    			var date1 = dateFilter($scope.delegasi.date_start, 'MM/dd/yyyy');
	            	var date2 = dateFilter($scope.delegasi.date_stop, 'MM/dd/yyyy');
	            	
	            	console.log('XXX delegateds are: ', $scope.delegasi.delegated);
	            	
	    			$http({
	        			url : __env.apiUrlPath + '/users/self/delegation/',
	        			method : 'POST',
	        			data : {
		                    	data : {
			                      delegator: $rootScope.profile.jabatan,
			                      delegated: $scope.delegasi.delegated[0],
			                      date_start: date1,
			                      date_stop: date2,
			                      reason: $scope.delegasi.reason,
			                      action: 'set'
			                    }
		                    },
		                headers: {'Content-Type': 'application/json'}
	        		}).then(function onSuccess(response) {
	        			if(response.data.data.errormsg) {
	        				$scope.alert.msg = response.data.data.errormsg;
	        				$scope.alert.type = 'danger';
	        				$scope.showAlert = true;
	        			} else {	        				
	        				$state.go('delegasi');
	        			}
	        		}, function onError(response) {
	        		    console.log('Error ', response.statusText);
	        		});
	    		}
	        }]
        })
        
        //RIWAYAT
        .state('riwayat',{
            url: '/riwayat/:id',
            templateUrl: 'pages/tables/riwayat.html',
            controller: function($http, $stateParams, $scope, __env){
	        	var id = $stateParams.id;
	        	$http.get(__env.apiUrlPath + '/letters/process/'+id+'/log')
	        	.then(function(response){
	        		$scope.dataRiwayat = response.data.data;
	        	})
	        }
        })

        //BUAT TANGGAPAN
        .state('buat-tanggapan',{
            url:'/buat-tanggapan/:id/:fromtitle',
            templateUrl: 'pages/forms/tanggapan.html',
            resolve: {
	        	data : function($http, $stateParams, __env){
        				return $http.get(__env.apiUrlPath + '/letters/inbox/' + $stateParams.id).then(function(detail){
        					return detail.data.data;
        				});
            	}
	        },
            controller: function($scope, $http, $location, $stateParams, $state, $rootScope, data){
	        	$scope.detailSuratDispo = data;
	        	console.log('From title : ', $stateParams.fromtitle);
        		$scope.tanggapan= {
        			kepada: [$stateParams.fromtitle],
        			catatan: ''
        		};
        		
        		$scope.kepadaRequired = function(){
	        		if($scope.tanggapan.kepada == undefined){
	        			$scope.tanggapan.kepada = [];
	        		}
	        		return $scope.tanggapan.kepada.length <= 0;
	        	}
        		
	        	$scope.sendTanggapan = function(jabatan){
        			$http({
        				url : __env.apiUrlPath + '/letters/disposisi',
		    			method : 'POST',
		    			data : JSON.stringify({
		                    	data : {
			                      id: $stateParams.id,
			                      penerima: $scope.tanggapan.kepada,
			                      pengirim: $rootScope.profile.jabatan,
			                      catatan: $scope.tanggapan.catatan,
			                      tipe: 'Tanggapan'
			                    }
		                    }),
		                headers: {'Content-Type': 'application/json'}
    	    		}).then(function onSuccess(data, status, headers, config) {
    	    			swal({
							title: 'Terkirim', 
							text: 'Tanggapan berhasil dikirim',
							type: "success",
							timer: 1500,
							showConfirmButton: false
						},
						function(dismiss){
							/*if (dismiss == 'timer') {
								swal.close();
								return $location.path('outbox/disposisi');
							} else {
								*/
								swal.close();
								return $state.go('outbox',{filter:'disposisi'});
							//}
						});
    	    		}).catch(function onError(data, status, headers, config) {
    	    			swal({
							title: 'Gagal', 
							text: 'Anda tidak bisa mengirim tanggapan. Silakan coba beberapa saat lagi.',
							type: "error"},
							function(){
								return $state.go('index');
						});
    	    		});
        		}
	        }
        })

        //RIWAYAT DISPOSISI
        .state('riwayat-disposisi',{
            url: '/riwayat-disposisi/:id',
            templateUrl: 'pages/tables/riwayat-disposisi.html',
            resolve: {
	        	data : function($http, $stateParams, __env){
		        	return $http.get(__env.apiUrlPath + '/disposisi/'+ $stateParams.id +'/log')
			    	.then(function(response){
			    		return response.data;
			    	}, function(error){
		        		return error.data.error_msg;
		        	});
        		}
	        },
            controller: function($scope, data){
	        	console.log("data ",data);
            	$scope.riwayatDispo = data.data;
            	$scope.riwayatBalasan = data.AlurDokumen;
            	console.log("riwayatBalasan",$scope.riwayatBalasan);
		    }
        })

        //LIHAT DELEGASI
        .state('lihat-delegasi',{
            url:'/lihat-delegasi/:id',
            templateUrl: 'pages/mailbox/delegasi.html',
            resolve : {
	        	data : function($http,$stateParams, __env){
		        	return $http.get(__env.apiUrlPath + '/delegation/'+$stateParams.id)
		        	.then(function(response){
		        		return response.data.data;
		        	}, function(error){
		        		return error.data.error_msg;
		        	});
		        }
	        },
            controller: function($scope, $location, $http, data, __env){
	        	$scope.detailDelegasi = data;
	        	$scope.batal = function() {
         	  		let confirmDelete = confirm('Akhiri delegasi ini ?');
         	  		if(confirmDelete) {
	         	  		$http({
    	        			url : __env.apiUrlPath + '/users/self/delegation/',
    	        			method : 'POST',
    	        			data : {
    		                    	data : {
    	        					  id : $scope.detailDelegasi.id,
    			                      delegator: $scope.detailDelegasi.delegator,
    			                      delegated: $scope.detailDelegasi.delegated,
    			                      date_start: $scope.detailDelegasi.date_start,
    			                      date_stop: $scope.detailDelegasi.date_stop,
    			                      reason: $scope.detailDelegasi.reason,
    			                      action: 'unset'
    			                    }
    		                    },
    		                headers: {'Content-Type': 'application/json'},
    		                responseType: 'json'
    	        		}).then(function onSuccess(response) {
    	        			if(response.data.data.errormsg) {
    	        				alert('Error: '+response.data.data.errormsg);
    	        			} else {
    	        				//location.reload();
    	        				$location.path('delegasi');
    	        			}
    	        		}, function onError(response) {
    	        			console.log("XXX error menghapus delegasi: ",response);
    	        		});	         	  		
         	  		}
	        	}
	        }
        })
    
    	//AGENDA SURAT
    	.state('agenda', {
    		url: '/agenda/:location/:id',
    		template: '<ng-include src="getTemplateUrl()"/>',
    		resolve: {
	    		data : function($http, $stateParams, __env){
    		    		if($stateParams.location == 'inbox'){
			    			return $http.get(__env.apiUrlPath + '/letters/inbox/' + $stateParams.id +'/agenda')
			    			.then(function (response){
			    				if(response.data.data.takahtype === 'SuratMasukManual' )
			    					response.data.data.headline = 'Agenda Surat Masuk Manual';
			    				else
			    					response.data.data.headline = 'Detail Agenda Surat Masuk';
			    				
			    				response.data.data.detailState = 'detail-inbox';
			    				response.data.data.id = $stateParams.id;
			    				let referensiObj = [];
				        		for(let ref of response.data.data.referensi) {
				        			let tmp = ref.split('~');
				        			
				        			let refObj = {noSurat: tmp[0]};
				        			
				        			let jenisDok = tmp[tmp.length - 1];
				        			if(jenisDok == 'SuratKeluar') {
				        				refObj.takahType = 'sk';
				        			} else if(jenisDok == 'NotaDinas') {
				        				refObj.takahType = 'nd';
				        			} else {
				        				refObj.takahType = 'me';
				        			}
				        			
				        			let storage = tmp[1].split('\\')[1];
				        			refObj.storage = storage;
				        			
				        			refObj.id = tmp[2];
				        			referensiObj.push(refObj);
				        		}
				        		response.data.data.referensiObj = referensiObj;
			    				return response.data.data;
			    			});
			    		}else if($stateParams.location == 'outbox'){
			    			return $http.get(__env.apiUrlPath + '/letters/outbox/' + $stateParams.id +'/agenda')
			    			.then(function (response){
			    				response.data.data.headline = 'Detail Agenda Surat Keluar';
			    				response.data.data.detailState = 'detail-outbox';
			    				response.data.data.id = $stateParams.id;
			    				let referensiObj = [];
				        		for(let ref of response.data.data.referensi) {
				        			let tmp = ref.split('~');
				        			
				        			let refObj = {noSurat: tmp[0]};
				        			
				        			let jenisDok = tmp[tmp.length - 1];
				        			if(jenisDok == 'SuratKeluar') {
				        				refObj.takahType = 'sk';
				        			} else if(jenisDok == 'NotaDinas') {
				        				refObj.takahType = 'nd';
				        			} else {
				        				refObj.takahType = 'me';
				        			}
				        			
				        			let storage = tmp[1].split('\\')[1];
				        			refObj.storage = storage;
				        			
				        			refObj.id = tmp[2];
				        			referensiObj.push(refObj);
				        		}
				        		response.data.data.referensiObj = referensiObj;
			    				return response.data.data;
			    			});
			    		}else if($stateParams.location == 'draft'){
			    			return $http.get(__env.apiUrlPath + '/letters/process/' + $stateParams.id +'/agenda')
			    			.then(function (response){
			    				response.data.data.headline = 'Detail Agenda Draft';
			    				response.data.data.detailState = 'detail-status';
			    				response.data.data.id = $stateParams.id;
			    				return response.data.data;
			    			});
			    		}else{
			    			return $http.get(__env.apiUrlPath + '/letters/process/' + $stateParams.id +'/agenda')
			    			.then(function (response){
			    				response.data.data.headline = 'Detail Agenda Surat In Progress';
			    				response.data.data.detailState = 'detail-status';
			    				response.data.data.id = $stateParams.id;
			    				let referensiObj = [];
				        		for(let ref of response.data.data.referensi) {
				        			let tmp = ref.split('~');
				        			
				        			let refObj = {noSurat: tmp[0]};
				        			
				        			let jenisDok = tmp[tmp.length - 1];
				        			if(jenisDok == 'SuratKeluar') {
				        				refObj.takahType = 'sk';
				        			} else if(jenisDok == 'NotaDinas') {
				        				refObj.takahType = 'nd';
				        			} else {
				        				refObj.takahType = 'me';
				        			}
				        			
				        			let storage = tmp[1].split('\\')[1];
				        			refObj.storage = storage;
				        			
				        			refObj.id = tmp[2];
				        			referensiObj.push(refObj);
				        		}
				        		response.data.data.referensiObj = referensiObj;
			    				return response.data.data;
			    			});
			    		}
			    	}
	    	},
    		controller: function($scope, data, $uibModal, __env, pdfDelegate){
	    		$scope.isPdfLoading = false;
	    		$scope.hidden = true;
	    		$scope.agenda = data;
	    		if (data.sifat === "Rhs") {
	    			$scope.agenda.sifat = "Rahasia";
	    		}  		
	    			    		
	    		$scope.previewRef;
	    		$scope.attFile = {};
	    		if(data.lampiran[0] != ""){
	        		$scope.attFile = data.lampiran.map(function(value, index) {
	      			    return {
	      			        name: value,
	      			        url: data.fileatturl[index]
	      			    }
	      			});
	      		}
	    		let versiMobile = (isMobile) ? 'yes' : 'no';
	    		
	    		$scope.isPdf = function(url) {
	    			return url.toLowerCase().endsWith('.pdf');
	    		}
	    		
	    		$scope.isPdfPreview = true;
	    		$scope.getTemplateUrl = function() {
	    			if($scope.isPdfPreview && data.takahtype === 'SuratMasukManual') {
	    				return 'pages/mailbox/pdf-preview.html';
	    			} else {
	    				return 'pages/mailbox/agenda.html';
	    			}
	    		}
	    		
	    		$scope.switchAgendaView = function() {
	    			$scope.isPdfPreview = !$scope.isPdfPreview;
	    		}
	    		
	    		$scope.viewPdf = function() {
	    			for(var att of $scope.attFile) {
	    				if(att.name.toLowerCase().endsWith('.pdf')) {
	    					$scope.pdfFile = att.name;
	    					$scope.previewPdf(att.url);
	    					break;
	    				}
	    			}
	    		}
	    		
	    		$scope.previewPdf = function(url) {
	    			console.log('XXX loading PDF from ', url);
	    			$scope.isPdfLoading = true;
	    			
	    			var pdfPromise = pdfDelegate.$getByHandle('my-pdf-container').load(url);
	    			pdfPromise.then(function(){
	    				console.log('XXX finish loading PDF');
	    				$scope.isPdfLoading = false;
	    			}).catch(function(reason){
	    				console.log('XXX error loading PDF caused by ', reason);
	    				$scope.isPdfLoading = false;
	    			});
	    		}
	        	
	        	$scope.previewReferensi = function(storage, unid, type) {
	        		$http.get(__env.apiUrlPath + '/letters/takah/'+type+'/'+data.id+'?dbLoc=storage&nsf='+storage+'&mdsunid='+unid+'&mob='+versiMobile, {cache: true}).then(
	        			function (response) {
	        				if(isMobile) {
	        					$scope.previewRef = response.data.data;
	        				} else {
	        					$scope.previewRef = response.data.data.html_takah;
	        				}
	        				
	        				var modalInstance = $uibModal.open({
	        				      ariaLabelledBy: 'modal-title',
	        				      ariaDescribedBy: 'modal-body',
	        				      templateUrl: 'myModalContent.html',
	        				      size: 'lg',
	        				      scope: $scope
	        				    });
	        			}, function (error) {
	        				console.log('XXX error mengambil dokumen referensi');
	        			}
	        		);
	        		
	        	};
	        	
	        	$scope.closePreview = function(modal) {
	        		modal.$close();
	        	}
	    	}
    	})
        .state('pdf-preview', {
    		url: '/pdf-preview',
    		templateUrl: 'pages/mailbox/pdf-preview.html',
    		controller: function($scope) {
	        	
	        }
        })
});


/* API List Services */

routerApp.factory('inboxListSvc', ['$http','$location', '__env', function($http,$location, __env) { 
	var data = {
	   getData : function(params,cache) {
		   if(getInboxFilterByName(params.takahType).length != 0 ){
		   console.log(params);
		   return $http.get(__env.apiUrlPath + '/users/self/inbox/letters/'+params.takahType+'/'+((params.currPage-1)*data_perpage+1)+'/'+params.perPage+params.searchKey,
				   		cache).then(function(response){
		   					 return response.data.data;
		   				 });
		   }else{
			   return $location.path("/err/notfound");
		   }
	   },
	   getCounts : function() {
		   return $http.get(__env.apiUrlPath + '/counts')
		   				 .then(function(response){
		   					 return response.data.data.kotak_masuk;
		   				 });
	   }
	}
	return data;
	 }]);


routerApp.factory('outboxListSvc', ['$http', '__env', function($http, __env) { 
	var data = {
	   getData : function(params,cache) {
		   console.log(params);
		   return $http.get(__env.apiUrlPath + '/users/self/outbox/letters/'+params.takahType+'/'+((params.currPage-1)*data_perpage+1)+'/'+params.perPage+params.searchKey,
				   		cache).then(function(response){
		   					 return response.data.data;
		   				 });
	   },
	   getCounts : function() {
		   return $http.get(__env.apiUrlPath + '/counts')
		   				 .then(function(response){
		   					 return response.data.data.kotak_keluar;
		   				 });
	   }
	}
	return data;
	 }]);

/*SERVICE COMPOSE*/

routerApp.factory('composeSvc',['$http', '$location', '$q', '__env', function($http, $location, $q, __env){
	
	var post = {
			postSuratManual : function(surat) {
				return $q(function(resolve, reject){
					surat['MDTanggalSurat'] = moment(surat['MDTanggalSurat']).format('MM/DD/YYYY');
					surat['MDDueDate'] = surat['MDDueDate'] ? moment(surat['MDDueDate']).format('MM/DD/YYYY') : '';
					surat['tanggal'] = surat['tanggal'] ? moment(surat['tanggal']).format('MM/DD/YYYY') : '';
					
					let formData = new FormData();
					for(let key in surat) {
						if(key !== 'attachment') {
							if(Array.isArray(surat[key]))
								formData.append(key, surat[key].join(';'));
							else
								formData.append(key, surat[key]);
						}
					}
        			formData.append('MDHariUndangan', surat.hari);
					formData.append('MDTanggalUndangan', surat.tanggal);
					formData.append('MDTmpUndangan', surat.tempat);
					formData.append('MDWaktuUndangan', surat.waktu);
					//formData.append('MDKlasifikasiMasalah', '00');
					//formData.append('MDKlasifikasiMasalahDesc', 'UMUM');
					formData.append('MDKlasifikasiMasalah', surat.kode);
					formData.append('MDKlasifikasiMasalahDesc', surat.topik);
					formData.append('MDJenisSMM', surat.MDJenisSMM);
					formData.append('isAPI', 'yes');
					formData.append('MDAction', 'Submit');
					formData.append('__Click', 0);
					formData.append('MDTakahType', 'SuratMasukManual');
					formData.append('MDStatus', 'Submit');
					
					for(let idx = 0, length = surat.attachment.length; idx < length; idx++) {
						formData.append('%%File.'+(idx+1), surat.attachment[idx]);
					}
					
					let xhr = new XMLHttpRequest();
					xhr.open('POST', __env.serverUrl + '/ndmasuk.nsf/fMDMasuk?OpenForm&action=createdocument&Seq=1');
					xhr.responseType = 'json';
					
					xhr.addEventListener('loadend', function(evt){
						if (xhr.readyState === xhr.DONE) {
							let resp = xhr.response;
							if(resp.data.status === 'Sukses'){
								console.log('XXX berhasil kirim surat manual');
								resolve(xhr.response);
							}else{
								console.log('XXX gagal kirim surat manual');
								reject(xhr.response);
							}
							
						}
					});
					
					xhr.send(formData);
				});
		},
		postData : function(letter, detailPengirim, tipeSurat){
			var url = '';
			
			if(letter.kepada != undefined || letter.kepada != null || letter.kepada != ''){
				letter.kepada = letter.kepada.join(';');
			}else{
				letter.kepada = '';
			}
			
			if(letter.pemeriksa != undefined || letter.pemeriksa != null || letter.pemeriksa != ''){
				letter.pemeriksa = letter.pemeriksa.join(';');
			}else{
				letter.pemeriksa = '';
			}
			
			if(letter.tembusanIn != undefined || letter.tembusanIn != null || letter.tembusanIn != ''){
				letter.tembusanIn = letter.tembusanIn.join(';');
			}else{
				letter.tembusanIn = '';
			}
			
			if(letter.tembusanEks != undefined || letter.tembusanEks != null || letter.tembusanEks != ''){
				letter.tembusanEks = letter.tembusanEks.join('\n');
			}else{
				letter.tembusanEks = '';
			}
			
			if(letter.manualSendTo != undefined || letter.manualSendTo != null || letter.manualSendTo != ''){
				letter.manualSendTo = letter.manualSendTo.join('\n');
			}else{
				letter.manualSendTo = '';
			}
			
			if (letter.referenceDocs.list == undefined) {
				letter.referenceDocs.list = '';
			}
			
			//if (letter.attCount == 0) letter.attCount = '';
			
			console.log('Referensi Agenda UNID ', letter.referenceDocs.list);
			
			let formData = new FormData();
			
			for(let key in letter) {
				if(key !== 'attachment') {
					if(Array.isArray(letter[key]))
						formData.append(key, letter[key].join(';'));
					else
						formData.append(key, letter[key]);
				}
			}
			
			formData.append('MDSendTo', letter.kepada);
			formData.append('MDCopyTo', letter.tembusanIn);
			formData.append('MDManualSendTo', letter.manualSendTo);
			formData.append('MDManualCopyTo', letter.tembusanEks);
			formData.append('MDCity', letter.kota_tujuan);
			formData.append('MDFromTitle', letter.fromtitle);
			formData.append('MDApprovers', letter.pemeriksa);
			formData.append('MDPemesanTitle', letter.pemesan);
			formData.append('MDPemesanName', detailPengirim.nama_pemesan);
			formData.append('MDPemesanCatatan', letter.pemesanCatt);
			formData.append('Subject', letter.subject);
			//formData.append('MDKlasifikasiMasalah', '00');
			//formData.append('MDKlasifikasiMasalahDesc', 'UMUM');
			formData.append('MDKlasifikasiMasalah', letter.kode);
			formData.append('MDKlasifikasiMasalahDesc', letter.topik);
			formData.append('Body', letter.body);
			formData.append('MDAbstract', letter.abstrak);
			formData.append('MDKomentar', letter.catatan);
			formData.append('MDTakahType', letter.takahtype);
			formData.append('tipeGenerik', letter.tipeGenerik);
			formData.append('MDAction', letter.action);
			formData.append('MDFromPositionID', detailPengirim.kodeunit);
			formData.append('MDFromDeptCode', detailPengirim.kodeunit);
			
			for(let idx = 0, length = letter.attachment.length; idx < length; idx++) {
				formData.append('%%File.'+(idx+1), letter.attachment[idx]);
			}
			
			formData.append('MDAttachCount', letter.attCount);
			formData.append('MDJumlahAttachment', letter.attCount);
			formData.append('MDDokReferensiPilih', letter.referensiNo);
			formData.append('MDDokReferensiData', letter.referensi);	
			formData.append('MDDokReferensiAgendaUNID', letter.referenceDocs.list);
			formData.append('MDType', letter.sifat);
			formData.append('MDTandaTangan', letter.tandaTangan);
			formData.append('MDJenisSMM', letter.MDJenisSMM);
            formData.append('MDDueDate', letter.MDDueDate ? moment(letter.MDDueDate).format('MM/DD/YYYY') : '');
			
			formData.append('MDIsWorkflowRequired', letter.workflow);
			formData.append('MDFromNameEdit', detailPengirim.nama_pejabat);
			formData.append('MDFromName', detailPengirim.nama_pejabat_full);
			formData.append('MDFromEmployeeID', detailPengirim.NUP);
			formData.append('MDFromDivision', detailPengirim.direktorat);
			formData.append('MDFromDepartment', detailPengirim.sub_direktorat);
			formData.append('MDFromAtasNama', '');
			formData.append('MDFromTipeAtasNama', letter.atasnama);
			formData.append('MDDocPriority', letter.balas);
			formData.append('MDAreaCode', detailPengirim.lokasi_2);
			formData.append('MDArea', detailPengirim.lokasi_1);
			formData.append('MDFromOfficeCity', detailPengirim.kota);
			
			if (detailPengirim.positionID === '') {
				formData.append('MDKodeJabatan', '500');
			} else {
				formData.append('MDKodeJabatan', detailPengirim.positionID);
			}
			//formData.append('MDKodeJabatan', detailPengirim.positionID);
			formData.append('isAPI', 'yes');
			formData.append('__Click', 0);
			
			console.log('Surat : ', letter);
			return $q(function(resolve, reject){
				let xhr = new XMLHttpRequest();
//				xhr.responseType = 'json';
				xhr.addEventListener('loadend', function(evt){
					if (xhr.readyState === xhr.DONE) {
						console.log('XXX response ', xhr.response);
						let resp = JSON.parse(xhr.response);
						/*let tmp = '{'+ 
							 '"data" : {'+ 
							 '"id" : "AC844BB18FE59F50472580E900385978",'+ 
							 '"status" : "Sukses"'+ 
							 '}'+
							 '}';*/
						//let resp = JSON.parse(tmp);
						console.log('XXX response from server: ', resp);
						console.log('XXX type of', typeof(resp));
						if(resp.data.status === 'Sukses'){
							resolve(resp);
						}else{
							reject({status:'error', message:'Proses pengiriman tidak dapat dilakukan.', redirect_path:'/nodin'});
						}
						
					}
				});
				
				if((tipeSurat === 'Memo') || (tipeSurat === 'NotaDinas')){
					xhr.open('POST', __env.serverUrl + '/ndkeluar.nsf/fMDSuratIntern?OpenForm');
				}else if(tipeSurat === 'SuratKeluar'){
					xhr.open('POST', __env.serverUrl + '/ndkeluar.nsf/fMDSuratExtern?OpenForm');
				}else {
					alert('Tipe surat tidak terdaftar');
					//return null;
					reject({status:'error', message:'Proses pengiriman tidak dapat dilakukan.', redirect_path:'/nodin'});
				}
				
				xhr.send(formData);
			});
			
			// xhr gak punya respon success atau error
			//console.log(response);
			//return response;
		},
		postDataDraft : function(letter, detailPengirim){
			letter.kepada = letter.kepada.join(';');
			letter.pemeriksa = letter.pemeriksa.join(';');
			letter.tembusanIn = letter.tembusanIn.join(';');
			letter.attDelete = letter.attDelete.join(';');
			letter.tembusanEks = letter.tembusanEks.join('\n');
			letter.manualSendTo = letter.manualSendTo.join('\n');
			if (letter.referenceDocs.list == undefined) {
				letter.referenceDocs.list = '';
			}
			//if (letter.attCount == 0) letter.attCount = '-';
			
			let formData = new FormData();
			
			for(let key in letter) {
				if(key !== 'attachment') {
					if(Array.isArray(letter[key]))
						formData.append(key, letter[key].join(';'));
					else
						formData.append(key, letter[key]);
				}
			}
			
			formData.append('MDSendTo', letter.kepada);
			formData.append('MDCopyTo', letter.tembusanIn);
			formData.append('MDManualSendTo', letter.manualSendTo);
			formData.append('MDManualCopyTo', letter.tembusanEks);
			formData.append('MDCity', letter.kota_tujuan);
			formData.append('MDFromTitle', letter.fromtitle);
			formData.append('MDApprovers', letter.pemeriksa);
			formData.append('MDPemesanTitle', letter.pemesan);
			formData.append('MDPemesanName', detailPengirim.nama_pemesan);			
			formData.append('MDPemesanCatatan', letter.pemesanCatt);
			formData.append('Subject', letter.subject);
			formData.append('MDKlasifikasiMasalah', letter.kode);
			formData.append('MDKlasifikasiMasalahDesc', letter.topik);
			formData.append('Body', letter.body);
			formData.append('MDKomentar', letter.catatan);
			formData.append('MDTakahType', letter.takahtype);
			formData.append('MDAction', letter.action);
			formData.append('MDFromPositionID', detailPengirim.kodeunit);
			formData.append('MDFromDeptCode', detailPengirim.kodeunit);
			
			for(let idx = 0, length = letter.attachment.length; idx < length; idx++) {
				formData.append('%%File.'+(idx+1), letter.attachment[idx]);
			}
			
			formData.append('MDAttachCount', letter.attCount);
			formData.append('MDJumlahAttachment', letter.attCount);
			formData.append('MDDokReferensiPilih', letter.referensiNo);
			formData.append('MDDokReferensiData', letter.referensi);
			formData.append('MDDokReferensiAgendaUNID', letter.referenceDocs.list);
			formData.append('DeleteAttFlag', letter.attDelete);
			formData.append('MDIsWorkflowRequired', letter.workflow);
			formData.append('MDType', letter.sifat);
			formData.append('MDTandaTangan', letter.tandaTangan);
			formData.append('MDJenisSMM', letter.MDJenisSMM);
			
			formData.append('MDDueDate', letter.MDDueDate ? moment(letter.MDDueDate).format('MM/DD/YYYY') : '');
			
			formData.append('MDFromNameEdit', detailPengirim.nama_pejabat);
			formData.append('MDFromName', detailPengirim.nama_pejabat_full);
			formData.append('MDFromEmployeeID', detailPengirim.NUP);
			formData.append('MDFromDivision', detailPengirim.direktorat);
			formData.append('MDFromDepartment', detailPengirim.sub_direktorat);
			formData.append('MDFromAtasNama', '');
			formData.append('MDFromTipeAtasNama', letter.atasnama);
			formData.append('MDDocPriority', letter.balas);
			formData.append('MDAreaCode', detailPengirim.lokasi_2);
			formData.append('MDArea', detailPengirim.lokasi_1);
			formData.append('MDFromOfficeCity', detailPengirim.kota);
			if (detailPengirim.positionID === '') {
				formData.append('MDKodeJabatan', '500');
			} else {
				formData.append('MDKodeJabatan', detailPengirim.positionID);
			}
			//formData.append('MDKodeJabatan', detailPengirim.positionID);
			formData.append('isAPI', 'yes');
			formData.append('__Click', 0);
			return $q(function(resolve, reject){
				let xhr = new XMLHttpRequest();
				xhr.open('POST', __env.serverUrl + '/ndkeluar.nsf/0/'+letter.id+'?EditDocument');
				xhr.addEventListener('loadend', function(evt){
		    		if (xhr.readyState === xhr.DONE) {
						if (xhr.status === 200) {
							console.log('XXX response ', xhr.response);
							let resp = JSON.parse(xhr.response);
							/*let tmp = '{'+ 
								 '"data" : {'+ 
								 '"id" : "AC844BB18FE59F50472580E900385978",'+ 
								 '"status" : "Sukses"'+ 
								 '}'+
								 '}';*/
							//let resp = JSON.parse(tmp);
							console.log('XXX response from server: ', resp);
							console.log('XXX type of', typeof(resp));
							if(resp.data.status === 'Sukses'){
								resolve(resp);
							}else{
								reject({status:'error', message:'Proses pengiriman tidak dapat dilakukan.', redirect_path:'/nodin'});
							}
						}
					}
		    	});
				xhr.send(formData);
			});
			
		}
	}
	return post;
}])

routerApp.factory('involvedListSvc', ['$http', '__env', function($http, __env) { 
	var data = {
	   getData : function(params,cache) {
		   console.log(params);
		   return $http.get(__env.apiUrlPath + '/users/self/'+params.view+'/'+params.status+'/'+((params.currPage-1)*data_perpage+1)+'/'+params.perPage+params.searchKey,
				   		cache).then(function(response){
		   					 return response.data.data;
		   				 });
	   },
	   getCounts : function() {
		   return $http.get(__env.apiUrlPath + '/counts')
		   				 .then(function(response){
		   					 return response.data.data.kotak_proses;
		   				 });
	   }
	}
	return data;
}]);

/*Controller Finalisasi Email*/
routerApp.controller('modalEmailCtrl', function ($uibModalInstance, $rootScope, $scope, __env) {
	$scope.cancelEmail = function () {
		$uibModalInstance.close();
	}
	
	$scope.sendEmail = function () {
		$uibModalInstance.close();
	}
});

/*Active*/
routerApp.controller('navController',['$scope','$location',function($scope, $location){
    $scope.isActive = function(destination){
        return destination === $location.path();
    }
}]);

/*Info User*/
routerApp.controller('userCtrl', function($scope,$state, $http, $timeout, $rootScope, $location, __env){
	$rootScope.$state = $state;
	$rootScope.$location = $location;
	
	$scope.nodin = nodin;
	$scope.suratExtern = suratExtern;
	
	$scope.inbox = inboxFilter;
	$scope.outbox = outboxFilter;
		
	$http.get(__env.apiUrlPath+'/users/self/unread/',{ignoreLoadingBar: true}).then(function(profile){
		$rootScope.profile = profile.data.data;
		
		var jabatan = profile.data.data.jabatan;
		if($rootScope.profile.pin_expired === 'yes'){
			swal({
				title: 'Pengingat Ubah PIN', 
				text: 'PIN anda sudah aktif lebih dari 3 bulan.',
				type: "warning",
				showConfirmButton: true,
				confirmButtonText: "Ubah Sekarang"
			},
			function(dismiss){
					swal.close();
					return $state.go('ganti-pin');
			});
		}
		$scope.updateCounts = function(){
			
			$http.get(__env.apiUrlPath + '/users/self/unread/',{ignoreLoadingBar: true}).then(function(response){
				$rootScope.profile.counts = response.data.data.counts;
				//$rootScope.profile.counts = response.data.data.counts;
				$http.get(__env.apiUrlPath + '/counts', {ignoreLoadingBar: true}).then(function (response) {
					$rootScope.profile.counts.needverification = response.data.data.kotak_proses.needverification;
					$rootScope.profile.counts.draft = response.data.data.kotak_proses.draft;
				});
				$scope.inboxTotalUnread = $rootScope.profile.counts.letter_unread + $rootScope.profile.counts.disposisi_unread + $rootScope.profile.counts.tanggapan;

				
				if(letter_unread_temp < $scope.inboxTotalUnread){
					$scope.push();
				}
				
				letter_unread_temp = $scope.inboxTotalUnread;
				for(var i=0;$scope.inbox.length > i;i++){
					getInboxFilterByName($scope.inbox[i].name)[0].unread =  eval('$rootScope.profile.counts.'+$scope.inbox[i].name);
				}

			});	
			$timeout($scope.updateCounts, 10000);
			
		};
	
		$http.get(__env.apiUrlPath+'/users/self/assistance/'+jabatan)
		.then(function(assistance){
			$rootScope.profile.sekretaris = assistance.data.data.sekretaris;
			$rootScope.profile.setSekretaris = assistance.data.data.status;
		});
		
		$http.get(__env.apiUrlPath+'/profpic?jenis=ttd').then(function(response){
			console.log('XXX ttd ', response);
			$rootScope.profile.ttdUrl = response.data.data.urlfoto;
			$rootScope.profile.ttdUNID = response.data.data.docunid;
		});
		$scope.updateCounts();
	});
	$scope.logout_uri = __env.eofficeUrl +"/eoffice.nsf?logout&redirectto=%2Feoffice%2Feoffice.nsf%3Flogin";
	$scope.push = function(){
		console.log('XXX eoffice url ', __env.eofficeUrl);
		
		Push.create('Office Automation Dit. PPKBLU', {
            body: 'Anda mendapatkan surat/informasi baru',
            icon: '/domcfg.nsf/icon-blu-128x128.png',
            link : __env.serverUrl+'/eoffice.nsf/index.html#!/inbox/all',
            timeout: 4000,
            onClick: function () {
                //console.log("Fired!");
                window.focus();
                this.close();
            },
            vibrate: [200, 100, 200, 100, 200, 100, 200]
        });
	}
	
	
	$scope.fabOpenMethod = $scope.isMobile ? 'click' : 'hover';
	$scope.close = function(){
		if(isMobile){
			document.getElementsByTagName("BODY")[0].className = "skin-blue-light sidebar-mini fixed ng-scope sidebar-collapse"; 
		}
	}

});

/*Info Modal*/
routerApp.controller('modalInfoCtrl', function($uibModalInstance, info, $scope, $filter){
	$scope.info = info;
	var obj = $scope.info.riwayat.reduce(function(objArr, cur, i){
		objArr[i] = cur;
		return objArr;
	}, {});
	
	var keys = Object.keys(obj);
	var len = keys.length;
	
	function arr(obj){
		$scope.arrObj = [];
		
		$scope.arrObj[0] = obj[0];
		for(i=2; i<len; i+=3){
			$scope.arrObj[i] = obj[i];
		}
		
		$scope.arrObj.splice(1,1);
		
		//Utk menghapus array yang kosong
		for(i=2; i<$scope.arrObj.length; i+=2){
			$scope.arrObj.splice(i,2);
		}
			
		return $scope.arrObj;
	};
	
	//console.log(obj[0].split(','));
	//checkSplit = $scope.info.riwayat[0].split(",");
	console.log(arr(obj));
	console.log(len);
	info.riwayat = arr(obj);
});

/*Search Jabatan*/
routerApp.controller('SearchCtrl', function($scope, $http, __env){
	/*$scope.search = function(jabatan){
	   return $http.get(__env.apiUrlPath + '/addrbook/all/1/20/' + jabatan)
	   .then(function(response){
		   $scope.listJabatan = response.data.data;
		});
	}*/
	$scope.search = function ($viewValue){
		return $http.get(__env.apiUrlPath + '/addrbook/all/1/20/' + $viewValue)
		.then(function(response){
		      return response.data.data;
		    });
	}
})

/*Tiny MCE*/
routerApp.controller('TinyMceController', function($scope) {
  $scope.tinymceModel = 'Initial content';

  $scope.getContent = function() {
    console.log('Editor content:', $scope.tinymceModel);
  };

  $scope.setContent = function() {
    $scope.tinymceModel = 'Time: ' + (new Date());
  };

  $scope.tinymceOptions = {
	        plugins: [
	                  'powerpaste',
	                  'advlist autolink lists link image charmap print preview hr anchor pagebreak',
	                  'searchreplace wordcount visualblocks visualchars code fullscreen',
	                  'insertdatetime media nonbreaking save table contextmenu directionality',
	                  'emoticons template paste textcolor colorpicker textpattern imagetools codesample toc'
	                ],
	        toolbar1: 'undo redo | insert | styleselect | bold italic | alignleft aligncenter alignjustify | bullist numlist outdent indent | link image',
	        toolbar2: 'print preview media | forecolor backcolor',
	        image_advtab: true,
	        powerpaste_word_import: 'prompt',
	        powerpaste_html_import: 'prompt',
	        min_height: '250'
	      };
	});

/*Modal Form*/
routerApp.factory("actionSvc", function(){
	var action = {
			aksi : '',
			body : '',
			abstrak: '',
			nosurat: '',
			kepada : [],
			tembusan : [],
			attachment : [],
			attDelete : [],
			referensi : [],
			referensiNo : []
	};
	
	return {
		getAction : function(){
			return action;
		},
		setAction : function(aksi, body, abstrak, kepada, tembusan,attachment,attDelete,referensiNo,referensi,sifat,nosurat){
			action.aksi = aksi;
			action.body = body;
			action.abstrak = abstrak;
			action.kepada = kepada;
			action.tembusan = tembusan;
			action.attachment = attachment;
			action.attDelete = attDelete;
			action.referensi = referensi;
			action.referensiNo = referensiNo;
			action.sifat = sifat;
			action.nosurat = nosurat;
		}
	};
});

routerApp.factory('actionWaitSvc', ['$http','$q', '__env', function ($http, $q, __env) {
	var wait = {
		actionWait : function (waitData, workflow) {
			console.log('Service Approval : ', waitData);
			let formData = new FormData();
			
			for(let key in waitData) {
				if(key !== 'attachment') {
					if(Array.isArray(waitData[key]))
						formData.append(key, waitData[key].join(';'));
					else
						formData.append(key, waitData[key]);
				}
			}
			
			formData.append('MDUNID',waitData.id);
			formData.append('MDSendTo', waitData.kepada);
			formData.append('MDCopyTo', waitData.tembusan);
			formData.append('CurrentPejabat',waitData.jabatanFullName);
			formData.append('CurrentJabatanName',waitData.jabatan);
			formData.append('MDComment',waitData.komentar);
			formData.append('Body',waitData.isi_surat);
			formData.append('MDAbstract', waitData.abstrak);
			formData.append('Subject',waitData.perihal);
			formData.append('MDIsWorkflowRequired',workflow);
			formData.append('MDAction',waitData.aksi);
			formData.append('isAPI','yes');
			
			for(let idx = 0, length = waitData.attachment.length; idx < length; idx++) {
				formData.append('%%File.'+(idx+1), waitData.attachment[idx]);
			}
			
			formData.append('DeleteAttFlag', waitData.attDelete);
			formData.append('MDDokReferensiPilih', waitData.referensiNo);
			formData.append('MDDokReferensiData', waitData.referensi);
			formData.append('MDType', waitData.sifat);
			formData.append('MDDueDate', waitData.tenggat ? moment(waitData.tenggat).format('MM/DD/YYYY') : '');
			formData.append('MDTandaTangan', waitData.tandaTangan);
			formData.append('__Click', 0);
			
			return $q(function (resolve, reject) {
				let xhr = new XMLHttpRequest();
				xhr.addEventListener('loadend', function(evt){
		    		if (xhr.readyState === xhr.DONE) {
		    			console.log('XXX response wait ', xhr.response);
		    			let resp = JSON.parse(xhr.response);
		    			console.log('XXX response wait from server: ', resp);
						console.log('XXX type of', typeof(resp));
						if (resp.data.status === 'Sukses') {
							resolve(resp);
						}else{
							reject({status:'error', message:'Proses persetujuan tidak dapat dilakukan.', redirect_path:'/wait'})
						}
					}
		    	});
				xhr.open('POST', __env.serverUrl + '/ndkeluar.nsf/0/'+waitData.id+'?EditDocument');
				xhr.send(formData);
			});
		}
	}
	return wait;
}]);

routerApp.controller("formModalCtrl", ['$uibModal', '$log', '$document', '$scope', '$http' , 'actionSvc', '__env', function ($uibModal, $log, $document,$scope, $http, actionSvc, __env) {
    $scope.modalKomen = function(action,body,abstrak,kepada,tembusan,attachment,attDelete,sifat){
        $uibModal.open({
        	templateUrl: 'modalKomentar.html',
            controller: 'modalCtrl'
        })
        
        console.log('Abstrak : ', abstrak);
        console.log('Body : ', body);
        
        $scope.resolveReferences = function(data) {
    		data.list = data.list.filter(function(val){
    			if(val)
    				return true;
    			else
    				return false;
    		});
    		
    		data.data = data.data.filter(function(val){
    			if(val)
    				return true;
    			else
    				return false;
    		});
    		
    		$scope.detailWait.referenceDocs.list.push(data.list.toString());
    		$scope.detailWait.referensiNo = data.subject;
    		$scope.detailWait.referensi = data.data;
    	}
        
        if($scope.detailWait.referenceDocs.list && $scope.detailWait.referenceDocs.list.length > 0) {
			$http.get(__env.apiUrlPath + '/referensi?docunid='+$scope.detailWait.referenceDocs.list.toString(), {responseType: 'json'}).then(
				function (response) {
        			$scope.detailWait.referensi = $scope.detailWait.referensi.concat(response.data.data.refDetail);
        			// remove duplicat
        			$scope.detailWait.referensi = $scope.detailWait.referensi.filter(function(value, idx, self){
        				return self.indexOf(value) == idx;
        			});
        			console.log('Referensi : ', $scope.detailWait.referensi);
        			console.log('ReferensiNo : ', $scope.detailWait.referensiNo);
        			actionSvc.setAction(action,body,abstrak,kepada,tembusan,attachment,attDelete,$scope.detailWait.referensiNo,$scope.detailWait.referensi,sifat,nosurat);
				}, function (error) {
					console.log('XXX error getting references ', error);
				}
			);
		} else {
			//$scope.detailWait.referensi = [];
			//$scope.detailWait.referensiNo = [];
			actionSvc.setAction(action,body,abstrak,kepada,tembusan,attachment,attDelete,$scope.detailWait.referensiNo,$scope.detailWait.referensi,sifat);
			console.log('Referensi : ', $scope.detailWait.referensi);
			console.log('ReferensiNo : ', $scope.detailWait.referensiNo);
		}
        
        //actionSvc.setAction(action,body,kepada,tembusan,attachment,attDelete);
        //console.log('Tembusan : ', tembusan);
    };
}]);

routerApp.controller("modalCtrl",['$uibModalInstance', '$scope', '$http','$state','actionSvc', '$location', 'actionWaitSvc', '$rootScope', function($uibModalInstance,$scope, $http, $state, actionSvc, $location, actionWaitSvc, $rootScope){
	//$scope.detailWait = {};
	$scope.disableBtnKirim = false;
	$scope.buttonKirimText = "Kirim";
	$scope.sendForm = function(){		
		//console.log(actionSvc.getAction());
		$scope.disableBtnKirim = true;
		$scope.buttonKirimText = "Loading...";
		var pin = document.getElementById('pin').value;
		var perihal = document.getElementById('perihal').value;
		var referensi = actionSvc.getAction().referensi;
		var referensiNo = actionSvc.getAction().referensiNo;
		var isi_surat = actionSvc.getAction().body;
		var abstrak = actionSvc.getAction().abstrak;
		var kepada = actionSvc.getAction().kepada;
		var tembusan = actionSvc.getAction().tembusan;
		var attachment = actionSvc.getAction().attachment;
		var attDelete = actionSvc.getAction().attDelete;
		var id = document.getElementById('id_surat').value;
		var komentar = document.getElementById('komentar').value;
		var jabatan = document.getElementById('jabatan').value;
		var jabatanFullName = document.getElementById('fullNameJabatan').value;
		var sifat = actionSvc.getAction().sifat;
		var aksi = actionSvc.getAction().aksi;
		var nosurat = actionSvc.getAction().nosurat;
		
		if (attachment == undefined || attachment == null){
			attachment = {};
		}
		if (kepada.length > 0){
			kepada = kepada.join(';');
		} else {
			kepada = '';
		}
		if (tembusan.length > 0) {
			tembusan = tembusan.join(';');
		} else {
			tembusan = '';
		}
		if (attDelete == undefined || attDelete == null){
			attDelete = [];
		}
		
		if (attDelete.length > 0){
			attDelete = attDelete.join(';');
		}
		$scope.persetujuan = {
			id : id,
			perihal : perihal,
			isi_surat : isi_surat,
			kepada : kepada,
			abstrak: abstrak,
			tembusan : tembusan,
			attachment : attachment,
			attDelete : attDelete,
			komentar : komentar,
			jabatan : jabatan,
			jabatanFullName : jabatanFullName,
			referensi : referensi,
			referensiNo : referensiNo,
			aksi : aksi,
			sifat: sifat
		}
		
		console.log('XXX Persetujuan : ',$scope.persetujuan);
		
		$http.get(__env.apiUrlPath + '/approvalpass').then(function(response){	
			if(!response.data.data.hasOwnProperty('errormsg')){
				$scope.approval_pass = response.data.data.approval_pass;
				$scope.key = response.data.data.key;
				if($scope.approval_pass === pin && $scope.key === $rootScope.profile.id){
					var approvalResult = actionWaitSvc.actionWait($scope.persetujuan, 1);
					approvalResult.then(function (response) {
						$scope.response = response;
						console.log('XXX cek response approval1: ', $scope.response);
						$uibModalInstance.close();
						if ($scope.response.status != 'error') {
							if (aksi == 'Approve') {
								if ($scope.response.data.MDStatus == 'Complete') {
									$scope.response.flow = 'Sukses!',
									$scope.response.message = 'Anda berhasil menyetujui surat ini'
									$scope.response.location = 'outbox';
								} else {
									$scope.response.flow = 'Surat berhasil disetujui',
									$scope.response.location = 'proses';
								}
							} else if (aksi == 'Return') {
								$scope.response.flow = 'Sukses',
								$scope.response.message = 'Anda berhasil mengembalikan surat ini',
								$scope.response.location = 'process';
							} else if (aksi == 'Reject') {
								$scope.response.flow = 'Sukses',
								$scope.response.message = 'Anda telah membatalkan surat ini'
								$scope.response.location = 'outbox';
							}
							
							swal({
								title: $scope.response.flow, 
								text: $scope.response.message,
								timer: 1500,
								showConfirmButton: false,
								type: "success"},
								function(dismiss){
									if (dismiss == 'timer') {
										swal.close();
										return $state.go('agenda',{location:$scope.response.location,id:$scope.response.data.id});
									} else {
										swal.close();
										return $state.go('agenda',{location:$scope.response.location,id:$scope.response.data.id});
									}
								});
						} else {
							swal({
								title:"Error",
								text: $scope.response.message,
								type: "error"
							});
							
						}
					}, function (error) {
						console.log('XXX error wait promise: ', error);
					});
				}
				else{
					$scope.disableBtnKirim = false;
					$scope.buttonKirimText = "Kirim";
					swal({
						title: 'PIN', 
						text: 'Kesalahan memasukan PIN',
						type: "error",
						timer: 1500,
						showConfirmButton: false
					},
					function(dismiss){
							swal.close();
							
					});
					
					
				}
			}else{
				$scope.disableBtnKirim = false;
				$scope.buttonKirimText = "Kirim";
				swal({
					title: 'PIN', 
					text: 'Anda belum memiliki PIN Approval',
					type: "warning",
					showConfirmButton: true,
					confirmButtonText: "Buat Sekarang"
				},
				function(dismiss){
					
						swal.close();
						$scope.cancelForm();
						return $state.go('ganti-pin');
				});
				
			}
		});
		
		
		console.log('Response Approve : ',$scope.persetujuan);
    };
    
    $scope.cancelForm = function(){
        $uibModalInstance.close();
    };
}]);

/*Pilih Jabatan*/
routerApp.controller('jabatanCtrl', function($uibModalInstance, items, $scope){
	$scope.items = items;
	$ctrl.ok = function () {
		$uibModalInstance.close($ctrl.selected.item);
	};

	$ctrl.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
	};
});

angular.module('eofficeApp').component('modalComponent', {
	  templateUrl: 'myModalContent.html',
	  bindings: {
	    resolve: '<',
	    close: '&',
	    dismiss: '&'
	  },
	  controller: function () {
	    var $ctrl = this;

	    $ctrl.$onInit = function () {
	      $ctrl.items = $ctrl.resolve.items;
	      $ctrl.selected = {
	        item: $ctrl.items[0]
	      };
	    };

	    $ctrl.ok = function () {
	      $ctrl.close({$value: $ctrl.selected.item});
	    };

	    $ctrl.cancel = function () {
	      $ctrl.dismiss({$value: 'cancel'});
	    };
	  }
	});

routerApp.controller('AddressBookList', ['$scope', function ($scope) {
  $scope.remove = function (scope) {
    scope.remove();
  };

  $scope.toggle = function (scope) {
    scope.toggle();
  };

  $scope.moveLastToTheBeginning = function () {
    var a = $scope.data.pop();
    $scope.data.splice(0, 0, a);
  };

  $scope.newSubItem = function (scope) {
    var nodeData = scope.$modelValue;
    nodeData.nodes.push({
      id: nodeData.id * 10 + nodeData.nodes.length,
      title: nodeData.title + '.' + (nodeData.nodes.length + 1),
      nodes: []
    });
  };

  $scope.collapseAll = function () {
    $scope.$broadcast('angular-ui-tree:collapse-all');
  };

  $scope.expandAll = function () {
    $scope.$broadcast('angular-ui-tree:expand-all');
  };

  $scope.data = [{
    'id': 1,
    'title': 'node1',
    'nodes': [
      {
        'id': 11,
        'title': 'node1.1',
        'nodes': [
          {
            'id': 111,
            'title': 'node1.1.1',
            'nodes': []
          }
        ]
      },
      {
        'id': 12,
        'title': 'node1.2',
        'nodes': []
      }
    ]
  }, {
    'id': 2,
    'title': 'node2',
    'nodrop': true, // An arbitrary property to check in custom template for nodrop-enabled
    'nodes': [
      {
        'id': 21,
        'title': 'node2.1',
        'nodes': []
      },
      {
        'id': 22,
        'title': 'node2.2',
        'nodes': []
      }
    ]
  }, {
    'id': 3,
    'title': 'node3',
    'nodes': [
      {
        'id': 31,
        'title': 'node3.1',
        'nodes': []
      }
    ]
  }];
}]);

routerApp.filter('trusted', function($sce){
    return function(html){
        return $sce.trustAsHtml(html);
    }
 });

routerApp.run(function($rootScope){
	$rootScope.$on('$stateChangeSuccess',function(ev, to, toParams, from, fromParams){
		$rootScope.previousState = from.name;
		$rootScope.previousParam = fromParams;
		
		$rootScope.currentState = to.name;
		$rootScope.currentParam = toParams;
		
		//console.log("from :",from);
		//console.log("from params :",fromParams);
		
		//console.log("current :",to);
		//console.log("current params :",toParams);
	    $("html, body").animate({ scrollTop: 0 }, 500);
	})
 })