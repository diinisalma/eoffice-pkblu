(function () {
  'use strict';
  
  function dateFormater(input){
	  // Input Date String Format MM/dd/yyyy KK:mm:ss a ZE7
	  // example : 12/13/2016 11:15:09 AM ZE7
	  // Output Date String Format
	  // example : 13 Des 2016 11:15
	  var format = "MM/dd/yyyy KK:mm:ss a 'ZE7'"
	  var month = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Agu","Sep","Okt","Nov","Des"];
	  var fullDateArr = input.split(" ");
	  var dateSplit = fullDateArr[0].split("/");
	  var timeSplit = fullDateArr[1].split(":");
	  var output = dateSplit[1]+ " "+ month[parseInt(dateSplit[0]) - 1]+ " "+dateSplit[2] +" "+timeSplit[0]+ ":"+timeSplit[1];
	  return output;  
  }
  
});